\section[Lorentz-invariant measure]{The Lorentz invariant measure on a mass hyperboloid}
\label{sec:lorentz-invariant-measure-mass-hyperboloid}
%
%
\newcommand{\dreierp}{\mathsf{p}}
\newcommand{\dreierq}{\mathsf{q}}
\newcommand{\viererp}{p}%{\mathsf{p}}
\newcommand{\viererq}{q}%{\mathsf{q}}

\para
Consider Minkowski space of space dimension $d$ that is $\R^{1+d}$ endowed with the Minkowski inner product 
\[ \langle \cdot,\cdot\rangle_\textup{M}: \R^{1+d}\times\R^{1+d} \to \R, \: ( p, q) \mapsto  - p^0  q^0 +
  \inprod{\vec{\,p},\vec{\,q}} =  p^0  q^0 -  \sum_{i=1}^d  p^i  q^i\ .\]
Note that $\inprod{\cdot,\cdot}$ stands here for the euclidean inner product, and
$\vec{\,p}$ is the \emph{spacial vector} $(p^1,\ldots,p^ d)$ associated to the \emph{space-time vector}
$p\in \R^{1+d}$. 
We sometimes will denote space-time dimension $1+d$ by $D$. 
%Denote by $q_\textup{M}: \R^{1+d} \to \R$ the Lorentz quadratic form
%$p \mapsto \langle p,p \rangle_\textup{M} = - (p^0)^2 + \sum_{i=1}^d  (p^i)^2$.
For $m>0$ let 
\[ H_m^+ = \{ p \in \R^D\mid \langle  p, p\rangle_\textup{M} =  m^2 \: \& \: p^0 >0 \} \]
be the positive mass hyperboloid of mass $m$. 
Observe that 
\[\chi^+ : \R^d \to H_m^+, \: \dreierp\mapsto \big(E({\dreierp}), \dreierp\big)\quad\text{with }
  E({\dreierp}) = \sqrt{m^2 + \inprod{\dreierp,\dreierp\, }}\]
is a global chart of the mass hyperboloid. 
Its inverse is given by \[\vec{\hspace{.5em}}: H^+_m \to \R^d, \: p = (p^0,p^1,\ldots ,p^d) \mapsto \vec{\,p}=(p^1,\ldots,p^d) \ .\]
Note that $E(\vec{\,p}) = p^0$ for all $p\in H_m^+$.

Now let $\lambda$ denote Lebesgue measure on $\R^d$. We will show that the pushforward measure 
$\Omega_m  = \chi^+_* \left(\frac{1}{E}\lambda\right)$ is a Lorentz invariant measure on $H_m^+$ that is 
$\Lambda_* \Omega_m =\Omega_m$ for all $\Lambda \in \LieSO^\uparrow (1,d)$. 
Note that we have used here that $\Lambda$ leaves $H_m^+$ invariant. 


\begin{lemma}\label{thm:properties-lorentz-transformation-hyperboloid-chart}
  For $\Lambda \in \LieSO^\uparrow (1,d)$ let $\Psi_\Lambda$ denote the map
  \[ 
    \Psi_\Lambda : \R^d\to \R^d, \: \dreierp\mapsto  
    \Psi_\Lambda(\dreierp) = \overrightarrow{\Lambda\chi^+(\dreierp)} \ .
  \]
  Then $\Psi_\Lambda$ is a diffeomorphism and the following holds true:
  \begin{romanlist}
  \item\label{ite:homomorphism-property-lorentz-transformation-hyperboloid-chart}
    The map $\LieSO^\uparrow (1,d) \to \Diff (\R^d)$, 
    $\Psi: \Lambda \mapsto \Psi_\Lambda$ is a homomorphism 
    that is 
    \[ 
     \Psi_{\Lambda_1\Lambda_2} = \Psi_{\Lambda_1}\Psi_{\Lambda_2}
     \quad\text{for all }
     \Lambda_1,\Lambda_2 \in \LieSO^\uparrow (1,d) \ .
    \]
  \item\label{ite:jacobian-lorentz-transformation-hyperboloid-chart} 
    The jacobian  of $\Psi_\Lambda$ is given by
    \[ J_{\Psi_\Lambda} = \left| D\Psi_\Lambda\right| = \det \circ \, D\Psi_\Lambda = \frac{E \circ \Psi_\Lambda}{E} \ .  \]
  \end{romanlist}
\end{lemma}
\begin{proof}
  \begin{adromanlist}
  \item
    Let $\Lambda_1,\Lambda_2 \in \LieSO^\uparrow (1,d) $, $ \dreierp \in \R^d$ and compute
    \[
       \Psi_{\Lambda_1}\Psi_{\Lambda_2}(\dreierp) = \Psi_{\Lambda_1}\left(\overrightarrow{\Lambda_2\chi^+(\dreierp)}\right)=
       \overrightarrow{\Lambda_1\Lambda_2 \chi^+(\dreierp)} = 
       \Psi_{\Lambda_1\Lambda_2} (\dreierp) \ .
    \]
    This implies in particular that $\Psi_\Lambda$ is a diffeomorphism with inverse $\Psi_{\Lambda^{-1}}$.
  \item
    Assume first that $\Lambda \in \LieSO^\uparrow (1,d)$ is a rotation that is 
    $\Lambda = \left(\begin{smallmatrix}1 & 0\\0&R\end{smallmatrix}\right)$ for some $R\in \LieSO(d)$. 
    Then observe that $\Psi_\Lambda =R$ and compute for $\dreierp \in \R^d$
    \[
       E(\Psi_\Lambda\dreierp) = E(R\dreierp)= \sqrt{m^2 + \inprod{R\dreierp,R\dreierp}}  =\sqrt{m^2+\inprod{\dreierp,\dreierp}} = 
       E(\dreierp) \ .
    \]
    Hence \[\det \left( D\Psi_\Lambda (\dreierp) \right)= 1 = \frac{E(\Psi_\Lambda\dreierp)}{ E(\dreierp)} \ . \]

    Next let $\Lambda$ be a Lorentz boost in the direction of $p^1$ that is let 
    $\Lambda =\left(\begin{smallmatrix}\cosh \tau & \sinh \tau & 0 \\ \sinh \tau & \cosh \tau & 0 \\
    0&0& 1 \end{smallmatrix}\right)$ where $\tau\in \R$ and $1$ denotes the identity matrix over $\R^{d-1}$. 
    
    Then compute with $\Psi_{\Lambda,i}$ for $i=1,\ldots,d$ denoting the $i$-th component of $\Psi_\Lambda:\R^d \to\R^d$:
    \[
      \Psi_{\Lambda,i} (\dreierp) = \Lambda_{i0} E(\dreierp) +\sum_{i=1}^d \Lambda_{ij} \mathsf{p}^j =
      \begin{cases}
        \sinh \tau \cdot E(\dreierp) + \cosh\tau \cdot \mathsf{p}^1 &\text{for } i=1,\\
        \mathsf{p}^i &\text{for } i=2,\ldots,d,
      \end{cases}  
    \]
    \[
      \frac{\partial\Psi_{\Lambda,i}}{\partial\mathsf{p}^j} (\dreierp) =
        \begin{cases}
        \sinh \tau \cdot\frac{\mathsf{p}^1}{E(\dreierp)} +  \cosh \tau  &\text{for } i=j= 1,\\ 
        \sinh \tau \cdot\frac{\mathsf{p}^j}{E(\dreierp)} &\text{for } i=1 \text{ and } j= 2,\ldots ,d,\\
        0 &\text{for }  i= 2,\ldots ,d \text{ and } j=1 , \\
        \delta_{ij} &\text{for } i,j=2,\ldots,d,
      \end{cases}  
    \]
    and 
    \begin{equation*}
      \begin{split}
        E^2(\Psi_\Lambda\dreierp) & = \sinh^2 \tau \cdot  E^2 (\dreierp) + 2 \sinh \tau \cosh \tau \cdot E(\dreierp) \cdot \mathsf{p}^1
        + \cosh^2 \tau \cdot (\mathsf{p}^1)^2 + \sum_{i=2}^d \left( \mathsf{p}^i\right)^2 + m^2 = \\
        & = (\sinh^2 \tau + 1) \cdot  E^2 (\dreierp)  + 2 \sinh \tau \cosh \tau \cdot E(\dreierp) \cdot \mathsf{p}^1 +
        (\cosh^2 \tau -1 ) \cdot (\mathsf{p}^1)^2 = \\
        & = \left( \cosh \tau \cdot E(\dreierp) + \sinh \tau \cdot \mathsf{p}^1\right)^2 \ .
      \end{split}
    \end{equation*}
    This entails the equality 
    \[ \det \left( D\Psi_\Lambda (\dreierp) \right) = \sinh\tau\cdot\frac{\mathsf{p}^1}{E(\dreierp)}+\cosh\tau = 
    \frac{E(\Psi_\Lambda\dreierp)}{ E(\dreierp)} \ .\]
    Since $\LieSO^\uparrow (1,d)$ is generated by the rotations and Lorentz boosts in direction $p^1$ and since
    by \ref{ite:homomorphism-property-lorentz-transformation-hyperboloid-chart}
    \[ 
      \det \left( D\Psi_{\Lambda_1\Lambda_2} (\dreierp) \right) = \det \left( D\Psi_{\Lambda_1} (\Psi_{\Lambda_2}\dreierp) \right)
      \cdot \det \left( D\Psi_{\Lambda_2} (\dreierp) \right)
    \]
    the claim follows. 
  \end{adromanlist}
\end{proof}

\begin{proposition}
  With notation as above the pushforward measure 
  $\Omega_m  = \chi^+_* \left(\frac{1}{\omega}\lambda\right)$ is a Lorentz invariant measure on the positive mass hyperboloid $H_m^+$
  that is 
  \begin{equation}
    \label{eq:invariance-pushforward-measure-mass-hyperboloid}
    \int_{H_m^+} f(\Lambda p) \, d\Omega_m (p) = \int_{H_m^+} f(p)\, d\Omega_m (p) 
  \end{equation}
  for all $f \in L^1 (H_m^+,\Omega_m )$ and $\Lambda \in \LieSO^\uparrow (1,d)$.
\end{proposition}

\begin{proof}
  By definition of the pushforward measure $\Omega_m $ is the unique Borel measure on $H_m^+$ such that
  for all $f \in \shContFcts_\textup{cpt} (H_m^+)$
  \[
      \int_{H_m^+} f(p) \, d\Omega_m (p) =  \int_{\R^d}  f(\chi^+ \dreierp) \, \frac{1}{E(\dreierp)}d\lambda (\dreierp) \ .
  \] 
 The claim  follows from this observation since for all $\Lambda \in \LieSO^\uparrow (1,d)$ 
 the equality
  \begin{equation*}
    \begin{split}
    \int_{\R^d} & f (\Lambda \chi^+ \dreierp) \frac{1}{E(\dreierp)}d\lambda (\dreierp) = 
    \int_{\R^d} f (\chi^+ \Psi_\Lambda \dreierp) \frac{1}{E(\dreierp)}d\lambda (\dreierp)  = \\
    &= \int_{\R^d} f (\chi^+ \Psi_\Lambda \dreierp) \frac{1}{E(\Psi_\Lambda\dreierp)} \det\left(D\Psi_\Lambda (\dreierp)\right)d\lambda (\dreierp) 
    = \int_{\R^d} f ( \chi^+ \dreierp) \frac{1}{E(\dreierp)}d\lambda (\dreierp) \ .   
    \end{split}
  \end{equation*}
 holds true by 
 \Cref{thm:properties-lorentz-transformation-hyperboloid-chart} \ref{ite:jacobian-lorentz-transformation-hyperboloid-chart} .
\end{proof}

