% Copyright 2017 Markus J. Pflaum
% main author: 
%   Markus J. Pflaum 
%
\section{The Lorentz group $\LieSO (1,3)$ and its universal cover $\LieSL (2,\C)$}
\textbf{to do:} \color{red} change signature from $(-,+,+,+)$ back to $(+,-,-,-)$. \color{black}
\para
Recall from \Cref{ex:bilinear-forms} \ref{ite:pseudo-euclidean-metric} that the \emph{Minowski inner product}
of two elements $x=(x^0,x^1,x^2,x^3) \in \R^4$ and $y=(y^0,y^1,y^2,y^3) \in \R^4$
is defined by  $\inprod{x,y}_\textup{M} =  x^0y^0 - \sum_{k =1}^3 x^k y^k$, and that $\R^4$
endowed with the Minkowski inner product is denoted $\R^{1,3}$. The signature of the
Minowski inner product therefore is $(+,-,-,-)$ or in other terms $(1,3)$. 
As usual we call  $\R^{1,3}$ \emph{Minkowski space} of (\emph{space-time}) \emph{dimension} $4$. 

Recall from \Cref{ex:specific-groups-defined-bilinear-forms} \ref{ite:pseudo-orthogonal-group}
that the \emph{pseudo-orthogonal group} $\LieO (1,3)$ consists of all $g\in \LieGL (4,\R)$ such that 
\[ 
   \inprod{gx,gy}_\textup{M} = \inprod{x,y}_\textup{M} \quad \text{for all } x ,y \in \R^4 \ .
\]
Following common language in mathematical physics we call $\LieO (1,3)$ the \emph{Lorentz group} in
\emph{space-time dimension} $4$. The subgroup
\[
  \LieSO (1,3) = \{ g \in \LieO (1,3) \mid \det g = 1\} \subset \LieO (1,3)
\]
is called the \emph{proper Lorentz group}. 
Let us show that the Lorentz groups $\LieO (1,3)$ and $\LieSO (1,3)$ are Lie groups. 
To this end put 
\[ \eta = \begin{pmatrix} 1 & 0 & 0 & 0 \\ 0 & -1 & 0 & 0 \\ 0 & 0 & -1 & 0 \\ 0 & 0 & 0 & -1 \end{pmatrix} \]
and observe that $ \inprod{x,y}_\textup{M}  = \inprod{x,\eta y}$ for all  $x ,y \in \R^4$, where $\inprod{ -,- }$
denotes the euclidean inner product. Hence a matrix $\Lambda \in \LieGL (4,\R)$ lies in  $\LieO (1,3)$
if and only if
\begin{equation}
  \label{eq:characterization-lorentz-transformation}  
  \Lambda^\trans \eta \Lambda - \eta  = 0 \ . 
\end{equation}
Following standard language in mathematical physics we call every such 
$\Lambda$ a \emph{Lorentz transformation}.
The map $f: \LieGL (4,\R) \to \symMat(4,\R)$, $\Lambda \mapsto  \Lambda^\trans \eta \Lambda - \eta$ is smooth and has derivative
\[ T_\Lambda f: \Mat (4,\R) \to \symMat(4,\R), \: A \mapsto  A^\trans \eta \Lambda  + \Lambda^\trans \eta A\]
at $\Lambda\in \LieGL (4,\R)$. The derivative at $\Lambda$ is
surjective since $T_\Lambda f \left( \frac 12 \eta (\Lambda^\trans)^{-1} B \right) = B$ for all $B \in \symMat(4,\R)$. Hence $f$ is a submersion
and the preimage $\LieO (1,3) = f^{-1} (0)$ a Lie subgroup of $\LieGL (4,\R)$. 
The Lie algebra  $\lieo (1,3)$ of the Lorentz group then consists of the kernel of $T_\idnum f$ that is of a
all matrices $A \in \Mat (4,\R)$ such that 
\begin{equation}
  \label{eq:characterization-lie-algebra-lorentz-group}
  A^\trans \eta + \eta A = 0 \ . 
\end{equation}
Since $\dim \lieo (1,3)=  \dim \Mat (4,\R) - \dim\symMat(4,\R) = 16 -10 = 6$, one concludes that 
the Lorentz group  $\LieO (1,3)$ is a Lie group of (real) dimension $6$.
By \eqref{eq:characterization-lorentz-transformation}, the determinant
of a Lorentz transformation $\Lambda \in \LieO (1,3)$ fulfills
$|\det (\Lambda)| = 1$. Moreover, time reversal
\[
  T =
  \begin{pmatrix}
    -1 & 0 & 0 & 0\\
    0  & 1 & 0 & 0\\
    0  & 0 & 1 & 0\\
    0  & 0 & 0 & 1\\
  \end{pmatrix}
\]
and parity inversion
\[
  P =
  \begin{pmatrix}
    1 & 0 & 0 & 0\\
    0  & -1 & 0 & 0\\
    0  & 0 & -1 & 0\\
    0  & 0 & 0 & -1\\
  \end{pmatrix}
\]
are both Lorentz transformations with determinant $-1$. One concludes that
$\LieSO(1,3)$ is a Lie subgroup of the Lorentz group $\LieO(1,3)$ and that the
latter is the disjoint union of $\LieSO(1,3)$ and
$\LieSO(1,3) \cdot T = \LieSO(1,3)\cdot P$.

\para 
The \emph{special linear group} $\LieSL(2,\C)$ consists of all $g\in \LieGL(2,\C)$ such that $\det g = 1$.
It is a complex Lie group by the following argument.
Observe that the determinant $\det : \LieGL(2,\C) \to \C$ is a complex differentiable group homomorphism. Its 
(complex) tangent map at the identity $\idnum$ is given by 
\[
   T_{\idnum} \det : \liegl (2,\C) \to \C ,  \quad A \mapsto \left.\frac{\partial}{\partial z}\right|_{z=0} \det \exp (z A) 
  = \left.\frac{\partial}{\partial z}\right|_{z=0} e^{ z \tr ( A) } = \tr A \ .
\] 
This entails that $T_{\idnum} \det \left( z \idnum \right) = 2 z$ for each $z \in \C$ hence $\det$ is a holomorphic submersion
and $\LieSL(2,\C) = \det^{-1} (1)$ a complex Lie group.  
\begin{proposition}\label{thm:simply-connectedness-special-linear-group-complex-dimension-two}
  The Lie group $\LieSL (2,\C)$ is simply-connected. 
\end{proposition}
\begin{proof}
  We first show that $\LieSL (2)$ is path-connected. So let 
  $g\in \LieSL (2\C)$. Then transform $g$ into Jordan normal form that is 
  choose $S\in \LieGL (2,\C)$ such that 
  \[
   SgS^{-1} = 
   \begin{pmatrix}a_1&e\\0&a_2  \end{pmatrix} \ ,
   \] 
  where $a_1,a_2 \in \C$ with $a_1 a_2 = 1$ and $e\in \{0,1\}$. Then choose a path
  $\gamma_1 : [0,1] \to \C^\times = \C\setminus \{ 0 \}$ such that 
  $\gamma_1 (0)= 1$ and $\gamma_1 (1) = a_1$. Let 
  $\gamma_2 :[0,1] \to \C^\times$ be the path 
  which maps $t$ to $\gamma_2 (t) = \big( \gamma_1(t)\big)^{-1}$. 
  Now put 
  \[
   h (t) = S^{-1}\begin{pmatrix}\gamma_1(t)&t e \\0& \gamma_2 (t)) 
   \end{pmatrix} \ . 
  \]
  Then $h : [0,1] \to \LieSL(2,\C)$ is a continuous path connecting $h(0) = I_2$ with $h (1) = g$. 
  So $\LieSL (2,\C)$ is path-connected. 

  Next we prove that $\LieSL (2,\C)$ is simply-connected. To this end 
  recall that the subgroup $\LieSU (2) \subset \LieSL(2,\C)$ is 
  simply-connected. So to verify that  
  $\pi_1 \big( \LieSL (2,\C)\big)$ is trivial it suffices to construct a 
  (strong) deformation retraction from $\LieSL(2,\C)$ onto $\LieSU(2)$ 
  which means that we have to construct a continuous map
  $r:\LieSL (2,\C) \times [0,1] \to\LieSL (2,\C)$  such that 
  \[ 
   r_0 = \id, \quad  r_1 \big(\LieSL(2,\C)\big) \subset \LieSU(2) , \quad 
   \text{and} \quad r_t|_{\LieSU (2)} = \id_{\LieSU (2)} \text{ for all } t \in [0,1] \ . 
  \]
  Here, as usual, $r_t$ stands for the map $\LieSL (2,\C) \to \LieSL (2,\C)$, $g \mapsto r(g,t)$. 
  
  Let us agree on the following notation. For every matrix 
  $a =
  \begin{pmatrix}
    a_{11} & a_{12} \\ a_{21} & a_{22} 
  \end{pmatrix} \in \Mat (2 \times 2, \C)$ we denote by 
  $a_i$ with  $i=1,2$ the column vector 
  $\begin{pmatrix} a_{1i}  \\ a_{2i}  \end{pmatrix}$ and write $a = (a_1 , a_2)$. Vice versa, if 
  $a_i = \begin{pmatrix} a_{1i}  \\ a_{2i}  \end{pmatrix} \in \C^2$ with $i=1,2$ are two (column) vectors 
  then we denote by $(a_1,a_2)$ the matrix 
  $a = \begin{pmatrix} a_{11} & a_{12} \\ a_{21} & a_{22} \end{pmatrix}$. 
  If now $g\in \LieSL (2,\C)$ with column vectors $g_1, g_2$, then we know that $g_1$ and $g_2$ form a basis of 
  $\C^2$. Gram--Schmidt orthonormalization will transform the basis $(g_1,g_2)$ into an orthonormal basis $(u_1,u_2)$: 
  \[
    (g_1,g_2) \mapsto (u_1,u_2) = 
    \left(\frac{g_1}{\norm{g_1}},\frac{g_2-\inprod{g_2,u_1}u_1}{\norm{g_2-\inprod{g_2,u_1}u_1}}\right) \ . 
  \]  
  Therefore, Gram--Schmidt orthonormalization can be understood as a retraction from  $\LieSL (2,\C)$ to $\LieSU (2)$ 
  leaving $\LieSU(2)$ invariant. So we are almost done, we just need to make the Gram--Schmidt 
  process ``continuous'' in the sense that it can be deformed to the identity.  

  To achieve this define the following matrices depending on the parameter $t \in [0,1]$: 
  \[
    p_t (g) = \begin{pmatrix}\frac{1}{\norm{g_1}^t}&0\\0&1  \end{pmatrix} \ ,\quad 
    q_t (g)  = \begin{pmatrix}1 &-t \inprod{g_2,u_1}\\0&1  \end{pmatrix} \ , \quad
    \widetilde{p}_t (g) = \begin{pmatrix}1&0\\0&\frac{1}{\norm{g_3}^t}  \end{pmatrix} \ , 
  \]
  where $u_1 = \frac{g_1}{\norm{g_1}}$ and $g_3 = g_2 - \inprod{g_2,u_1} u_1$. 
  Each of these matrices lies in $\LieGL (2,\C)$ since their determinant is non-zero. 
  Now we define  $r:\LieSL (2,\C) \times [0,1] \to\LieSL (2,\C)$ by
  \[
     r (g,t)  = g \cdot p_t(g) \cdot q_t(g) \cdot \widetilde{p}_t(g) \ , \quad \text{where } 
     g \in \LieSL (2,\C), \: t \in [0,1] \ . 
  \] 
  Then $r_0 (g) = g$, $r_1 (g) = (u_1,u_2) \in \LieSU(2)$ (since $(u_1,u_2)$ is an orthonormal basis 
  of $\C^2$), $r (g,t) = g$ if $g \in \LieSU (2)$, and  
  $r (g,t) \in \LieSL (2,\C)$ for all $g \in \LieSL (2,\C)$, $t \in [0,1]$. The last property is the only 
  not obvious one and needs to be verified because it guarantees that $r$ is well-defined.
  The other properties are immediate and just tell that $r$ is a strong  deformation retraction of the kind we have been looking for. 
  
  We check two identies from which the remaining claim will follow immediately. For every 
  $g = \begin{pmatrix} g_{11} & g_{12} \\ g_{21} & g_{22} \end{pmatrix}  \in \LieSL (2,\C)$ compute 
  \begin{equation*}
  \begin{split}
      1  = \, & \det g \cdot \overline{\det g} = (g_{11} \, g_{22} - g_{21} \, g_{12} ) \cdot 
      (\, \overline{g_{11} \, g_{22} - g_{21} \, g_{12}} \, ) = \\
      = \, & \left( | g_{11}|^2 \, | g_{22}|^2 + |g_{21}|^2 \, |g_{12}|^2  \right) - 
      2 \Re (g_{11} \, g_{22} \, g_{21} \, g_{12}  )  = \\
      = \, & \left( |g_{11}|^2 \, |g_{12}|^2 + | g_{11}|^2 \, | g_{22}|^2 + |g_{21}|^2 \, |g_{12}|^2 + |g_{21}|^2 \, |g_{22}|^2  \right) - \\
      & - \big( |g_{11}|^2 \, |g_{12}|^2 + |g_{21}|^2 \, |g_{22}|^2 +   2 \Re (g_{11} \, g_{22} \, g_{21} \, g_{12}  ) \big) 
      = \norm{g_1}^2\norm{g_2}^2- |\inprod{g_1,g_2}|^2 \ .
  \end{split}
  \end{equation*}
  Then 
  \begin{equation*}
  \begin{split}
     \det & \, r(g,t) = \det g \, \det p_t(g) \, \det q_t(g) \, \det \widetilde{p}_t(g)
     = \left(\frac{1}{\norm{g_1}\norm{g_3}}\right)^t = \\
     &=\left(\frac{1}{\norm{g_1}}\cdot\frac{\norm{g_1}}{\sqrt{\norm{g_1}^2\norm{g_2}^2-|\inprod{g_1,g_2}|^2}}\right)^t 
     =\left(\frac{1}{\sqrt{\norm{g_1}^2\norm{g_2}^2-|\inprod{g_1,g_2}|^2}}\right)^t = 1 \ ,
  \end{split}
  \end{equation*}
  which means that $r(g,t)$ is in fact an element of $\LieSL(2,\C)$ for all $g \in \LieSL (2,\C)$ and $t \in [0,1]$.
  This finishes the proof. 
\end{proof}

\begin{proposition}
\label{thm:isomorphism-hermitian-two-by-two-matrices-minkowski-space}  
  Consider the space 
  \[
   \hermMat (2) =  \big\{ X \in \liegl(2,\C)\bigmid X^* = X \big\}
  \]
  of all hermitian $2\times 2$ matrices. Then $\hermMat (2)$ is a real vector space of dimension 
  $4$ with a basis given by the identity matrix plus the Pauli matrices that is by 
  \[
     \sigma_0 = \left(\begin{array}{cc}
         1 & 0 \\ 0 & 1
       \end{array}\right), \: 
     \sigma_1 = 
       \left(\begin{array}{cc}
         0 & 1 \\ 1 & 0
       \end{array}\right),   \: 
       \sigma_2 =  \left(\begin{array}{cc}
         0 & -\cplxi \\ \cplxi & 0 
       \end{array}\right),  \: 
        \sigma_3 = \left(\begin{array}{cc}
         1 & 0 \\ 0 & -1 
       \end{array}\right) 
  \]
  The bilinear form
  \[
   \inprod{\cdot,\cdot}_{\mathfrak{h}} :  \hermMat(2) \times \hermMat(2) \to \R, 
   \quad (X,Y)\mapsto 
    \frac 12 \left( \det (X+Y)  - \det X - \det Y \right) 
  \]
  is symmetric, non-degenerate, and has signature $(1,3)$. 
  An isometric isomorphism between $\left(\R^{1,3},\langle \cdot , \cdot \rangle_\textup{M}\right)$ and
  $\left(\hermMat(2),\langle \cdot , \cdot \rangle_{\mathfrak{h}}\right)$ is given by
  \[
     \sigma : \R^{1,3} \to \hermMat(2), \quad x \mapsto  \sigma \cdot x  = \sum_{k=0}^3 x^k \, \sigma_k \ .
  \]
  Its inverse maps $X \in \hermMat(2)$ to the vector $x$ with 
  components $x^k = \frac 12 \tr (X\sigma_k)$, where $k=0,1,2,3$. 
\end{proposition}

\begin{theorem}
  For every $g \in \LieSL(2,\C)$ the linear map 
  \[
    \pi_g : \R^4 \to \R^4 ,\quad 
    x \mapsto \sigma^{-1} \big( g  ( \sigma \cdot x ) g^* \big)
  \]
  is a proper orthochronous Lorentz  transformation. Moreover, the map
  \[
   \pi: \LieSL(2,\C) \to \LieSO^\uparrow (1,3), \quad g \mapsto \pi_g 
  \]
  is a differentiable surjective group homomorphism with kernel $\{ \pm I_2 \} \cong \Z/2$.
  In particular, the tangent map
  $T_\idnum \pi: \liesl (2,\C) \to \lieso (1,3)$ is an isomorphism and 
  $\pi$ is the universal covering map of $\LieSO^\uparrow(1,3)$. 
\end{theorem}

\begin{proof}
  Every element $g\in \LieSL(2,\C)$ induces a linear isomorphism 
  \[ \alpha_g : \hermMat(2) \to \hermMat(2), \quad X \mapsto gXg^*\]
  which is isometric since  $\det (\alpha_g X) =\det X$ for all $X \in \hermMat(2)$.
  By \Cref{thm:isomorphism-hermitian-two-by-two-matrices-minkowski-space},
  $\sigma : \R^{1,3} \to \hermMat(2)$ is an isometric isomorphism, hence
  $\pi_g = \sigma \circ \alpha_g \circ \sigma^{-1}$ leaves the Minkowski metric invariant
  and therefore is a Lorentz transformation. 
\end{proof}