% Copyright 2017-2019 Markus J. Pflaum, licensed under CC BY-NC-ND 4.0
% main author: 
%   Markus J. Pflaum 
%
\section{Wightman axioms}\label{sec:wightman-axioms}
\para The Wightman axioms were first introduced in the paper \cite{WigGarFOVDRQT}, and then explained in more detail 
in the textbooks \cite{JosGTQF} and \cite[Sec.~3.1]{StrWigPCTSS}. The latter is still the main reference for the 
axiomatic treatment of quantum field theory in the spirit of Wightman and G{\aa}rding.   
See also \cite[Sec.~8.3]{SchotMICFT} for a more modern formulation which we follow here. 
\begin{definition}
  A \emph{Wightman quantum field theory} of \emph{space-time dimension} 
  $D= d +1$, $d \in \N_{>0}$, consists of the following data:
  \begin{itemize}
   \item 
     the \emph{state space} of the theory given by the projective space 
     $\P (\hilbertH)$ associated to a separable complex Hilbert space $\hilbertH$,
   \item 
     a distinguished state $\omega_\circ = \C v_\circ\in \P (\hilbertH)$ called the \emph{vacuum state} 
     together with the choice of a normalized representing vector $v_\circ \in \hilbertH$ called 
     \emph{vacuum vector},
   \item 
     a unitary representation 
     $U: \ucover{\LieP^\uparrow_{+}}  (d+1)  \to \LieU (\hilbertH)$ of the universal cover 
     \[ \ucover{\LieP^\uparrow_{+}} (d+1) \cong \R^{d+1} \rtimes \ucover{\LieSO^\uparrow} (1,d) \]
     of the  proper orthochronous Poincar\'e group 
     $\LieP^\uparrow_{+} (d+1) = \R^{1+d} \rtimes \LieSO^\uparrow (1,d)$,
   \item
     and finally a family $(\Phi^j)_{1 \leq j \leq n}$, $n\in \N_{>0}$, of so-called
     \emph{field operators} 
     \[
        \Phi^j : \SchwartzFcts (\R^{d+1}) \to  \ulinOps (\hilbertH)
     \]
     which are defined on the Schwartz space of rapidly decreasing functions on $\R^n$ and map to
     the space of unbounded linear operators on the Hilbert space $\hilbertH$. 
  \end{itemize}
 These data are assumed to fulfill the following axioms, the so-called \emph{Wightman axioms}:
 \begin{axiomlist}[W]
 \item (\emph{Assumptions about the domain and the continuity of the field})\\
     There exists a dense linear subspace $\opDom \subset \hilbertH$ containing 
     $v_\circ$ such that $\opDom$ is contained in the domain of all the operators 
     $\Phi^j (f)$  and their adjoints  $\Phi^j (f)^*$, where
     $f \in \SchwartzFcts (\R^{d+1})$ and $j = 1,\ldots , k$.   
     Moreover, the unitary representation $U$ and the operators $\Phi^j (f)$  and $\Phi^j (f)^*$
     leave $\opDom$ invariant that is
     \[
       U (a, A) \opDom \subset \opDom, \quad \Phi^j (f) \opDom  \subset \opDom , 
       \: \Phi^j (f)^* \opDom  \subset \opDom 
     \]
     for all $(a,A) \in \ucover{\LieP^\uparrow_{+}}  (1,d)$, $f \in \SchwartzFcts (\R^{d+1})$ and $j = 1,\ldots , k$.
     Finally, for every $v\in \opDom$, $w \in \hilbertH$ and $j = 1, \ldots , n$ the maps 
     \[
        \SchwartzFcts (\R^{d+1}) \to \C,\: f \mapsto \langle w , \Phi^j (f) v\rangle  
     \]
     are tempered distributions.
 \item (\emph{Transformation law of the field})\\
   For all $(a,A) \in  \ucover{\LieP^\uparrow_{+}}  (d+1)$ and
   all $f \in \SchwartzFcts (\R^{d+1})$ the equation
   \[
     U(a,A)  \Phi^j (f) U(a,A)^{-1} = \sum_{k=1}^n  \varrho^{jk} \big( A^{-1} \big) \Phi^k ((a, A)f) 
   \]
   is valid over the domain $D$, where $\varrho : \ucover{\LieSO^\uparrow} (1,d) \to \LieGL(n,\C) $ is a finite 
   dimensional representation of the universal cover of the proper orthochronous Lorentz group 
   $\LieSO^\uparrow (1,d)$ and the action of $\ucover{\LieP} (d+1)$ on $\SchwartzFcts (\R^{d+1})$ is given by
   \begin{equation*}
     \begin{split}
       \ucover{\LieP}  (d+1) \times \SchwartzFcts (\R^{d+1}) & \to \SchwartzFcts (\R^{d+1}), \\
       \big( (a,A),f\big) & \mapsto (a,A)f = \Big(\R^{d+1}\ni x \mapsto f\big( A^{-1} (x-a)\big) \in \C \Big) \ .
     \end{split} 
   \end{equation*}
    
   
 \item (\emph{Local commutativity or microscopic causality})\label{axiom:wightman-axiom-microscopic-causality}\\
   If the support of test functions $f,g \in  \SchwartzFcts (\R^{d+1})$ is space-like separated that is if
   $f(x) \, g(y)=0$ for all $x,y\in \R^{d+1}$ with $\minkowskimetric{x -y,x-y} \geq 0$,
   then for all $j,k =1, \ldots , n$ the relation 
   \[
    [\Phi^j (f) , \Phi^k (g) ]_{-} =  [\Phi^j (f) , \Phi^j (g)^* ]_{-} = 0
   \]
   or the relation 
   \[
    [\Phi^j (f) , \Phi^k (g) ]_{+} = [\Phi^j (f) , \Phi^j (g)^* ]_{+} = 0 
   \]
   holds true over the domain $\opDom $. Hereby, $[S,T]_{-}$ denotes the \emph{commutator}
   \[
     [S,T]_{-} : \: \opDom \to \hilbertH, \quad v \mapsto S T v - T S v
   \] 
   and $[S,T]_{+}$  the \emph{anti-commutator}
   \[
     [S,T]_{+} : \: \opDom \to \hilbertH, \quad v \mapsto S T v + T S v
   \]   
   of two operators $S,T \in \ulinOps (\hilbertH)$ which are both assumed to 
   be defined on the domain $\opDom$ and to leave it invariant.  
 \item (\emph{Cyclicity of the vacuum vector})\label{axiom:cyclicity-vacuum-vector}\\ 
   The linear span of the set of all elements $v \in \hilbertH$ of the form 
   \[
      v = \Phi^{j_1} (f_1) \, \ldots \,  \Phi^{j_m} (f_m) v_\circ \ ,
   \]
   where $m \in \N$, $1 \leq j_1, \ldots , j_m \leq n$, and $f_1, \ldots , f_m \in \SchwartzFcts (\R^{d+1})$, 
   is dense in $\hilbertH$. 
 \end{axiomlist} 
\end{definition}

\begin{remarks}
\begin{letterlist}
\item
  The vacuum vector $v_\circ$ being normalized just means that $\| v_\circ \| = 1$.
  This implies that the vacuum state $\omega_\circ$ determines $v_\circ$  only up to a factor $z \in S^1 \subset \C$.
  The physically measurable quantities  of the quantum field theory such as expectation values or transition amplitudes
  do not depend on that choice.   
\item
  The field operators $\Phi^j$ are operator valued distributions. This reflects the fact 
  that only the ``smeared'' fields $ \Phi^j (f)$ can be interpreted physically as observable. The notation 
  $\Phi^j (x)$ for a field evaluated at a space-time point $x \in \R^{1,3}$ therefore does not make sense, neither 
  mathematically nor physically. Nevertheless it is often used for reasons of convenience, in particular in the physics 
  literature. The smeared field  $ \Phi^j (f)$ then is interpreted, again imprecisely, as the integral
  \[
     \Phi^j (f) = \int_{\R^{d+1}} f(x) \, \Phi^j(x) \, dx \ .
  \]
  We will avoid the notation of pointwise evaluated fields in the formulation of definitions and theorems, but 
  occasionally use it as a heuristic.

  For example, Axiom \ref{axiom:wightman-axiom-microscopic-causality} can heuristically be interpreted as saying that 
  the (anti-) commutation relations  
  \[
    [\Phi^j (x) , \Phi^k (y) ]_{\mp} =  [\Phi^j (x) , \Phi^j (y)^* ]_{\mp} = 0 
  \]
  hold true for $x,y \in \R^{1,d}$ space-like separated which means for the situation when 
  \[
   \minkowskimetric{x-y,x-y} < 0 \ . 
  \]
  \end{letterlist}
\end{remarks}




         
