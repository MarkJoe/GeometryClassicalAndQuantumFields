% Copyright 2017-2021 Markus J. Pflaum
% main author: 
%   Markus J. Pflaum 
%
\section{The geometry of projective Hilbert spaces}
\para
  Let $\hilbertH$ be a Hilbert space over the field $\fldK = \R$ or $=\C$. 
  The associated \emph{projective Hilbert space} $\P\hilbertH$ then is
  defined as the space of all \emph{rays} in $\hilbertH$ that is as the space 
  \[
    \P\hilbertH = \big\{ \linel \in \power{\hilbertH} \bigmid 
    \linel  \text{ is a } 
    1\text{-dimensional } \fldK\text{-linear subspace of }\hilbertH\big\} \ . 
  \]
  It carries a natural topology  which we now describe.
  Consider  $\hilbertH\setminus\{0\} $ with its subspace topology.
  Then one has a natural map 
  \[
    \pi : \hilbertH\setminus\{0\} \to \P\hilbertH ,\:  v \mapsto \fldK v
  \]
  which obviously is surjective.  One endows $\P\hilbertH$ with the final topology with respect to $\pi$.  
  Next let us introduce an equivalence relation $\sim$ on  $\hilbertH\setminus\{0\}$
  by defining $v \sim w$ if there exists a $\lambda \in \fldK^\times = \fldK\setminus \{0\}$ such that
  $v = \lambda w$. Obviously $\sim$ is reflexive, since $1\in\fldK^\times$, symmetric, since
  with $\lambda \in \fldK^\times$  the inverse $\lambda^{-1}$ is in $\fldK^\times$ as well, and transitive, 
  since the product of two elements of $\fldK^\times$ is in $\fldK^\times$. Hence $\sim$ is an equivalence 
  relation indeed.  Denote by $\hatv$ the equivalence class of an element $v\in \hilbertH\setminus\{0\}$. Let 
  $\hathilbertH$ be the quotient space $(\hilbertH\setminus\{0\}) / \!\sim$ and 
  $\hatpi : \hilbertH\setminus\{0\} \to \hathilbertH$ the quotient map. 
  

  
  \begin{lemma}
    The map $\pi : \hilbertH \setminus \{ 0 \} \to \P\hilbertH$ factors
    through a unique homeomorphism 
     $\kappa :  \hathilbertH \to \P\hilbertH$ which means that the diagram 
     \begin{displaymath}
     \begin{tikzcd}
        \hilbertH\setminus\{0\} \ar[rd,"\pi"] \ar[d,"\hatpi",swap] \\
        \hathilbertH \ar[r,"\kappa",swap] & \P\hilbertH 
     \end{tikzcd}
     \end{displaymath}
     commutes and that $\kappa$ is uniquely determined by this condition. 
  \end{lemma}
  \begin{proof}
     If $v \sim w$, then the lines through $v$ and through $w$ coincide, hence $\pi$ 
     factors through a unique continuous map $\kappa :  \hathilbertH \to \P\hilbertH$
     by the universal property of the quotient space. By surjectivity of $\pi$, $\kappa$ is surjective, too.
     By definition, $\kappa$ maps $\hatv$ to $\fldK v$, hence if $\fldK v = \fldK w$, then 
     $v$ and $w$ are linearly dependant, and $v \sim w$. So $\kappa$ is injective. 
     Continuity of the inverse $\kappa^{-1} : \P\hilbertH \to \hathilbertH$   
     is a consequence of the fact that $\P\hilbertH$ carries the final topology with respect to $\pi$. Uniqueness of $\kappa$ follows from $\hatpi$ being  surjective. 
  \end{proof}

  \begin{lemma}
    The projection map $\pi : \hilbertH \setminus \{ 0 \}\to \P\hilbertH$ and its restriction
    $\pi|_{\sphere\hilbertH} : \sphere\hilbertH \to \P\hilbertH$ to the sphere of $\hilbertH$ are open. 
  \end{lemma}
  \begin{proof}
    By the preceding lemma it suffices to show that  $\hatpi : \hilbertH \setminus \{ 0 \} \to \hathilbertH$ is open. 
    Let $U\subset \hilbertH \setminus \{ 0 \}$ be open. Then 
    \[
          \hatpi^{-1} \big( \hatpi (U) \big) = \bigcup_{\lambda \in \fldK^\times} \lambda \cdot U \ ,
    \]
    which is again open and the first part of the claim is proved. The second part follows in the same way, since
    \[
          \hatpi|_{\sphere\hilbertH}^{-1} \big( \hatpi|_{\sphere\hilbertH} (U) \big) = 
          \bigcup_{\lambda \in \sphere(\fldK)} \lambda \cdot U 
    \]
    is open for all $U \subset \sphere\hilbertH$ open.
  \end{proof}

\begin{remark}
  Strictly speaking, the projective space $\P\hilbertH$ depends on the ground field $\fldK$. 
  If $\hilbertH$ is a complex Hilbert space one therefore sometimes writes $\R\P\hilbertH$ or 
  $\C\P\hilbertH$ to denote that the projective space of all real respectively all complex lines is meant. 
  In this work we agree that for $\hilbertH$ complex  $\P\hilbertH$ always stands for the 
  projective space of complex lines in $\hilbertH$. If we want to consider the projective space of real
  lines in some complex Hilbert space $\hilbertH$ instead, we write $\R\P\hilbertH$. 
\end{remark}
 
\para
The inner product on the underlying Hilbert space $\hilbertH$ induces the \emph{projective inner product}  
or \emph{ray inner product}
\[ 
  \pinprod{\cdot,\cdot} : \P\hilbertH \times \P\hilbertH \to [0,1] , \:
     (\fldK v,\fldK w) \mapsto \pinprod{\fldK v,\fldK w} = \frac{|\inprod{v,w}| }{\| v \| \, \| w \| },
        \quad \text{where } v,w\in \hilbertH \setminus \{ 0\} ,
\]   
on the associated projective space.
Note that the projective inner product is well-defined, since $\frac{|\inprod{v,w}| }{\| v \| \, \| w \| }$ is homogeneous 
of degree $0$ both in $v$ and $w$.

Now we can formulate the first postulate of quantum mechanics. 
\begin{axiomlist}[QM]
\item  The \emph{state space} of a quantum mechanical system is accomplished by 
       a projective space $\P\hilbertH$ associated to a complex separable Hilbert 
       space $\hilbertH$. 
       The elements $v \in \hilbertH \setminus \{ 0 \}$ are called \emph{state vectors},
       the rays $ \linel  \in \P\hilbertH$  are the \emph{pure states}. 

       If a quantum mechanical system is prepared so that it is in the state 
       $ \linel \in \P\hilbertH$, the probability that a measurement detects the system  
       to be in the state $ \linek  \in \P\hilbertH $ is given by 
       the \emph{transition probability} $\pinprod{ \linek ,  \linel}^2$.  
\end{axiomlist} 

Because of their appearance in the first  postulate of quantum mechanics we want to study projective Hilbert spaces
in some more depth. We will use topological, geometric and analytic tools for that endeavor. A first result is the following. 

\begin{theorem}
  Let $\P\hilbertH$ be the projective space of a Hilbert space of dimension $\geq 2$ over the field $\fldK$ of 
  real or complex numbers. Then the following holds true:
  \begin{romanlist}
  \item\label{ite:complete-metrizability-projective-hilbert-space}
     The projective Hilbert space $\P\hilbertH$ is a completely metrizable topological space.
  \item\label{ite:complete-metric-projective-hilbert-space}
     A complete metric inducing the topology on $\P\hilbertH$ is given by 
     \[
        d : \P\hilbertH \times \P\hilbertH \to \R_{\geq 0}, \: ( \linek, \linel) \mapsto 
        \inf \left\{ \left\| v - w \right\| \bigmid v \in  \linek, \: w \in  \linel \: \& \:\|v\|=\|w\|= 1 \right\} \ .
     \]
  \item\label{ite:relation-metric-transition-amplitudes-projective-hilbert-space}
    The metric $d$ and the transition amplitudes satisfy the relation
    \begin{equation}
    \label{eq:relation-metric-projective-inner-product}
      d^2( \linek, \linel) =   2 \big( 1 - \pinprod{ \linek, \linel} \big)
      \geq  1 - \pinprod{ \linek, \linel}^2  \quad \text{for all } \linek, \linel \in \P\hilbertH \ .
    \end{equation}
  \item\label{ite:fubini-study-distance-projective-hilbert-space}
     The \emph{Fubini--Study distance} 
     \[
       d_\textup{FS} : \P\hilbertH \times \P\hilbertH \to \R_{\geq 0}, \: ( \linek, \linel) \mapsto 
       \arccos \pinprod{ \linek, \linel} 
     \] 
     is a metric on $\P\hilbertH$ which is equivalent to the metric $d$. More precisely
     \begin{equation}
     \label{eq:equivalence-metric-projective-space-fubini-study-distance}
       d ( \linek, \linel)\leq d_\textup{FS}  ( \linek, \linel) \leq \sqrt{2}  d ( \linek, \linel)
       \quad \text{for all } \linek ,  \linel \in \projH \ .
     \end{equation}
     The diameter of $\P\hilbertH$ with respect to the Fubini--Study distance equals
     $\frac \pi 2$. 
  \item\label{ite:projective-hilbert-space-operator-embedding}
     The mapping $P: \projH \to \blinOps(\hilbertH)$ which associates  to every ray $ \linek$
     the orthogonal projection onto it is a bi-Lipschitz embedding.  
     The \emph{gap metric}
     \[
       d_\textup{gap} : \P\hilbertH \times \P\hilbertH \to \R_{\geq 0}, \: ( \linek, \linel) \mapsto
       \left\| P( \linek) - P( \linel) \right\|
     \]
     obtained by restricting the operator norm distance  to $\projH$ is equivalent to $d$ and
     satisfies 
     \begin{equation}
     \label{eq:relation-metric-operator-norm-difference-projections}
       \frac{1}{\sqrt{2}} d( \linek, \linel) \leq d_\textup{gap} ( \linek, \linel) 
        = \sqrt{1 - \pinprod{ \linek, \linel}^2} \leq 
       d ( \linek, \linel) \quad \text{for all } \linek ,  \linel \in \projH \ .
     \end{equation}
  \end{romanlist}
\end{theorem}

\begin{proof}
\begin{adromanlist}
\item[{\itshape ad }\ref{ite:complete-metric-projective-hilbert-space}]
    Let us first show that the map $d$ is a metric indeed. 
    By definition, $d$ is non-negative and symmetric. Assume  $d( \linek, \linel)=0$ for two rays 
    $ \linek, \linel$. For given unit vectors $v \in  \linek$ and $w\in  \linel$ there then 
    exists a sequence $(\sigma_k)_{k\in \N} \subset \sphere^1$ such that 
    \[
      \lim_{k\to\infty} \norm{v -\sigma_kw } = 0 \ .
    \]
    By compactness of $\sphere^1$ we can assume that the sequence $(\sigma_k)_{k\in \N} $ converges
    after possibly passing to a subsequence. Let $\sigma \in \sphere^1$ be its limit. Then $\norm{v -\sigma w} =0$,
    hence  $ \linek =  \linel$. 
    Now let $ \linek, \linel, \linej \in \P\hilbertH$ and $z \in  \linej$ a representing unit vector. 
    Then 
    \begin{equation*}
      \begin{split}
        d( \linek, \linel) \, & = 
        \inf \left\{ \left\| v - w \right\| \bigmid v \in  \linek, \: w \in  \linel \: \& \:\|v\|=\|w\|= 1 \right\} \leq \\
        & \leq\inf\left\{\norm{v - z} + \norm{z - w } \bigmid v\in \linek, \: w \in \linel \:\&\:\|v\|=\|w\|= 1 \right\} =\\ 
        & =  \inf \left\{ \norm{v - z}  \bigmid v \in  \linek \: \& \:\|v\|= 1 \right\} +
      \inf \left\{  \norm{z - w } \bigmid  w \in  \linel\:  \& \: \|w\|= 1  \right\} = \\
        & = d( \linek, \linej) +  d( \linej, \linel) \ ,
      \end{split}
    \end{equation*}
    hence $d$ satisfies the triangle inequality, and therefore is a metric. 

    Next we prove that the metric topology of $d$ coincides with the quotient topology of $\pi$.  
    Let $v,w\in \sphere\hilbertH$. By definition of the metric $d$ one then has 
    \[
      d (\fldK v , \fldK w) \leq \norm{v-w}  \ .
    \]
    This implies that for all $\varepsilon >0$ 
    \[
       \pi \left( \ball_{\sphere\hilbertH} (v,\varepsilon) \right) \subset \ball_{\P\hilbertH} (\fldK v,\varepsilon) \ ,
    \] 
    where $\ball_{\sphere\hilbertH} (v,\varepsilon) $ denotes the $\varepsilon$-ball around $v$ in
    the sphere with respect to the norm and 
    $\ball_{\P\hilbertH} (\fldK v,\varepsilon)$ the  $\varepsilon$-ball  around $\fldK v$ 
    in the projective Hilbert space with respect to the metric $d$.
    Hence the quotient topology on $\P\hilbertH$ is finer than the metric topology. 
    If for  given $\varepsilon >0$ a $\delta >0$ is chosen so that $\delta < \varepsilon$, then 
    for every ray $ \linel$ with $ d(\fldK v ,  \linel ) < \delta$ there exists an element 
    $w \in  \linel \cap \sphere\hilbertH$ such that $\norm{v-w} < \epsilon$ which means that
    $ \linel = \pi (w) \in \pi \big( B (v,\varepsilon)\big)$. Hence
    \[
       \ball_{\P\hilbertH} (\fldK v,\delta) \subset  \pi \left( \ball_{\sphere\hilbertH} (v,\varepsilon) \right)
    \] 
    and the quotient topology  on $\P\hilbertH$ is coarser than the metric topology. So $d$   
    induces the topology on $\P\hilbertH$ as claimed.  
    
    It remains to verify that $d$ is a complete metric. To this end observe first that for every 
    $v \in \sphere\hilbertH$ and ray $ \linel$ there exists a representative 
    $w \in  \linel \cap \sphere\hilbertH$ such that $\inprod{v,w} = \pinprod{\fldK v, \linel}$. 
    We will call such a representative of $ \linel$  \emph{distinguished with respect to} $v$. 
    Now let $( \linel_n)_{n\in \N}$ be a Cauchy sequence of rays. Then there exists an increasing sequence
    of natural numbers $n_0 < \ldots < n_k < n_{k+1} < \ldots $ such that 
    \[
       d(  \linel_n ,  \linel_m) < \frac{1}{2^{k+1}} \quad \text{for all } n,m \geq n_k \ .
    \]
    Choose  a representative 
    $v_0 \in  \linel_{n_0} \cap \sphere\hilbertH$ and let $v_1\in \sphere\hilbertH$ be a
    representative of $ \linel_{n_1}$ distinguished with respect to $v_0$. Then
    \[
       \norm{v_1-v_0} = \sqrt{2(1-\Re \inprod{v_0,v_1})} =  \sqrt{2(1- \pinprod{ \linel_{n_0}, \linel_{n_1}})} = d(  \linel_{n_0}, \linel_{n_1} ) < \frac 12 \ . 
    \]
    Now assume we have constructed $v_0, \ldots , v_k \in \sphere\hilbertH$ such that 
    $\fldK v_l =  \linel_{n_l}$ for $l=0,\ldots , k$ and such that for $l=0,\ldots , k- 1$
    \begin{equation}
    \label{eq:representative-sequence-recusrion-condition}
      \norm{v_{l+1}-v_l} <  \frac{1}{2^{l+1}} \ . 
    \end{equation} 
    Let $v_{k+1}\in \sphere\hilbertH$ be a representative of $ \linel_{n_{k+1}}$ distinguished with respect to $v_k$.
    Then
    \[
      \norm{v_{k+1}-v_k}  = \sqrt{2(1-\Re \inprod{v_{k+1},v_k})} =  \sqrt{2(1- \pinprod{ \linel_{n_{k+1}}, \linel_{n_k}})} = d(  \linel_{n_{k+1}}, \linel_{n_k} )   <  \frac{1}{2^{k+1}} \ . 
    \]
    We thus obtain a sequence $(v_k)_{k\in \N}$ in $\hilbertH$ such that 
    \eqref{eq:representative-sequence-recusrion-condition} is fulfilled for all $l\in \N$. The sequence 
    $(v_k)_{k\in \N}$ is even a Cauchy sequence since for $n\geq m \geq k$ 
    \[
       \norm{v_n-v_m} \leq \sum_{k=m}^{n-1} \norm{v_{k+1} -v_k} < \sum_{k=m}^{n-1}  \frac{1}{2^{k+1}}  
       < \frac{1}{2^m} \ . 
    \]
    Let $v \in \hilbertH$ be its limit. Then 
    \[
      \lim_{k\to \infty} d(\fldK v ,  \linel_{n_k}) \leq \lim_{k\to \infty} \norm{v-v_k} = 0 \ .
    \]
    Hence the sequence of rays $( \linel_n)_{n\in \N}$ converges to the ray $\fldK v$ and $\P\hilbertH$ 
    is complete with respect to the metric $d$. Claim \ref{ite:complete-metrizability-projective-hilbert-space}
    is now proved as well. 
\item[{\itshape ad }\ref{ite:relation-metric-transition-amplitudes-projective-hilbert-space}]
    Let $ \linek, \linel$ be rays in $\hilbertH$ and $v\in  \linek$, $w\in  \linel$ representing unit vectors. 
    Let $\lambda \in \sphere^1 $ such that $\inprod{v,w} = \lambda \pinprod{ \linek, \linel} $
    and  $\sigma \in \sphere^1$ arbitrary.  
    Then compute 
    \[
      \norm{ v - \sigma w }^2 = 2 \big( 1 - \Re \inprod{v, \sigma w}  \big) 
      = 2 \big( 1 - \pinprod{ \linek, \linel}  \Re  \, \overline{\sigma} \lambda \big) \geq
       2 \big( 1 - \pinprod{ \linek, \linel}  \big) \ . 
    \]
    For $\sigma = \lambda$, equality holds, hence
    \[
      d^2 ( \linek, \linel) = 
      \inf \left\{ \norm{v - \sigma w}^2 \bigmid \sigma \in \sphere^1 \right\} =  
      2 \big( 1 - \pinprod{ \linek, \linel}  \big) \ . 
    \]
    With $ \linek, \linel,v,w$ as before and $\delta = d( \linek, \linel) $, the claimed inequality now
    follows immediately: 
    \[
      d^2( \linek, \linel) \geq \delta^2\left( 1 -\frac 14 \delta^2 \right) = 2 \big( 1 - \pinprod{ \linek, \linel}  \big)  
      \left( 1 - \frac 12 \big(  1 - \pinprod{ \linek, \linel}  \big)\right) = 1 - \pinprod{ \linek, \linel}^2 \ . 
    \]

\item[{\itshape ad }\ref{ite:fubini-study-distance-projective-hilbert-space}]
   The map $d_\textup{FS}$ is symmetric by symmetry of the projective inner product. By the assumption $\dim \hilbertH \geq 2$,
   the image of $\pinprod{\cdot,\cdot}$ is the whole interval $\closedint{0,1}$, since $\P\hilbertH$ is connected,  
   $\pinprod{\cdot,\cdot}$ is bounded by $1$, $\pinprod{ \linel, \linel} = 1$ for every ray $ \linel$ and since there exist orthogonal rays. The image of $d_\textup{FS}$ therefore  coincides with $\closedint{0,\frac \pi 2}$ which 
   already entails the claim about the diameter. By strict monotony of  $\arccos$, $d_\textup{FS}( \linek, \linel) = 0$ if and only if
   $\pinprod{ \linek, \linel} = 1$. By \eqref{eq:relation-metric-projective-inner-product} this is the case if and only if 
   $d  ( \linek, \linel) = 0$ which means if and only if $ \linek =  \linel$. Let us now show that $d_\textup{FS}$ satisfies the triangle 
   inequality. To this end let $ \linek, \linel, \linej$ be rays in $\hilbertH$.  If the Fubini--Study distance between any two of 
   these rays is zero, the triangle inequality obviously holds true, so we exclude that case. 
   Choose representatives $v\in  \linek$, $w \in  \linel$, $z\in  \linej$ such that all have norm $1$.
   After possibly multiplying $v$ and $z$ by elements of $\sphere^1 \cap \fldK$  one can achieve that  
   \[
       \inprod{v,w} = \pinprod{ \linek, \linel} \quad\text{and}\quad  \inprod{w,z} = \pinprod{ \linel, \linej}  \ .
   \]  
   Let $\theta = \arccos \inprod{v,w}$ and $\varphi = \arccos \inprod{w,z} $. 
   Then $\theta = d_\textup{FS} ( \linek, \linel)$ and $\varphi = d_\textup{FS} ( \linel, \linej)$. 
   Now let $x$ be a unit vector in the plane through $v$ and $w$ which is orthogonal to $w$ and $y$ a unit vector in
   the plane through $w$ and $z$ which is orthogonal to $w$. After possibly multiplying $x$ and $y$ by elements of
   $\sphere^1 \cap \fldK$ one can achieve that $\inprod{v,x},  \inprod{z,y} \in [0,1]$. 
   Then
   \[
      v = \inprod{v,w} \, w + \inprod{v,x}  \, x \quad\text{and}\quad  z = \inprod{z,w} \, w + \inprod{z,y} \, y \ .
   \]
   By $\theta, \varphi  \in \left[0,\frac\pi 2\right]$ and  $\inprod{v,x},  \inprod{z,y} \geq 0$ one concludes
   \[
      v = \cos \theta \: w + \sin \theta \: x  \quad\text{and}\quad  z = \cos \varphi \: w + \sin \varphi \: y \ .
   \]
   Hence, by the triangle inequality for the absolute value and the Cauchy--Schwarz inequality 
   \[
    \left| \inprod{v,z} \right| =  \left| \cos \theta \: \cos \varphi +  \sin \theta \: \sin \varphi \: \inprod{x,y}\right|
    \geq \cos \theta \: \cos \varphi -  \sin \theta \: \sin \varphi = \cos (\theta + \varphi) \ . 
   \]
   Since $\arccos$ is monotone decreasing, one obtains
   \[
     d_\textup{FS} ( \linek, \linej) = \arccos \left| \inprod{v,z} \right| \leq 
     \theta + \varphi = d_\textup{FS} ( \linek, \linel) + d_\textup{FS} ( \linel, \linej) \ . 
   \]
   So the Fubini--Study distance satisfies the triangle inequality and is a metric indeed.  
   
   Last we need to prove that the Fubini--Study distance is equivalent to $d$. 
   To this end consider the functions 
   \[
     f : \closedint{0,\sqrt{2}} \to \R , \: s \mapsto \arccos\left( 1 -\frac{s^2}{2} \right) \quad\text{and}\quad 
     g: \closedint{0,\frac{\pi}{2}} \to \R , \:  t \mapsto \sqrt{2( 1 -\cos t) } \ .
   \] 
   Then both functions are continuous and differentiable on the interior of
   their domains. Now observe that $f(0)=g(0)=0$ and compute
   \[
     f'(s) = \frac{s}{\sqrt{1-\left(1-\frac{s^2}{2}\right)^2}} = \frac{s}{\sqrt{s^2-\frac{s^4}{4}}} = \frac{2}{\sqrt{4-s^2}}
     \leq \sqrt{2} \quad\text{for } s\in \openint{0,\sqrt{2}}  
   \]
   and 
   \[
     g'(t) = \frac{\sqrt{2}}{2} \, \frac{\sin t}{\sqrt{1-\cos t}} = \frac{\sqrt{2}}{2} \, \sqrt{1+\cos t} \leq 1
     \quad\text{for } t\in \openint{ 0,\frac{\pi}{2}} \ .
   \]
   By definition of $d_\textup{FS}$ and \eqref{eq:relation-metric-projective-inner-product}, the mean-value theorem then entails
   \[
      d( \linek, \linel) = g \left( d_\textup{FS} ( \linek, \linel) \right) \leq  d_\textup{FS} ( \linek, \linel)  = 
      f \left( d ( \linek, \linel) \right) \leq \sqrt{2} \, d( \linek, \linel) \quad \text{for all } 
       \linek, \linel \in \P\hilbertH \ .
   \] 
   Hence the estimate \eqref{eq:equivalence-metric-projective-space-fubini-study-distance}
   is proved and the metrics $d$ and $d_\textup{FS}$ are equivalent. 
   \item[{\itshape ad }\ref{ite:projective-hilbert-space-operator-embedding}]
   Recall that the operator norm distance of  $P( \linek)$ and $P( \linel)$ is given by 
   \begin{equation}
     \label{eq:definition-operator-norm-difference-projections}
     \left\| P( \linek) - P( \linel) \right\| = \sup\left\{ \left\| 
    \big( P( \linek) - P( \linel)\big)z\right\| \bigmid  z \in \sphere\hilbertH\right\} \ . 
   \end{equation}
   Choose normalized representatives $v \in  \linek$ and  $w\in  \linel$. After 
   possibly multiplying $w$ by a complex number of modulus $1$ we can assume that 
   $\inprod{v,w} = \pinprod{ \linek, \linel} \geq 0$. If $\inprod{v,w} =1$ 
   or in other words if  $v$ and $w$ are linearly dependent then  $ \linek$ and $ \linel$ coincide 
   and the claim is trivial, so we assume that $v$ and $w$ are linearly independent. 
   First we want to show that 
   \begin{equation}
     \label{eq:estimate-operator-norm-difference-projections-normalized-vectors}
     \left\| \big( P( \linek) - P( \linel)\big)z \right\| \leq 1 - \left|\inprod{v,w}\right|^2
     \quad \text{for all } z \in \sphere\hilbertH \ .
   \end{equation}
   To this end expand $z = z^\parallel + z^\perp$, where 
   $z^\parallel$ lies in the plane spanned by $v$ and  $w$ and $z^\perp$ is perpendicular to that
   plane. Then 
   \[
      \big( P( \linek) - P( \linel)\big)z =   \inprod{z,v} v  - \inprod{z,w} w =
      \inprod{z^\parallel,v} v  - \inprod{z^\parallel,w} w = \big( P( \linek) - P( \linel)\big)z^\parallel \ .
   \]
   Hence it suffices to verify \eqref{eq:estimate-operator-norm-difference-projections-normalized-vectors} for 
   $z \in \sphere\hilbertH \cap \linSpan (v,w)$. Observe that there exist unique elements
   $ \varphi \in [0, \frac \pi 2]$ and  $\mu\in \sphere^1\cap \fldK $ such that 
   $\inprod{z,v} = \overline{\mu} \cos \varphi$. One can then find a normalized vector $w^\perp \in \linSpan (v,w)$ 
   perpendicular to $v$ such that 
   \[
     \mu z = \cos \varphi \, v + \sin \varphi \, w^\perp \ . 
   \]
   Note that with this 
   \[
     w = \inprod{v,w} v + \inprod{w,w^\perp} w^\perp \quad\text{and}\quad 
     |\inprod{w,w^\perp}|^2 = 1 - |\inprod{v,w}|^2 \ .
   \]
   
   Now compute
   \begin{equation*}
     \begin{split}
       \big\| \big( P( \linek) & - P( \linel)\big)z \big\|^2  =  
       \left\| \big( P( \linek) - P( \linel)\big)\mu z \right\|^2 = 
       \left\|  \inprod{z,v} v  - \inprod{z,w} w \right\|^2 = \\
        = \, & |\inprod{\mu z,v}|^2 - 2 \inprod{v,w} \, \Re \left( \inprod{\mu z,v} \inprod{\mu z,w} \right) + |\inprod{\mu z,w}|^2 = \\
        = \, & \cos^2 \! \varphi -2  \cos \varphi \, \inprod{v,w}\left( \cos \varphi \,  \inprod{v,w}  
         +  \sin \varphi \, \Re \inprod{w,w^\perp} \right) \, \\ 
         & +  \cos^2 \! \varphi \, |\inprod{v,w}|^2 + 2  \cos \varphi \, \inprod{v,w} \, \sin \varphi \, 
         \Re \inprod{w,w^\perp}  + \sin^2 \! \varphi \, |\inprod{w,w^\perp}|^2  = \\
        = \, &  1 -   |\inprod{v,w}|^2   \ . 
     \end{split}
   \end{equation*}
   This proves \eqref{eq:estimate-operator-norm-difference-projections-normalized-vectors}, but also implies
   by \eqref{eq:definition-operator-norm-difference-projections} that  
   \[
    \left\| P( \linek) - P( \linel) \right\|^2 =  1 -   |\inprod{v,w}|^2 = 1 - \pinprod{ \linek, \linel}^2 \ .
   \]
   The claim now follows by \ref{ite:relation-metric-transition-amplitudes-projective-hilbert-space}
   and the theorem is proved.
\end{adromanlist}
\end{proof}


After having examined some topological properties we come now to 
the  geometry of projective Hilbert spaces.

\begin{theorem}\label{thm:differential-geometry-projective-hilbert-space} 
  The projective Hilbert space $\P\hilbertH$ of a Hilbert space of dimension $\geq 2$ over $\fldK$ 
  has the following differential geometric properties:
\begin{romanlist}
  \item\label{ite:hilbert-manifold-structure-projective-hilbert-space}
      $\P\hilbertH$  carries a natural structure of an analytic manifold modelled on a Hilbert space
      isomorphic to each of the Hilbert spaces $\vectorspV_w = (\fldK w)^\perp$, where $w \in \hilbertH$ is a unit vector. 
  \item \label{ite:fiber-bundle-stzructure-sphere-hilbert-space}
      Let $\sphere\hilbertH \subset \hilbertH\setminus \{ 0 \}$ be the sphere in $\hilbertH$. Then the restriction 
      \[ \pi|_{\sphere\hilbertH} : \sphere\hilbertH \to \P\hilbertH,\: v \mapsto \fldK v  \]
      is a real analytic fiber bundle  with typical fiber $\sphere^1$ in the complex case and typical fiber
      $\Z/2$ in the real case.
  \item 
      Endow $\sphere\hilbertH$ with the riemannian metric $g$ inherited from the ambient Hilbert space.
      Then there exists a unique riemannian metric  $g_\textup{FS}$ on $\P\hilbertH$ such that 
      $\pi|_{\sphere\hilbertH} : \sphere\hilbertH \to \P\hilbertH$ becomes a riemannian submersion. 
      This metric is called the \emph{Fubini--Study} metric. Its geodesic distance coincides with the 
      Fubini--Study distance $d_\textup{FS}$. 
  \item 
      In case $\hilbertH$ is a complex Hilbert space, the projective space $\P\hilbertH$ carries in a natural way the structure 
      of a K\"ahler manifold. Its complex structure is the one inherited from $\hilbertH$, and its riemannian metric
      is the Fubini--Study metric.   
\end{romanlist}  
\end{theorem}

\begin{proof}
\begin{adromanlist}
\item[{\itshape ad }\ref{ite:hilbert-manifold-structure-projective-hilbert-space}]\,
     For a given unit vector $w\in \sphere\hilbertH$ consider the linear form $w^\flat: \hilbertH \to\fldK$, $v \mapsto \langle v,w \rangle$.
     Let $\vectorspV_w = \ker w^\flat = (\fldK w)^\perp$ and $U_w = \pi \big( \hilbertH \setminus \vectorspV_w \big)$. Then, by 
     Theorem \ref{thm:orthogonal-decomposition-theorem},
     one has the orthogonal decomposition $\hilbertH = \vectorspV_w \oplus \fldK w$ which gives rise to the orthogonal projection 
     $\pr_{\vectorspV_w} : \hilbertH \to \vectorspV_w$. Next observe that $U_w \subset \P\hilbertH$ is open since 
     $\pi^{-1} (U_w)  = \hilbertH \setminus \vectorspV_w$ is open and $\P\hilbertH$ carries the quotient topology with respect to $\pi$.
     Now we can define a chart $h_w : U_w \to \vectorspV_w$ by 
     \[
       h_w (\fldK v) = \pr_{\vectorspV_w} \left( \frac{v}{\langle v,w \rangle} \right) = \frac{v}{\langle v,w \rangle} - w \quad \text{for } v \in  \hilbertH \setminus \vectorspV_w \ .
     \]
     The map $h_w$ is well-defined since $\langle v ,w \rangle \neq 0$ for all $v \in \hilbertH \setminus \vectorspV_w$ and since $\frac{v}{\langle v,w \rangle} = \frac{\lambda v}{\langle \lambda v,w \rangle} $ for all
     $\lambda\in \fldK^\times$. Moreover, $h_w$ is continuous by continuity of  the composition $h_w \circ \pi|_{\hilbertH \setminus \vectorspV_w}$. 
     If $h_w (\fldK v) = h_w (\fldK v')$, then 
     \[ 
        \pr_{\vectorspV_w} \left( \frac{v}{\langle v,w \rangle} - \frac{v'}{\langle v',w \rangle} \right) = 0
        \quad\text{and}\quad
        \left\langle \frac{v}{\langle v,w \rangle} - \frac{v'}{\langle v',w \rangle} , w \right\rangle = 0 ,
     \]
     hence $\fldK v = \fldK v'$, so $h_w$ is injective.   
     The map $\vectorspV_w \to  U_w$, $y \mapsto \pi ( y +w ) $ is obviously continuous 
     and inverse to $h_w$ since $h_w \big( \pi ( y +w ) \big) = y$ for all $y \in \vectorspV_w$ 
     and since $h_w$ is injective. So we have proved that $h_w : U_w \to \vectorspV_w$ is a homeomorphism. 
      
     Next observe that all the  Hilbert spaces $\vectorspV_w$, $w \in \sphere\hilbertH$ are  pairwise isomorphic 
     since each of them has codimension $1$ in $\hilbertH$. 
     After this observation we show that for all $v,w \in \sphere\hilbertH$ 
     \begin{equation}
     \label{eq:intersected-domains-projective-hilbert-charts}
         h_w (U_w \cap U_v)  = \vectorspV_w \setminus \left( - \pr_{\fldK v} w + \vectorspV_w \cap \vectorspV_v  \right) \ .
     \end{equation}
    Assume that $ y \in \vectorspV_w$. The relation
    $ v \notin \left( - \pr_{\fldK v} w + \vectorspV_w \cap \vectorspV_v  \right) $ then is equivalent to 
    $\pr_{\fldK v} (y + w) \neq 0$, which on the other hand is equivalent to the existence of 
    some $\lambda \in \fldK^\times$ and $x \in \vectorspV_v$ such that  $y+w = \lambda (x + v)$. 
    Since  $h_w^{-1} (y) = \pi (y + w)$, the latter is equivalent to the existence of an 
    $x \in \vectorspV_v$ such that $h_w^{-1} (y) = \pi (x+v)$.
    But that is equivalent to $h_w^{-1} (y) \in U_w \cap U_v$. 
    This proves \eqref{eq:intersected-domains-projective-hilbert-charts}.

    The transition map between the chart $h_w$ and the chart $h_v$ is now given by 
    \begin{equation*}
    \begin{split}
      h_v \circ h_w^{-1} :  
      \vectorspV_w \setminus \left( - \pr_{\fldK v}  w + \vectorspV_w \cap \vectorspV_v  \right)  \to 
      \vectorspV_v \setminus \left( - \pr_{\fldK w}  v + \vectorspV_w \cap \vectorspV_v  \right) ,\,
       y  \mapsto  \pr_{\vectorspV_v} \frac{y + w}{\langle y+w , v \rangle} \ .
    \end{split}
    \end{equation*}
    But this map is analytic as a composition of analytic maps, hence any two charts 
    are $\shC^\omega$-compatible. 
    Since $\P\hilbertH$ is obviously covered by the open domains $U_w$, $w \in \sphere\hilbertH$, 
    the projective Hilbert space  $\P\hilbertH$ becomes an analytic manifold locally modelled on a
    Hilbert space isomorphic to each of the $\vectorspV_w$, $w \in \sphere\hilbertH$.
\item[{\itshape ad }\ref{ite:fiber-bundle-stzructure-sphere-hilbert-space}] \,
  Fix a unit vector $w\in \sphere\hilbertH$, let $\vectorspV_w = (\fldK w)^\perp$ as before and
  and put
  \[
      \widetilde{\vectorspV}_w =
      \begin{cases}
        \vectorspV_w & \text{if } \fldK =\R \ , \\
        \vectorspV_w \oplus  \cplxi \R w  & \text{if } \fldK =\C \ .
      \end{cases}
  \] 
  Then $\widetilde{\vectorspV}_w$ is the orthogonal complement of the real line $\R  w$
  with respect to the real inner product $\Re \inprod{-,-}$ on $\hilbertH$. 
  Hence any vector $v \in \hilbertH$ can be uniquely
  represented in the form $v = v_0 w + \hat{v} $ where
  $v_0 = \Re \inprod{ v,w} \in \R$ and  $\hat{v}  = \pr_{\widetilde{\vectorspV}_w} (v)\in \widetilde{\vectorspV}_w $.
  Put $N_w = \sphere \hilbertH \setminus \{ - w \}$.
  The stereographic projection
  \[
    g_w : N_w \to \widetilde{\vectorspV}_w , \: v \mapsto \frac{2}{1+v_0} \hat{v}
  \]
  then is a chart for $\sphere\hilbertH$ with inverse
  \[
    g_w^- : \widetilde{\vectorspV}_w  \to N_w, \: z \mapsto
    \frac{1}{4+\| z\|^2} \left( (4-\| z\|^2)w + 4 z\right)  \ .
  \]
  Since $\frac{4-r}{4+r} > -1$ for all $r\geq 0$ and
  \[
    \| g_w^- (z) \|^2 = \frac{1}{(4+\| z\|^2)^2} \left( (4-\| z\|^2)^2 + (4\| z\|)^2\right) = 1
     \quad \text{for all } z \in  \widetilde{\vectorspV}_w \ ,
  \]
  the map $g_w^-$ has image in $N_w$, indeed.
  Moreover, for $z \in  \widetilde{\vectorspV}_w$,
  \[
    g_w \circ g_w^- (z) = \frac{2}{1+\frac{4-\| z\|^2}{4+\| z\|^2}} \, \frac{4}{4+\| z\|^2} z = z
  \]
  and for $v \in N_w$ by application of the equality $|v_0|^2 + \| \hat{v}\|^2 =1 $, 
  \begin{align}
    g_w^- \circ g_w (v) & = g_w^2 \left( \frac{2}{1+v_0} \hat{v} \right) =
    \frac{1}{4+\frac{4}{(1+v_0)^2}\|\hat{v}\|^2} \left( 4-\frac{4}{(1+v_0)^2}\|\hat{v}\|^2 w + \frac{8}{1+v_0}\hat{v} \right) =  \nonumber \\
    & = \frac{1}{(1+v_0)^2+\|\hat{v}\|^2}\left(\left( (1+v_0)^2-\|\hat{v}\|^2\right) w + 2(1+v_0)\hat{v}\right) = \nonumber \\
    & = \frac{1}{2(1+v_0)}\left( 2 v_0 (1+v_0) w + 2(1+v_0)\hat{v}\right) = v_0 w + \hat{v} = v \ . \nonumber
  \end{align}
  Therefore,  $g_w$ are $g_w^-$ mutually inverse as claimed. Observe that for $v \in \sphere \hilbertH \setminus  \{ w \}$ the transition map
  $g_w \circ g_v^- :  \widetilde{\vectorspV}_v \setminus \{ g_v ( -w )\} \to  \widetilde{\vectorspV}_w \setminus \{ g_w ( -v )\}$
  is given by % real analytic since it maps $ z \in \widetilde{\vectorspV}_v \setminus \{ g_v ( -w )\}$ to 
  \begin{align}
   z & \mapsto  g_w  \left( \frac{1}{4+\| z\|^2} \left( (4-\| z\|^2)v + 4 z\right)  \right) = \nonumber \\
    &  =  \frac{2}{1 + \frac{1}{4+ \|z\|^2} \left(  \Re \inprod {(4- \|z\|^2)v + 4 z,w} \right)}
         \pr_{\widetilde{\vectorspV}_w} \left( \frac{1}{4+\| z\|^2} \left( (4-\| z\|^2)v + 4 z\right)  \right) = \nonumber \\
   % = & \frac{2}{1 + \frac{1}{4+ \|z\|^2} \left(  \Re \inprod {(4- \|z\|^2)v + 4 z,w} \right)}
   %  \frac{1}{4+\| z\|^2} \left( (4-\| z\|^2)v + 4 z  - \Re \inprod{ (4-\| z\|^2)v + 4 z, w}w \right) \nonumber \\
   &  =  \frac{2}{4+ \|z\|^2 + \Re \inprod {(4- \|z\|^2)v + 4 z,w} }
      \left( (4-\| z\|^2)v + 4 z  - \Re \inprod{ (4-\| z\|^2)v + 4 z, w}w \right)  \ ,\nonumber
  \end{align}
  which is real analytic. Since the open sets $N_w$ with $w \in \sphere\hilbertH$ cover the sphere $\sphere\hilbertH$ it thus becomes
  a real analytic manifold modelled on a possibly infinite dimensional real Hilbert space.
  Now consider the composition
  \[
    \widetilde{\vectorspV}_w \setminus 2 \sphere \vectorspV_w \to \vectorspV_w , \quad
    z \mapsto h_w \circ \pi \circ g_w^- (z) = \pr_{\vectorspV_w} \left(
    \frac{(4-\| z\|^2)w + 4 z}{4-\| z\|^2 + 4 \inprod{z,w}} \right) =
    \frac{4\left( z - \inprod{z,w}w \right)}{4-\| z\|^2 + 4 \inprod{z,w}}  \ .
  \]
  This is a real analytic map for every $w \in \sphere\hilbertH$, so
  $\pi|_{\sphere\hilbertH}$ is real analytic. Let us show that it is a principal fiber bundle.
  To this end put $G =\Z/2$ in the real case and $G = \sphere^1$ in the complex case and
  note that $G$ acts smoothly on $\sphere\hilbertH $ by scalar multiplication.
  Since $G$ is abelian, we can write this also as a right action
  $\cdot : \sphere \hilbertH \times G \to \sphere\hilbertH$. By definition of the projective Hilbert space
  this right action is free and transitive on the fibers of the projection
  $\pi :\sphere\hilbertH \to \P\hilbertH$ which therefore are homeomorphic to $G$.
  For each $w\in \sphere\hilbertH$ the map
  \[
    f_w :
    \sphere\hilbertH \setminus \vectorspV_w \to U_w \times G \subset \P\hilbertH   \times G , \:
    v \mapsto \left( \fldK v , \frac{\inprod{v,w}}{| \inprod{v,w} |}   \right)
  \]
  now is a bundle trivialization as the following argument shows.
  By construction, $f_w $ is real analytic with inverse
  \[
    f_w^- : U_w  \times G \to \sphere\hilbertH \setminus \vectorspV_w , \:
    (\fldK v ,  \lambda ) \mapsto \lambda \frac{h_w (\fldK v) + w}{\| h_w (\fldK v) + w \| } \ .
  \]
  Indeed, $f_w$ is obviuously surjective and 
  \[
    f_w^- \circ f_w (v) =    \frac{\inprod{v,w}}{| \inprod{v,w} |} \frac{h_w (\fldK v) + w}{\| h_w (\fldK v) + w \| }
    = \frac{\inprod{v,w}}{| \inprod{v,w} |} \frac{ \frac{v}{\inprod{v,w}} }{ \frac{\| v \| }{|\inprod{v,w}|} }  = v 
    \quad \text{for all }  v \in \sphere\hilbertH \setminus \vectorspV_w  \ . 
  \]
  Observe that $\pr_2 f_w (v \cdot \lambda) = (\pr_2 f_w (v))\lambda$ for all
  $v \in \sphere\hilbertH \setminus \vectorspV_w$ and $\lambda \in G$, where $\pr_2$
  denotes projection onto the second coordinate.
  Finally note that for $v,w  \in \sphere\hilbertH $ and
  $z \in \sphere\hilbertH \setminus (\vectorspV_v \cup \vectorspV_w )$,  
  \[
    f_v \circ f_w^- (\fldK z , \lambda) =
    f_v \left( \lambda \frac{h_w (\fldK z) + w}{\| h_w (\fldK z) + w \| } \right) 
    = \left( \fldK z  , \lambda\frac{\inprod{z,v}}{\inprod{z,w}} \right)
    = \left( \fldK z  , \lambda \right) \cdot \frac{\inprod{z,v}}{\inprod{z,w}} \ ,
  \]
  where $\cdot : (\P\hilbertH \times G) \times G \to \P\hilbertH \times G$
  denotes the right action $((\linel,\mu),\lambda) ) \mapsto ( \linel,\mu) \cdot\lambda =
  (\linel,\mu \lambda)$. Hence $\pi|_{\sphere\hilbertH}: \sphere\hilbertH \to\P\hilbertH$ is a real analytic
  $G$-principal bundle with local trivializations $f_w$, $w\in \sphere\hilbertH$. 
\end{adromanlist}
\end{proof}

\begin{remark}
\label{rem:charts-finite-dimensional-projective-space}
   Notice that the chart $h_w$ in the proof of \ref{ite:hilbert-manifold-structure-projective-hilbert-space}
   can be written as 
   \[ h_w(\fldK v) = \dfrac{v}{\inprod{v,w}} - w \ . \]
   This is the same as for the charts of finite dimensional projective space $\fldK\P^n$. Indeed, we can choose $w$ as a 
   basis element, say $e_k$, $k= 0,\ldots ,n$ and we have a line 
   \[ [v_0:\ldots :v_k:\ldots :v_n] \in \fldK\P^n \]
   represented by the vector $v= (v_0,\ldots ,v_k,\ldots ,v_n)$, where $v_k\neq 0$.
   Then the standard chart is obtained as follows. First normalize the vector representing the line in the 
   $k$-th coordinate, i.e. divide by $\inprod{v,w}$: 
   \[ \left[ \frac{v_0}{v_k}:\ldots :1:\ldots :\frac{v_n}{v_k} \right] \ ,\]
   and then map this to $\fldK^n$ via  dropping the $1$ in the $k$-th coordinate: 
   \[ \left[\frac{v_0}{v_k}:\ldots :1:\ldots :\frac{v_n}{v_k}\right] \mapsto 
      \left(\frac{v_0}{v_k},\ldots ,\frac{v_{k-1}}{v_k},\frac{v_{k+1}}{v_k},\ldots ,\frac{v_n}{v_k}\right) \ .
   \]
\end{remark}
