% Copyright 2017 Markus J. Pflaum, licensed under CC BY-NC-ND 4.0
% main author: 
%   Markus J. Pflaum 
%
\section{Jet bundles}
\label{sec:jet-bundles}
\para 
Let us fix in this section a smooth finite dimensional fiber bundle 
$\pi^E: E \to M$. Denote 
by $F$ its typical fiber and put $d = \dim M$, $n = \dim F$. 
The dimension   of the total space $E$ then is given by $\dim E = d + n$.
Note that for each point $p\in M$ the fiber $F_p = (\pi^E)^{-1} (p)$ 
is diffeomorphic to $F$. 

Recall that $\Gamma^\infty (\pi^E)$ stands for the \emph{sheaf of smooth local
sections} of $\pi^E$. Its space of sections over an open $U \subset M$ 
consists of all smooth $s : U \to E$ such that $\pi^E \circ s = \id_U$ and 
is denoted by $\Gamma^\infty (U,\pi^E)$. When writing $s \in \Gamma^\infty (\pi^E)$
we mean that $s$ is a smooth local section of $E$ defined over some 
open subset $U= \dom s \subset M$. If $p\in X$ is a point,
then $\Gamma^\infty (p,\pi^E)$ denotes the space of local smooth sections
about $p$ that is the space of all smooth sections $s : U \to E$ defined 
on an open neighborhood $U \subset X$ of $p$. We will write 
$\nbhds^\circ_p$ for the filter basis of all open neighborhoods of $p$ and 
$\Gamma^\infty_p (\pi^E)$ for the \emph{stalk} of $\Gamma^\infty (\pi^E)$ at $p$ 
which is defined as the colimit 
\begin{equation}
  \label{eq:definition-stalk-colimit}
  \Gamma^\infty_p (\pi^E) = \colim_{U \in \nbhds_p^\circ}  \Gamma^\infty (U,\pi^E) 
  = \Gamma^\infty (p, \pi^E) / \sim_p \ .
\end{equation}
Here we have made use of the fact that the colimit can be represented as 
the quotient of $ \Gamma^\infty (p, \pi^E) $ by the equivalence relation $\sim_p$,
where  equivalence $s_1 \sim_p s_2 $ of two smooth sections $s_1:U_1 \to E$ and 
$s_2:U_2\to E$ over open neighborhoods of $p$ is defined 
by the existance of an open neighborhood $U\subset U_1 \cap U_2$ of $p$ such that 
$s_1|_U: = s_2|_U$. The equivalence class of a section $s \in \Gamma^\infty (p,\pi^E)$
is denoted $[s]_p$ and is called the \emph{germ} of $s$ at $p$. So in other words,
$\Gamma^\infty_p (\pi^E)$ is the space of all germs of smooth sections at $p$. 
To distinguish $\sim_p$ from the later defined $m$-equivalence  we call 
the relation $\sim_p$ \emph{germ equivalence} at $p$.
\begin{definition}
  Let $p\in M$ be a point in the base manifold $M$ 
  and $k \in \N\cup \{ \infty\} $. Two local smooth sections  $s_1:U_1 \to E $
  and $s_2:U_2 \to E $ defined over open neighborhoods of $p$ 
  are said to be $k\,$-\emph{equivalent} at $p$ if $s_1(p)=s_2(p)$ and 
  if for every fibered chart
  $(x,u) : W \to \R^d \times \R^n$ of $\pi^E$ such that $p\in \pi (W)$,
  every index $\indb\in \{1,\ldots, n\}$  and all $\alpha \in \N^d$ with $ |\alpha|\leq k$ the equality
  \begin{equation}
    \label{eq:greek-k-equivalence-germs-jets}
    \frac{\partial^{|\alpha|} (u^\indb\circ s_1) }{\partial x^\alpha} (p) = 
    \frac{\partial^{|\alpha|} (u^\indb\circ s_2) }{\partial x^\alpha} (p)   
  \end{equation}
  holds true.
\end{definition}
\begin{propanddef}
  Let $p\in M$ be a point and $k \in \N\cup \{ \infty\} $. Then $k$-equivalence at $p$ is an equivalence relation on
  $\Gamma^\infty (p, \pi^E)$. It will be denoted  by the symbol $\sim_{k,p}$. 
  The $k$-equivalence class of a smooth section $s : U \rightarrow E$ at $p$ will be
  written $\jet^k_p (s)$. It is called the $k$-\emph{jet} of $s$ at $p$. 
  The set of such $k$-jets at $p$ coincides with the quotient space $ \Jet^k_p (\pi^E)= \Gamma^\infty (p, \pi^E)/\sim_{k,p}$.
  The union 
  \[
   \Jet^k (E) = \Jet^k (\pi^E) = \bigcup_{p\in M} \Jet^k_p (\pi^E)  % = \bigcup_{p\in M} \Gamma^\infty (p, \pi^E)/\sim_{k,p} 
  \]
  will be called the \emph{space of $k$-jets} of sections of the bundle $\pi^E :E \to M$. 
  Finally, there is a \emph{projection} $\pi^k = \pi^{\Jet^k (E)}: \Jet^k (\pi^E) \to M$ which maps 
  a jet $\jet^k_p(s)$, $s\in \Gamma^\infty (p, \pi^E)$ to its \emph{footpoint} $p$.
\end{propanddef}
\begin{proof}
  The relation of $k$-equivalence at $p$ is obviously reflexive and symmetric  by definition. It is also transitive by transitivity of equality. 
  Hence $k$-equivalence at $p$  is an equivalence relation indeed. The claim is proved. 
\end{proof}

\begin{lemma}
  The following statements are equivalent for two sections $s_1,s_2\in \Gamma^\infty(p,\pi^E)$  
  such that $s_1(p)=s_2(p)$: 
  \begin{numberlist}
  \item 
    The local sections $s_1$ and $s_2$ are $k$-equivalent at $p$.
  \item  
    If $(x,u) : W \to \R^d \times \R^n$ is a fibered chart of $\pi^E$ such that $p\in \pi (W)$, then
    for all $\indb \in \{ 1,\ldots, n\}$ and  $\indI \in \{1,\ldots,d\}^l$ with $1\leq l \leq k$:
    \begin{equation}
    \label{eq:roman-k-equivalence-germs-jets}
    \frac{\partial^{|\indI|} (u^\indb\circ s_1) }{\partial x^\indI} (p) = 
    \frac{\partial^{|\indI|} (u^\indb\circ s_2) }{\partial x^\indI} (p)  \ .  
  \end{equation}
    
  \item
    There exists a fibered chart
    $(x,u) : W \to \R^d \times \R^n$ of $\pi^E$ with $p\in \pi (W)$ 
    such that \eqref{eq:greek-k-equivalence-germs-jets} holds true. 
  \item
    There exists a fibered chart
    $(x,u) : W \to \R^d \times \R^n$ of $\pi^E$ with $p\in \pi (W)$ 
    such that \eqref{eq:roman-k-equivalence-germs-jets} holds true.   
  \end{numberlist}
\end{lemma}

\begin{proof}
  The claim is an immediate consequence of the  formula of Fa\`a-di-Bruno.
\end{proof}

\para
 Next we want to define a topology on the jet space $\Jet^k (\pi^E)$ so that $\pi^k: \Jet^k (\pi^E) \to M$ 
 becomes a (topological) fiber bundle. 


