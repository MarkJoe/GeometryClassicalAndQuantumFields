% Copyright 2017-2020 Markus J. Pflaum, licensed under CC BY-NC-ND 4.0
% main author: 
%   Markus J. Pflaum 
%
\section{Fock space}
\para 
Recall from \Cref{sec:monoidal-structure-category-hilbert-spaces}
that the Hilbert tensor product $\hilbertH_1 \cplttensor \hilbertH_2$ of 
two Hilbert spaces $\hilbertH_1$ and $\hilbertH_2$ is defined as the completion of the 
the algebraic tensor product $ \hilbertH_1 \otimes \hilbertH_2$ endowed with the inner product 
\[
  \inprod{\cdot,\cdot} : \big( \hilbertH_1 \otimes \hilbertH_2 \big) \times 
  \big( \hilbertH_1 \otimes \hilbertH_2 \big) \to \fldK , \:   
  \big( v_1 \otimes v_2 , w_1 \otimes w_2 \big)
  \mapsto \inprod{v_1,w_1} \cdot \inprod{v_2,w_2} \ .
\]
The norm of an element $v_1\otimes v_2 \in \hilbertH_1 \cplttensor \hilbertH_2$
is then given by $\| v_1\otimes v_2 \| = \| v_1 \| \cdot \| v_2 \|$, 
and every element $v  \in \hilbertH_1 \cplttensor \hilbertH_2$ can be 
written as the sum of a square summable family 
$\big(v_{i1}\otimes v_{i2} \big)_{i\in I}$ that is as
\[
  v = \sum_{i\in I}  v_{i1} \otimes v_{i2} \quad \text{where } 
  \| v\|^2 = \sum_{i\in I} \|v_{i1}\|^2 \cdot  \|v_{i2}\|^2 < \infty \ .
\]
If $(e_i)_{i\in I}$ is Hilbert basis for $\hilbertH_1$ and $(f_j)_{j\in J}$ one of $\hilbertH_2$,
the family $(e_i\otimes f_j)_{(i,j)\in I \times J}$ is a Hilbert basis of
$\hilbertH_1 \cplttensor \hilbertH_2$.
Moreover, the canonical map 
$\tau : \hilbertH_1 \times\hilbertH_2 \to \hilbertH_1 \cplttensor \hilbertH_2$, $(v_1,v_2) \mapsto v_1 \otimes v_2$
is bilinear and weakly Hilbert--Schmidt that means that there exists a $C \geq 0$ such that 
for all Hilbert bases  $(e_i)_{i\in I}$ of $\hilbertH_1$, all Hilbert bases $(f_j)_{j\in J}$ of $\hilbertH_2$,
and all $w \in \hilbertH_1 \cplttensor \hilbertH_2$
\[
  \sum_{(i,j)\in I \times J} \left| \inprod{\tau (e_i , f_j),w} \right|^2 \leq C \| w \|^2 \ . 
\]
Note that if this condition holds for one Hilbert basis of $\hilbertH_1$ and one of $\hilbertH_2$,
it holds for all. The Hilbert tensor product, which in the following we will
only call tensor product, satisfies the following universal property.

\begin{axiomlist}[\hspace{3em}]
\item[\textup{\sffamily (HTensor)}]
   \label{axiom:hilbert-tensor-product}
   For every Hilbert space $\hilbertH$ and every weakly Hilbert--Schmidt bilinear map 
   $\mu : \hilbertH_1\times \hilbertH_2 \to \hilbertH$ there exists a unique 
   bounded linear map $\widehat{\mu} : \hilbertH_1 \cplttensor \hilbertH_2  \to \hilbertH$ such that the diagram 
   \begin{displaymath}
   \begin{tikzcd}
       \hilbertH_1\times \hilbertH_2  \ar[d,"\tau",swap] \ar[r,"\mu"]  &  \hilbertH \\
       \hilbertH_1 \cplttensor \hilbertH_2 \ar[ru,"\widehat{\mu}",swap]
   \end{tikzcd}
   \end{displaymath}
   commutes. 
\end{axiomlist}
For a proof of the universal property see \Cref{sec:monoidal-structure-category-hilbert-spaces} %\ref{thm:universal=property-hilbert-tenros-rptoduct}
or \cite[Sec.~2.6.]{KadRinFTOAII}. Note that by its universal property the Hilbert tensor product
$\cplttensor$ is a bifunctor on the category $\category{Hilb}$ of Hilbert spaces and bounded maps.
Moreover, $\category{Hilb}$  equipped with the bifunctor $\cplttensor$ becomes a monoidal category.
See \Cref{sec:monoidal-structure-category-hilbert-spaces} for details and proofs.

\para 
Now let us  fix a Hilbert space $\hilbertH$ and consider the higher Hilbert 
tensor product powers $\fockSp^n (\hilbertH) = \hilbertH^{\cplttensor\, n}$ for natural $n$.
These are recursively defined by
\[
  \hilbertH^{\cplttensor\,0} = \fldK, \quad 
  \hilbertH^{\cplttensor\,n+1} = 
  \hilbertH \cplttensor  \big( \hilbertH^{\cplttensor\,n}\big) \ . 
\]
The \emph{Fock space} of $\hilbertH$ now is defined as the Hilbert space direct sum
\[
  \fockSp (\hilbertH) = \widehat{\bigoplus\limits_{n\in \N}} \fockSp^n (\hilbertH) =
  \widehat{\bigoplus\limits_{n\in \N}}\hilbertH^{\cplttensor\, n} \ .
\]
Its elements are families $(v_n)_{n\in \N} $ of vectors $v_n \in \hilbertH^{\cplttensor\, n}$ such that
$\sum_{n\in \N} \| v_n\|^2 < \infty$. The inner product of two such families
$v= (v_n)_{n\in \N}, w= (w_n)_{n\in \N} \in  \fockSp (\hilbertH)$ is given,
according to \Cref{thm:inner-product-hilbert-direct-sum}, by
\[
  \inprod{v,w} = \sum_{n\in \N} \inprod{v_n,w_n} \ . 
\]


\begin{remark}
  The construction of the Fock space resembles the one of the tensor algebra. Recall that the tensor algebra
  of $\hilbertH$ is the vector space $\Tensor (\hilbertH) = \bigoplus\limits_{n\in \N} \Tensor^n (\hilbertH)$
  where $\Tensor^n (\hilbertH)$ is defined as the algebraic tensor product power
  $\hilbertH^{\otimes n}$.  
  The \emph{completed tensor algebra} of $\hilbertH$ now is the $\ell^1$-completion
  \[
    \cpltTensor  (\hilbertH) = \ell_1\text{-}\widehat{\bigoplus\limits_{n\in \N}} \, \cpltTensor^n (\hilbertH) \ ,
  \]
  where $\cpltTensor^n (\hilbertH) = \fockSp^n (\hilbertH) = \hilbertH^{\cplttensor\, n}$.
  The completed tensor algebra  lies densely in Fock space. To verify this observe that,
  regarded in the category of Banach spaces, Fock space (including its norm) coincides with the
  $\ell_2$-direct sum of the spaces Banach spaces $\cpltTensor^n (\hilbertH)$ and  $\cpltTensor (\hilbertH)$
  with their $\ell_1$-direct sum. Since for every summable family
  $v=(v_n)_{\in \N} $ with $v_n \in \cpltTensor^n (\hilbertH)$ the relation 
  \[
    \|v\| = \|v\|_2 = \sqrt{\sum\limits_{n\in\N} \|v_n\|^2} \leq \sqrt{\sum\limits_{n\in\N} \|v_n\|}
    \cdot \sqrt{\sup\limits_{n\in\N} \|v_n\|} \leq \sum\limits_{n\in\N} \|v_n\| =  \|v\|_1 
  \]
  holds true by H\"olders inequality for series, $\cpltTensor (\hilbertH)$ is contained in
  $\fockSp (\hilbertH)$. It is also dense in Fock space because the (algebraic) direct sum
  $\bigoplus\limits_{n\in \N} \cpltTensor^n (\hilbertH)$ is already so. 

  Unlike Fock space in the case $\dim \hilbertH =\infty$, the completed tensor algebra $\cpltTensor (\hilbertH)$
  always carries a canonical algebra structure.
  To define the product of two summable families $v= (v_n)_{n\in \N}$ and $w= (w_n)_{n\in \N}$
  one puts for  all natural $n$
  \[
    z_n = \sum_{k=0}^n v_k \otimes w_{n-k} \ .
  \]
  Then $z_n \in \Tensor^n (\hilbertH)$ for all $n\in\N$, and the family $z =(z_n)_{n\in \N}$ is absolutely summable
  again since
  \[
    \sum_{n\in \N} \| z_n\| = \lim_{N \to \infty} \sum_{n=0}^N \| z_n\| \leq
    \lim_{N \to \infty} \sum_{n=0}^N \sum_{k=0}^n \|v_k \| \, \| w_{n-k} \| \leq
    \lim_{N \to \infty} \sum_{k=0}^N \sum_{l=0}^N \|v_k \| \, \| w_l \| \leq
    \| v\|_1 \, \| w\|_1 \ .
\]
  Hence $z =(z_n)_{n\in \N}$ is an element $\cpltTensor (\hilbertH)$ which we call the \emph{product}
  of $v$ and $w$. It will be denoted by $v \otimes w$. By the preceding estimate we thus obtain a continuous
  map
  \[
    \otimes : \cpltTensor (\hilbertH) \times \cpltTensor (\hilbertH) \to \cpltTensor (\hilbertH), \:
    (v,w) \mapsto v\otimes w 
  \]
  such that
  \[
     \|v\otimes w  \|_1 \leq \| v\|_1 \, \| w\|_1 \quad \text{for all } v,w \in \cpltTensor (\hilbertH) \ .
  \]
  The restriction of $\otimes$ to the (uncompleted) tensor algebra
  $\Tensor (\hilbertH) = \bigoplus\limits_{n\in \N} \Tensor^n (\hilbertH)$ is associative,
  so by density one concludes that $\otimes$ on $\cpltTensor (\hilbertH)$ is associative as well.
  Hence $\cpltTensor (\hilbertH)$ is  a Banach algebra. 

  Even though $\fockSp (\hilbertH)$ might not possess a compatible Banach algebra structure,
  it carries the structure of a $\cpltTensor (\hilbertH)$ left and right module with
  the left and right actions being continuous. Let us show this for the left module structure in
  some more detail. The right module case is analogous. So assume $v =(v_n)_{n\in\N} \in \cpltTensor (\hilbertH)$,
  $w =(w_n)_{n\in\N} \in \fockSp (\hilbertH)$, and let $z = (z_n)_{n\in\N}$ where as before
  $z_n = \sum_{k=0}^n v_k \otimes w_{n-k}$. Put $w_k = 0$ for $k<0$. Then compute using the triangle and
  H\"older's inequality
  \begin{equation*}
    \begin{split}
    \| z \|_2^2 & =  \lim\limits_{N\to \infty} \sum\limits_{n=0}^N \| z_n\|^2 =
    \lim\limits_{N\to \infty} \sum\limits_{n=0}^N \left\| \sum\limits_{k=0}^n v_k \otimes w_{n-k} \right\|^2
    \leq \\ & \leq 
    \lim\limits_{N\to \infty} \sum\limits_{n=0}^N \left( \sum\limits_{k=0}^N \left( \| v_k\|^{1/2}   \, \| w_{n-k}\| \right)
    \, \| v_k\|^{1/2} \right)^2
    \leq  \\ & \leq \lim\limits_{N\to \infty} \sum\limits_{n=0}^N
    \left( \sum\limits_{k=0}^N \| v_k\|   \, \| w_{n-k}\|^2 \right) \,
    \left( \sum\limits_{k=0}^N \| v_k \| \right)
    \leq \\ & \leq  \lim\limits_{N\to \infty} \| v\|_1 \, 
    \sum\limits_{k=0}^N  \left( \| v_k\|   \sum\limits_{n=0}^N \| w_{n-k}\|^2 \right)
    \leq \\ & \leq \lim\limits_{N\to \infty} \| v\|_1 \, 
     \sum\limits_{k=0}^N \left( \| v_k\|   \sum\limits_{n=0}^N \| w_n \|^2 \right) = \| v\|_1^2 \,  \| w\|_2^2 \ .
    \end{split}
  \end{equation*}
  Hence $z \in \fockSp (\hilbertH)$, and the product
  $\otimes : \cpltTensor (\hilbertH) \times \cpltTensor (\hilbertH) \to \cpltTensor (\hilbertH)$ has a unique continuous
  extension to a left action
  \[
     \otimes : \cpltTensor (\hilbertH) \times \fockSp (\hilbertH) \to \fockSp (\hilbertH), \: (v,w) \mapsto v\otimes w 
  \]
  such that
  \[
    \| v\otimes w\|_2 \leq \| v\|_1 \,  \| w\|_2 \quad \text{for all }
    v\in \cpltTensor (\hilbertH),\: w \in \fockSp (\hilbertH) \ .
  \] 
\end{remark}

\para
Next we will show that associating to a Hilbert space its Fock space  can be extended to a functor on the category $\category{Hilb}_1$
of Hilbert spaces and linear contractions between them. Recall that by a linear contraction one understands a bounded linear operator with
norm $\leq 1$. So assume that $A : \hilbertH_1 \to \hilbertH_2$ is a contraction between Hilbert spaces
$\hilbertH_1$ and $\hilbertH_2$. By functoriality of the algebraic tensor product one obtains for each $n \in \gzN$ a linear map
\[
  A^{\otimes n} : \hilbertH_1^{\otimes n} \to \hilbertH_2^{\otimes n}, \: v_1\otimes \ldots \otimes v_n \mapsto A v_1\otimes \ldots \otimes A v_n \ .
\]
By \Cref{thm:tensor-product-functor-category-hilbert-spaces-bounded-linear-operators} or \cite[Prop.~2.6.12 \& Eq.~2.6.(16)]{KadRinFTOAII} this operator has norm
$\|A\|^n$ and extends uniquely to a bounded  linear operator  $\fockSp^n (A) : \fockSp^n (\hilbertH_1) \to \fockSp^n (\hilbertH_2)$
having the same norm. Since by assumption $\|A\|\leq 1$, one concludes that   $\| \fockSp^n (A) \| \leq 1$ for all $n\in \gzN$.
One further puts $\fockSp^0 (A) = \id_\fldK$ and observes that then $\sup_{n\in \N} \| \fockSp^n (A) \| = 1$. Hence, by construction of the
operators $\fockSp^n (A) $ and definition of the Hilbert direct sum the map
\[
  \fockSp (A) : \fockSp (\hilbertH_1) \to \fockSp (\hilbertH_2), \: v = (v_n)_{n\in \N} \mapsto   \left( \fockSp^n(A) (v_n)\right)_{n\in \N}
\]
is well-defined and a bounded linear operator of norm $1$. Note that hereby we have again used the (silent) agreement that
$v = (v_n)_{n\in \N}$ denotes a square-integrable family with $v_n \in \fockSp^n (\hilbertH_1)$ for all $n\in \N$. 
By construction it is immediate that  $\fockSp (\id_\hilbertH) =\id_{\fockSp(\hilbertH)}$ for every Hilbert space $\hilbertH$ and that
for linear contractions $A: \hilbertH_1 \to \hilbertH_2$ and $B: \hilbertH_2 \to \hilbertH_3$ between Hilbert spaces the relation
\[
  \fockSp ( BA ) = \fockSp (B) \, \fockSp (A)
\]
holds true. Hence we obtain as promised a (covariant) functor $\fockSp$ from the category $\category{Hilb}$ to  itself. One
sometimes calls $\fockSp$ the \emph{functor of second quantization}.

\para
Particularly important for quantum field theory is the observation going back to \cite{CooMSQ}
that every closed densely defined linear operator on a Hilbert space has an extension to Fock space which again is closed
and densely defined. Let us explain this in some more detail. We essentially follow the approach by \cite{CooMSQ};
see also \cite{EmcAMSMQFT}.

Let $(\hilbertH)_{i=1}^n$ be a finite family of Hilbert spaces and
$(A_i)_{i=1}^n$ a family of closed densely defined unbounded linear operators 
$A_i : \Dom (A_i) \subset \hilbertH_i\to \hilbertH_i$, $i=1,\ldots ,n$ over the same index set. 
Hence  the adjoint $A_i^*$ of $A_i$ is a closed  densely defined unbounded linear operator on
$\hilbertH_i$ for every index $i=1,\ldots ,n$.  Put $\domD_i = \Dom (A_i)$ and $\domD_i^* = \Dom (A_i^*)$ and
note that then $\domD_i$ and $\domD_i^*$ are dense in $\hilbertH_i$ by assumption and the preceding observation.


