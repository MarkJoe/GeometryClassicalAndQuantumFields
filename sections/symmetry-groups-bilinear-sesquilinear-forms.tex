% Copyright 2017 Markus J. Pflaum, licensed under CC BY-NC-ND 4.0
% main author: 
%   Markus J. Pflaum
%
\section{Symmetry groups of bilinear and sesquilinear forms}

\para In this section $\fldK$ will always stand for the field of real or 
complex numbers. Before defining their symmetry groups let us recall 
the notions of bilinear and sesquilinear forms.
A \emph{bilinear form} on a $\fldK$-vector space $V$ is a map 
$b : \vectorspV \times \vectorspV \to \fldK$ having the following properties:
\begin{axiomlist}[BF]
\item
\label{axiom:bilinear-form-linearity-in-first-coordinate} 
  The map $b$ is \emph{linear} in its first coordinate which means that
  \[
    b ( v_1 + v_2, w ) = b( v_1, w ) + b ( v_2, w )
    \quad \text{and} \quad b ( r v, w ) = r b( v, w )
  \]
  for all  $v, v_1,v_2 , w\in \vectorspV$ and $r\in \fldK$.
\item
 \label{axiom:bilinear-form-linearity-in-second-coordinate}{\hspace{-2mm}, \textup{({\sffamily SF2})}} 
  The map $b$ is \emph{linear} in its second coordinate which means that
  \[
    b( v, w_1+w_2 ) =  b( v, w_1 ) + b( v, w_2 )
    \quad \text{and} \quad b(  v, r w ) = r b( v, w )
  \]
  for all  $v, w,w_1,w_2 \in \vectorspV$ and $r\in \fldK$.
\end{axiomlist}
Bilinear forms with the property that commuting its variables leads to the same or to the negative of the original 
bilinear form are given a particular name. 
More precisely, a bilinear map $b : \vectorspV \times \vectorspV \to \fldK$ is said to be \emph{symmetric} if
\begin{enumerate}[label={\textup{({\sffamily BF\arabic*s})}},leftmargin=*]
\setcounter{enumi}{2}
\item
\label{axiom:bilinear-form-symmetry} 
  $b ( v, w ) = b ( w, v )$ for all  $v, w \in \vectorspV$,
\end{enumerate}
and \emph{antisymmetric} or \emph{skew-symmetric} if
\begin{enumerate}[label={\textup{({\sffamily BF\arabic*a})}},leftmargin=*]
\setcounter{enumi}{2}
\item
\label{axiom:bilinear-form-antisymmetry} 
   $b ( v, w ) = - b ( w, v )$ for all  $v, w \in \vectorspV$.
\end{enumerate}

A map $b: \vectorspV \times \vectorspV \to \fldK$ which satisfies 
\ref{axiom:bilinear-form-linearity-in-second-coordinate} and axiom 
\ref{axiom:sesquilinear-form-conjugate-linearity-in-first-coordinate} below instead of 
\ref{axiom:bilinear-form-linearity-in-first-coordinate}
is  called a \emph{sesquilinear form}.
\begin{axiomlist}[SF]
\item
 \label{axiom:sesquilinear-form-conjugate-linearity-in-first-coordinate} 
  The map $b$ is \emph{conjugate-linear} in its first coordinate which means that
  \[
    b( v_1 + v_2, w ) =  b( v_1, w ) + b( v_2, w )
    \quad \text{and} \quad b(  rv,  w ) = \overline{r} b( v, w )
  \]
  for all  $v, v_1,v_2, w \in \vectorspV$ and $r\in \fldK$.
\end{axiomlist}
A sesquilinear form $b$ is called a \emph{hermitian form} if it has the following property:
\begin{enumerate}[label=\textup{({\sffamily SF\arabic*c)}},leftmargin=*]
\setcounter{enumi}{2}
\item
\label{axiom:sesquilinear-form-conjugate-symmetry} 
  The map $b$ is \emph{conjugate-symmetric} which means that 
  \[
    b ( v, w ) = \overline{b ( w, v )} \quad \text{for all  } v, w \in \vectorspV \ .
  \]
\end{enumerate}

If the ground field of the underlying vector space of a bilinear or sesquilinear form $b$ is $\C$, one
calls $b$ a \emph{complex} bilinear form respectively a \emph{complex} sesquilinear form. 
One uses analogous language when the ground field is $\R$.
Note that a real sesquilinear form is the same as a real bilinear form. 

A bilinear or sesquilinear form  $b$ is said to be \emph{weakly-nondegenerate} if it satisfies  axiom
\begin{enumerate}[label=\textup{({\sffamily SF\arabic*w)}},leftmargin=*]
\setcounter{enumi}{3}
\item
\label{axiom:bilinear-sesquilinear-form-weakly-nondegenerate} 
  The map ${}^\flat : \vectorspV \to \vectorspV'$,
  $v \mapsto v^\flat = b ( - ,v ) = \big(  \vectorspV \ni w \to b ( w,v ) \in \fldK \big)$ from 
  $\vectorspV$ to its algebraic dual $\vectorspV'$ is injective .
\end{enumerate}
Note that \ref{axiom:bilinear-sesquilinear-form-weakly-nondegenerate} is equivalent to the 
requirement that for every $v\in \vectorspV$ the map 
$b ( v,- ) : \vectorspV \to \fldK$, $w \to b ( v,w )$ is the zero map if and only if $v=0$.

In case the underlying vector space $\vectorspV$ is normed, there is a stronger version of nondegeneracy
for bounded bilinear or sesquilinear forms $b :\vectorspV \times \vectorspV \to \fldK$. Namely, one calls such a form \emph{nondegenerate} 
if it fulfills
\begin{enumerate}[label={\textup{({\sffamily SF\arabic*n})}},leftmargin=*]
\setcounter{enumi}{3}
\item
\label{axiom:bilinear-sesquilinear-form-nondegenerate} 
  The map ${}^\flat : \vectorspV \to \vectorspV^*$, $v \mapsto v^\flat = b ( v, - ) = \big(  \vectorspV \ni w \to b ( v,w ) \in \fldK \big)$ from $\vectorspV$ to its topological dual $\vectorspV^*$ is 
  a linear or conjugate-linear topological isomorphism.
\end{enumerate}

Recall that $b(v,v) \in \R$ for every hermitian form  $b$ on $V$ and $v\in \vectorspV$. In case 
that  such a $b$ satisfies
\begin{enumerate}[label=\textup{({\sffamily SF\arabic*s)}},leftmargin=*]
\setcounter{enumi}{4}
\item
\label{axiom:bilinear-sesquilinear-form-positive-semidefinite} 
  $b ( v,v ) \geq 0$ for all $v\in \vectorspV$,
\end{enumerate}
then one calls the hermitian form $b$ \emph{positive semidefinite}.

Recall from \Cref{thm:positive-definitess-equivalent-nondegeneracy} that a positive semidefinite  hermitian form $b$ on a
$\fldK$-vector space $V$ is weakly-nondegenerate if and only if it is \emph{positive definite}  which means that
  \begin{enumerate}[label=\textup{({\sffamily SF\arabic*p)}},leftmargin=*]
  \setcounter{enumi}{4}
  \item
  \label{axiom:bilinear-sesquilinear-form-positive-definite} 
  $b ( v,v ) > 0$ for all $v\in \vectorspV\setminus \{ 0 \}$.
  \end{enumerate}

\begin{remark}
 If $b$ is a nondegenerate bilinear or sesquilinear form on a Banach space $\banachV$, then
 one sometimes calls the map ${}^\flat : \vectorspV \to \vectorspV^*$ from Axiom
 \ref{axiom:bilinear-sesquilinear-form-nondegenerate} and its inverse
 ${}^\sharp : \vectorspV^* \to \vectorspV$ the \emph{musical isomorphisms} associated 
 to $b$. 
\end{remark}

\begin{examples}
In addition to the hermitian forms introduced in \Cref{ex:inner-product-spaces} 
let us give a few more examples of bilinear forms which are particularly relevant for mathematics 
or mathematical physics. 
\begin{letterlist}
\label{ex:bilinear-forms}
\item\label{ite:pseudo-euclidean-metric}
  Let $p,q$ be positive integers, $n = p+q$, and 
  $\pseudoeuclidean{\cdot,\cdot}{p,q} : \R^n \times \R^n \to \R$ the \emph{pseudo-euclidean} 
  form given by 
  \[
    \pseudoeuclidean{x,y}{p,q} = \sum_{i=1}^p x^i\, y^i - \sum_{j=p+1}^n x^j \, y^j
    \quad \text{for } x=(x^1,\ldots, x^n),\: y = (y^1,\ldots, y^n) \in \R^n \ . 
  \]
  The map  $\pseudoeuclidean{\cdot,\cdot}{p,q}$ is a nondegenerate bilinear  form, 
  but it is not positive semidefinite by definition. The space $\R^n$ together with the 
  pseudo-euclidean form $\pseudoeuclidean{\cdot,\cdot}{p,q}$ is sometimes denoted $\R^{p,q}$. 
  For the particular case $(p,q) =(1,d)=(1,n-1)$ one calls $\R^{1,d}$ \emph{Minkowski space} 
  of \emph{space--time dimension} $d+1$, and 
  $\minkowskimetric{\cdot,\cdot} := \pseudoeuclidean{\cdot,\cdot}{1,d}$
  the corresponding \emph{Minkowski metric}. 
  The components of elements $x,y \in \R^{1,d}$ of Minkowski space are often indexed in the form 
  $x= (x^0,x^1,\ldots,x^{d})=(x^\mu)_{\mu=0}^{d}$ and
  $y= (y^0,y^1,\ldots,y^{d})=(y^\nu)_{\nu=0}^{d}$. In this notation, the Minkowski metric is given by 
  \[
    \minkowskimetric{x,y} = x^0 \, y^0 - \sum_{i=1}^{d} x^i\,y^i \ .
  \] 
  Moreover, one associates to $x$ and $y$ the \emph{space-vectors}
  \[
    \vec{x} = (x^1,\ldots ,x^{d}) = (x^i)_{i=1}^{d} \quad \text{and} \quad 
    \vec{y} = (y^1,\ldots ,y^{d}) = (y^j)_{j=1}^{d} \ . 
  \] 
  When labels run through all space-time indices they are usually denoted in the 
  mathematical physics literature by lower-case Greek letters, when they run only through 
  space indices, they are denoted by lower-case Roman letters. We will follow these conventions. 
\item
  Next consider $\fldK^{2n}$ with $n \in \gzN$ and define
  \[ 
    \omega : \fldK^{2n} \times \fldK^{2n}  \to \fldK , \quad (v,w) \mapsto 
   \sum_{i=1}^n (v_i \, w_{n+i} - w_i \, v_{n+i}) \ .   
  \] 
  Then $\omega$ is a nondegenerate antisymmetric bilinear form. We call it the 
  \emph{standard symplectic form} on $\fldK^{2n}$. More generally, 
  a nondegenerate antisymmetric bilinear form $\omega :\banachE \times \banachE \to \fldK$ 
  on a Banach space $\banachE$ over $\fldK$ is called a \emph{symplectic form}.
  If $\omega :\banachE \times \banachE \to \fldK$ is only weakly-nondegenerate (but still
  antisymmetric), then one says that $\omega$ is a weakly-symplectic form. 

  If $\banachV$ is a Banach space and $\banachE = \banachV \oplus \banachV^*$, then 
  \[ 
    \omega  : \banachE \times \banachE \to \fldK, \: \big( (v,\alpha), (w,\beta)\big) \mapsto 
    \beta (v) - \alpha (w) 
  \]
  is a weakly-symplectic form on $\banachE$ which is symplectic if and only if $\banachV$ is
  reflexive that is if and only if the canonical embedding $\banachV \hookrightarrow \banachV^{**}$ 
  is an isomorphism.

  \color{red}
  \begin{proof}
    Antisymmetry is clear by definition. 
  \end{proof}
\color{black}
\end{letterlist}
\end{examples}


\para 
Next consider a Banach space $\banachE$ over $\fldK$ with norm $\|\cdot \|$  and the space $\blinOps (\banachE)$ of bounded $\fldK$-linear operators on $\banachE$.  
Recall that $\blinOps (\banachE)$ carries the following natural topologies:
\begin{romanlist}
\item the \emph{norm  topology} or \emph{uniform operator  topology} $\topology_\textup{n}$ defined by the \emph{operator norm}
   \[
      \| - \| : \blinOps (\banachE) \to \R_{\geq 0},\: 
      A \mapsto \| A \| := \sup \big\{ \| Av \| \in \R_{\geq 0} \mid v \in \banachE \: \& \: \| v \| \leq 1 \big\} \ ,
   \]
\item the \emph{compact-open topology}  $\topology_\textup{co}$  defined by the seminorms
   \[
      p_K : \blinOps (\banachE) \to \R_{\geq 0},\: 
      A \mapsto p_K( A ) := \sup \big\{ \| Av \|  \in \R_{\geq 0} \mid v \in K \big\} \ ,
   \]
   where $K$ runs through the nonempty compact subsets of $\banachE$, 
\item the \emph{strong operator topology} $\topology_\textup{s}$   defined by the seminorms
   \[
      p_v : \blinOps (\banachE) \to \R_{\geq 0},\: 
      A \mapsto p_v( A ) := \| Av \| \ ,
   \]
   where $v $ runs through the nonzero elements of $\banachE$, 
\item the \emph{weak operator topology}  $\topology_\textup{w}$  defined by the seminorms
   \[
      p_{\lambda,v} : \blinOps (\banachE) \to \R_{\geq 0},\: 
      A \mapsto p_{\lambda,v} ( A ) := \lambda (Av ) \ ,
   \]
   where $\lambda$ runs through the  nonzero bounded linear functionals $\banachE \to \fldK$ and $v$  
   through the nonzero elements of $\banachE$.
\end{romanlist}

 These four operator topologies are comparable. More precisely one has
 \[
  \topology_\textup{w} \subset \topology_\textup{s} \subset \topology_\textup{co} \subset \topology_\textup{n}  \ .
 \]
 In case $\banachE$ is finite dimensional, the topologies coincide, if $\banachE$ is infinite dimensional,  
 then the inclusions are proper.    

 To denote which topology  $\blinOps (\banachE)$ is endowed with we write 
 $\blinOps (\banachE)_\textup{n}$, $\blinOps (\banachE)_\textup{co}$, $\blinOps (\banachE)_\textup{s}$ and 
 $\blinOps (\banachE)_\textup{w}$, respectively.


\begin{propanddef}
  Let $\banachE$ be a Banach space over $\fldK$, and $\LieGL(\banachE) \subset \blinOps (\banachE)_\textup{n}$ the space
  of bounded invertible $\fldK$-linear operators on $\banachE$ endowed with the norm topology.
  Then the following holds true. 
  \begin{letterlist}
  \item  
    The space $\LieGL(\banachE)$ is open in  $\blinOps (\banachE)_\textup{n}$.
  \item
    $\LieGL(\banachE)$ together with the operator product and the identity map $\id_\banachE$ is a group. 
  \item
    The group $\mathsf{G}:=\LieGL(\banachE)$ endowed with the norm topology is a \emph{topological group}
    which means that it has the following properties:
    \begin{axiomlist}[TopGr]
    \item
      The multiplication map $\cdot : \mathsf{G} \times \mathsf{G} \to \mathsf{G}$ is continuous.
    \item
    The inversion map $i : \mathsf{G} \to \mathsf{G}$ is continuous. 
  \end{axiomlist}
  \end{letterlist}
\end{propanddef}

\begin{proof}
%{\bfseries Exercise!}
  \begin{adletterlist}
  \setcounter{enumi}{1}
  \item 
    By the open mapping theorem the inverse of a bounded invertible operator 
    is bounded as well, hence $g^{-1}\in \LieGL(\banachE)$ for all $g\in \LieGL(\banachE)$.
    Obviously $\id_\banachE \in \LieGL(\banachE)$, so $\LieGL(\banachE)$ is a group indeed.
  \setcounter{enumi}{0}
  \item
    Let $g \in \LieGL(\banachE)$. Then $\| g^{-1}\| > 0$, since $1 =\| v \| \leq  \| g^{-1} \| \, \| g v \|$ 
    for every unit vector $v\in \banachE$. 
    Let $0< r < \frac{1}{\| g^{-1}\|}$. For $A \in \blinOps(\banachE)$ 
    with $\| A \| < r$ the series $\sum_{k \in \N} (-1)^k \big( g^{-1}A\big)^k$ then is dominated by the converging geometric series
  $\sum_{k\in \N} r^k$, hence converges too. Compute
  \[ 
     \big( \id_\banachE +  g^{-1}A\big) \left( \sum_{k = 0}^\infty (-1)^k \big( g^{-1}A\big)^k \right)
     =  \sum_{k = 0}^\infty (-1)^k \big( g^{-1}A\big)^k - \sum_{k = 1}^\infty (-1)^k \big( g^{-1}A\big)^k = \id_\banachE 
  \]
  and analogously
   \[ 
    \left( \sum_{k = 0}^\infty (-1)^k \big( g^{-1}A\big)^k \right) \big( \id_\banachE +  g^{-1}A\big) 
     =  \id_\banachE  \ .
  \]
  Therefore $\id_\banachE  +  g^{-1}A$ is invertible with bounded inverse  $\sum_{k = 0}^\infty (-1)^k \big( g^{-1}A\big)^k$. 
  Hence the operator $g +  A = g \big(\id_\banachE  +  g^{-1}A\big)$ is invertible as well and the open ball of radius 
  $r$ around $g$ is contained in $\LieGL(\banachE)$. Thus $\LieGL(\banachE)$ is open in $\blinOps(\banachE)$.
  \setcounter{enumi}{2}
  \item
    To verify continuity of multiplication recall that $\| A B \| \leq  \| A \| \, \| B\|$
    for all $A,B \in \blinOps (\banachE)$. Then
    \begin{equation*}
    \begin{split}
       \| A B - A' B'\| =  \| ( A B - A' B ) + ( A' B - A' B') \| \leq
        \|  A  - A'  \| \, \| B \| + \| A'\| \, \|  B -  B' \|\ .
    \end{split}
    \end{equation*}
    Hence multiplication is locally Lipschitz continuous, therefore continuous. 

    To prove continuity of inversion let $g \in \LieGL(\banachE)$ and choose $0< r< \frac{1}{\| g^{-1}\|}$. 
    Then $g + A \in  \LieGL(\banachE)$ for all $A \in \blinOps (\banachE)$ with $\| A \| < r$ by the preceding 
    considerations. Moreover, 
    \begin{equation*}
    \begin{split}
       \left\|( g+A )^{-1} - g^{-1}  \right\| = &   
       \left\| \big( ( \id_\banachE  + g^{-1} A )^{-1} - \id_\banachE  \big) g^{-1}  \right\| \leq \\ 
       \leq & \left\| g^{-1}  \right\| \,  
       \left\| \sum_{k=1}^\infty  (-1)^k \big( g^{-1}A\big)^k \right\| 
       \leq  \left\| g^{-1}  \right\| \, \sum_{k=1}^\infty  \left\|g^{-1}A \right\|^k  \leq 
       \frac{\left\| g^{-1}  \right\|^2}{1 - r\| g^{-1} \| } \| A \| \ .
    \end{split}
    \end{equation*}
    Hence inversion is locally Lipschitz continuous, so in particular continuous.
  \end{adletterlist}
\end{proof}

Unless mentioned differently, we assume from now on that $\LieGL(\banachE)$  carries the norm topology. Sometimes 
we will write $\LieGL(\banachE)_\textup{n}$ to emphasize this.

\para 
  Assume that $b : \banachE \times \banachE \to \fldK$ is a bounded bilinear or sesquilinear form on a Banach space $\banachE$ over 
  $\fldK$. Consider the group $\LieGL(\banachE)$ and define $\mathsf{G}(\banachE,b)$ as  the set of all 
  $g \in \LieGL(\banachE) $ such that 
  \[
    b (gv , gw) = b (v,w) \quad \text{for all } v,w \in \banachE \ . 
  \]
\begin{proposition}
  Under the assumptions stated $\mathsf{G}(\banachE,b)$ is a closed subgroup of $\LieGL(\banachE)_\textup{n}$.  
\end{proposition}
\begin{proof}
  If $g,h \in \mathsf{G}(\banachE,b)$, then their operator product $gh$ lies in $\mathsf{G}(\banachE,b)$
  as well since
  \[
    b (ghv , ghw) =  b( hv,hw) = b (v,w) \quad \text{for all } v,w \in \banachE \ . 
  \]
  Moreover, $\id_\banachE $ leaves $b$ invariant, so is in $\mathsf{G}(\banachE,b)$, too. Hence 
  $\mathsf{G}(\banachE ,b)$ is a subgroup of  $\LieGL(\banachE)_\textup{n}$. 

  Now assume that $g \in \LieGL(\banachE)_\textup{n} \setminus \mathsf{G}(\banachE,b)$. Then 
  there are $v,w\in \banachE$ such that 
  \[
     b(gv,gw) \neq b(v,w) \quad\text{and}\quad \|v\|=\|w\| = 1 \ .
  \]
  Put $\delta = |  b(gv,gw) - b(v,w) |$ and let 
  $C= \sup\big\{ |b(x,y)|  \bigmid x,y \in \banachE \: \& \: \|x\| =\|y\|=1 \big\}$. 
  Then one has for all $h \in \blinOps (\banachE)$
  \begin{equation*}
  \begin{split}
    | b(hv,hw)- b(v,w) |  \, & 
    = \big|  \big( b(hv,hw)- b(gv,gw)\big) - \big( b(v,w)- b(gv,gw) \big)\big| \geq \\
    & \geq \big| \delta - | b(hv,hw) -b(gv,gw)|  \big| \geq \\
    & \geq  \delta - | b(hv,hw) -b(gv,hw)|  -  | b(gv,hw) -b(gv,gw)| \geq \\
    & \geq  \delta - C \, \| h - g \| \, ( \| h\| +  \| g \|) \ . 
  \end{split}
  \end{equation*}
  Hence, if $\| h -g \| < \varepsilon$ with
  $\varepsilon = \min \left\{ 1, \frac{1}{2\| g^{-1}\|}, \frac{\delta}{2(C +1)(2\| g\|+1)} \right\} $, then
  $h\in \LieGL (\banachE)$ and 
  \[
      | b(hv,hw)- b(v,w) | \geq \delta - C \, (2 \| g\| +1) \, \varepsilon \geq \frac 12 \delta \ . 
  \]
  So  $\LieGL(\banachE)_\textup{n} \setminus \mathsf{G}(\banachE,b)$ is open and the claim is proved.
\end{proof}

\begin{examples}\label{ex:specific-groups-defined-bilinear-forms}
\begin{letterlist}
\item
   For a Hilbert space $\hilbertH$, the group $\mathsf{G}(\hilbertH,\langle\cdot,\cdot\rangle)$    
   is called the \emph{unitary group} of  $\hilbertH$ and denoted  $\LieU (\hilbertH)$.
   If the underlying ground field is $\R$, one often writes  $\LieO (\hilbertH)$
   for $\mathsf{G}(\hilbertH,\langle\cdot,\cdot\rangle)$ and calls it the \emph{orthogonal group}
   of the real Hilbert space $\hilbertH$. In the finite dimensional case,
   $\LieU (n)$ stands for  $\LieU (\C^n)$ and  $ \LieO (n)$ for  $\LieO (\R^n)$. 
\item\label{ite:pseudo-orthogonal-group}
   Given two positive integers $p,q$ consider the pseudo-euclidean metric 
   $\langle\cdot,\cdot\rangle_{p,q}$ on $\R^{p,q}\cong \R^{p+q}$, 
   see Example \ref{ex:bilinear-forms} \ref{ite:pseudo-euclidean-metric}.
   The invariant group  $\mathsf{G}\big(\R^n,\pseudoeuclidean{\cdot,\cdot}{p,q} \big)$ 
   then is called a \emph{pseudo-orthogonal group} and is denoted  $ \LieO (p,q)$. 
\item 
   Let $\banachE$ be a Banach space over $\fldK$ with a symplectic form $\omega$. 
   The group $ \LieSp (\banachE,\omega) := \mathsf{G}\big(\banachE,\omega)$ then is the 
   \emph{symplectic group} associated to $(\banachE,\omega)$.  
   If  $\banachE$ is $\fldK^{2n}$ and $\omega $ its canonical symplectic form, then one writes
   $ \LieSp (2n,\fldK)$ for the associated symplectic group. 
\end{letterlist}
\end{examples}

\para It has been claimed wrongly at several places in the mathematical literature, notably in 
\cite{SimLGQM}[Proof of Thm.~1] and \cite{AtiSegTKT}[p.~321] that the unitary group with the strong operator topology 
respectively with the compact-open topology is not a topological group.  
The correct(ed) statement appeared in  \cite{SchotGSP}[III.3.2~Satz], {\cite{NeeTSB}[Prop.~II.1], and \cite{SchotMICFT}[Prop.3.11]},
whose presentation we will essentially follow here. 

\begin{proposition}
  If $\hilbertH$ is a Hilbert space, then $\LieU (\hilbertH)_\textup{s}$, the unitary group $\LieU (\hilbertH)$ endowed 
  with the strong operator topology, is a complete topological group. Moreover, the compact-open topology, the
  strong operator topology, and weak operator topology all coincide on $\LieU (\hilbertH)$.
  Finally, if $\hilbertH$ is separable, then $\LieU (\hilbertH)_\textup{s}$  is completely metrizable. 
\end{proposition}

\begin{proof}
  For $v \in \hilbertH$ and $V \in \LieU (\hilbertH)$ let $p_{v,V} : \LieU (\hilbertH) \to \R_{\geq 0}$ be defined by 
  \[
   U \mapsto p_{v,V} (U) = \| (U - V) v \| \ .
  \]
  A subbasis of the strong operator topology on $\LieU (\hilbertH)$ then is given by the sets
  \[
    \big\{  U \in  \LieU (\hilbertH) \bigmid  p_{v,V} (U) < \varepsilon \big\}, \quad \text{where } v \in \hilbertH, \: 
    V \in \LieU (\hilbertH), \text{ and } \varepsilon > 0 \ .
  \]
\end{proof}