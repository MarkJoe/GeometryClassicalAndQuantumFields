% Copyright 2017 Markus J. Pflaum, licensed under CC BY-NC-ND 4.0
% main author: 
%   Markus J. Pflaum 
%
\section{Quantum mechanical symmetries}
\label{sec:quantum-mechanical-symmetries}

\subsection*{Automorphisms of the projective Hilbert space and Wigner's theorem}
\para 
  Assume that a quantum mechanical system is described by the projective Hilbert space $\projH$
  and that two observers $\mathscr{O}$ and $\mathscr{O}'$ observe the system. 
  While observer $\mathscr{O}$ describes the states the system is in by rays 
  $\linek, \linel, \linel_i, ,... \in\projH$, observer $\mathscr{O}'$ describes them by possibly different rays
  $\linek', \linel', \linel_i',...\in\projH$. In other words this means that from the point of physics the rays 
  are not invariant under observer change. Rather does the observer change give rise to a map 
  $A :\projH \to \projH$, $\linel \mapsto A\linel = \linel' $. This map has to be invertible because 
  the observer change is reversible. Even though rays describing the states of the system do change 
  under an observer change, the corresponding transition probabilities remain invariant by the paradigm 
  that the laws of (quantum) physics do not change from one observer to another. 
  Mathematically this can be expressed by 
  \[
    \pinprod{ A \linek, A \linel}^2 = \pinprod{\linek,\linel}^2 \quad \text{for all } \linek,\linel \in \projH \ .  
  \]
  This leads us to the following definition.
\begin{definition}
  Let $\projH$, $\projH_1$ and $\projH_2$ denote projective Hilbert spaces.
  One then calls a  map $A:\projH_1 \to \projH_2  $ an \emph{isometry}, if 
   \[
    \pinprod{ A \linek, A \linel} = \pinprod{\linek,\linel} \quad \text{for all } \linek,\linel \in \projH_1 \ .
  \]
  A bijective isometry $A : \projH \to \projH$ 
  is called an \emph{isometric automorphism}, a \emph{Wigner automorphism} or just 
  an \emph{automorphism}.  

  In quantum mechanics, an automorphism of a projective Hilbert space $\projH$ is called 
  a \emph{symmetry} of the quantum mechanical system described by $\projH$. 
\end{definition}

\para
  Because the composition of isometric maps between projective Hilbert spaces is an isometric map 
  and the identity map on a projective Hilbert space is isometric the projective Hilbert spaces 
  as objects and the isometric maps as morphisms form a category which we call the 
  \emph{Wigner category} denoted it by $\category{Wig}$. 
  The Wigner automorphisms are then the automorphisms of that category. 

  The automorphisms of a projective Hilbert space $\projH$ form a group denoted by $\Aut(\projH)$.

 
\para\label{par:quantum-mechanical-symmetries}
  From now on in this section let the symbol $\hilbertH$ stand for a complex Hilbert space of dimension
  $\geq 2$.
  We want to examine what maps on $\hilbertH$ induce automorphisms of the corresponding 
  projective Hilbert space. 

  If $S :\hilbertH \to \hilbertH$ is a unitary operator that is $S \in \LieGL (\hilbertH)$ and 
  $\inprod{Sv,Sw} = \inprod{v,w}$ for all $v,w\in \hilbertH$, then 
  $\hat{S}:\projH \to \projH$, $\C v\mapsto \C Sv$ is well-defined and an automorphism of $\projH$. 
  But not every automorphism of $\projH$ is of the form $\hat{S}$ with $S\in \LieU(\hilbertH)$. 
  Namely let $T:\hilbertH\to \hilbertH$ be an  anti-unitary map that is $T\in \LieGL (\hilbertH,\R)$, 
  $T(\lambda v) = \overline{\lambda}T v$ for all $v\in \hilbertH$, $\lambda \in \fldC$ and 
  $\inprod{Tv,Tw}= \overline{\inprod{v,w}} = \inprod{w,v}$ for all  $v,w \in \hilbertH$. 
  Then $\hat{T}:\projH\to \projH$, $\C v\mapsto \C Tv$ is also well-defined, invertible and 
  preserves transition probabilities. Therefore $\hat{T}\in \Aut(\projH)$. We will later see that
  $\hat{T}$ is not equal to any of the automorphisms $\hat{S}$ with $S \in \LieU (\hilbertH)$. 
  Observe also that by the dimension assumption on $\hilbertH$ there exists an anti-unitary 
  transformation, for example the real linear map 
  $T :\hilbertH \to \hilbertH$ which acts on some initially chosen Hilbert basis $(v_j)_{j\in J}$
  by $T(v_j) = v_j$ and $T (\cplxi v_j) = - \cplxi v_j$.

  One easily checks that the products $ST$ and $TS$ of a unitary operator $S:\hilbertH\to \hilbertH$ and an anti-unitary
  operator $T :\hilbertH \to \hilbertH$  are anti-unitary. If $T_1,T_2: \hilbertH \to \hilbertH$ are both
  anti-unitary, then the product $T_1T_2$ is unitary. Hence we obtain a new group $\LieAU(\hilbertH)$ 
  consisting of all unitary and anti-unitary operators on $\hilbertH$. The map
  \[
   \pi: \LieAU(\hilbertH) \to \Aut (\projH), \: S \mapsto \hat{S}
  \]
  then is a group homomorphism. Its kernel coincides with $\LieU(1) \cong \sphere^1$. To see this let 
  $\pi (S) = \id_{\projH}$. Then for every ray $\linel$ there exists a complex number $\mu_\linel $ 
  such that $Sv = \mu_{\C v} v$ for all $v\in \linel$. By unitarity $|\mu_\linel|=1$. 
  Let $v,w \in \hilbertH$ be two linearly independant vectors of norm $1$. Since 
  \[
    \mu_{\C(w-v)} (w-v) = S (w-v) = \mu_{\C w} w - \mu_{\C v} v\ ,
  \]
  one has $0 = (\mu_{\C(w-v)} -\mu_{\C w}) w + (\mu_{\C v}  - \mu_{\C (w-v)}) v$ which implies 
  $\mu_{\C w} = \mu_{\C(w-v)} =  \mu_{\C v} $ by linear independence of $v$ and $w$. 
  Hence all the $\mu_{\C v}$ coincide and $S = \mu \, \id_\hilbertH$ for some complex number 
  $\mu \in \LieU(1) \cong \sphere^1$. 
  A consequence of this observation is also that the homomorphism 
  $\pi|_{ \LieU(\hilbertH)} : \LieU(\hilbertH) \to \Aut(\projH)$, $S \mapsto \hat{S}$ is not surjective
  because for every anti-unitary $T$ and unitary $S$ the product $TS^{-1}$ is anti-unitary, hence can not 
  be an element of $\LieU(1)$. 
  We denote the image of  $ \LieU(\hilbertH)$ under $\pi$ by $\LieU(\projH)$ and call its elements 
  the \emph{unitary automorphisms} of $\projH$. 

  \begin{theorem}[Wigner's theorem, \cite{WigGAQA}]\label{thm:wigners-theorem}
    Let $\hilbertH$ be a complex Hilbert space of dimension $\geq 2$.  
    Then the sequence of group homomorphisms 
    \[
     1 \longrightarrow\LieU(1)\longrightarrow\LieAU (\hilbertH)\xrightarrow{\pi}\Aut(\projH)\longrightarrow 1
    \] 
    is exact. % i.e. $\pi$ is surjective, $\iota$ is injective, and $\ker(\pi) = \mathrm{im}(\iota)$. 
  \end{theorem}
  
  \begin{remark}
    Wigner's theorem was first stated in  \cite{WigGAQA}, but with an incomplete proof. 
    Only several years later complete and independent proofs of Wigner's result were given by
    \cite{UhlRSTQM}, \cite{LomMenWUAT}, and \cite{BarNWTSO}.
  \end{remark}
 
  \begin{proof}
     Wigner's theorem is an immediate consequence of the precedinmg considerations and 
     the following more general result. 
  \end{proof}
 
  \begin{theorem}[Optimal version of Wigner's theorem, \cite{GehEPNBVWT}]
     Let $\hilbertH$ be a complex Hilbert space of dimension $\geq 2$.  
     Then for every isometry $ A : \projH\to \projH$ there exists a 
     linear or conjugate-linear isometry $S : \hilbertH \to \hilbertH$ such that 
     $A = \hat{S}$, where $\hat{S}$ is the isometry on $\projH$ which maps
     the ray $\C v$ with $v \in \hilbertH \setminus \{ 0 \}$  to the ray $\C Sv$. 
  \end{theorem}

  \begin{proof}
    To prove the claim we will follow the elementary argument by \cite{GehEPNBVWT}. 
  \end{proof}
  
 
\subsection*{Lifting of projective representations and Bargmann's theorem}
\begin{theorem}[{\textsc{Bargmann's Theorem}}]\label{thm:bargmann-theorem}
  Let $\hilbertH$ be a complex Hilbert space and $G$ a connected and 
  simply connected Lie group with $H^2(\lieg,\fldR)= 0$. 
  Then every projective representation $\tau:G\to \LieU(\P\hilbertH)$ 
  can be lifted to a unitary representation 
  $\sigma : G\to \LieU(H)$ that is 
  $\pi \circ \sigma = \tau$, where 
  $\pi : \LieU(\hilbertH)\to \LieU(\P\hilbertH)$ is the canonical projection.
\end{theorem}
 
\begin{remark}
  The lifting theorem was proved first in \cite{BarURRCG}. 
  The short proof we present here goes back to \cite{SimSPBCLPRLG}. 
  We closely follow his argument.
\end{remark}

\begin{proof}[Proof of the theorem]
  Let $E$ be the fibered product of $\pi$ and $\tau$ with the canonical 
  homomorphisms $\tildetau : E \to  \LieU(\hilbertH)$ and 
  $\pi^E : E \to G $. 
  For the resulting commutative diagram of groups with two exact rows
  \[
    \begin{tikzcd}  
    1 \arrow[r] & \LieU(1)  \arrow[r] \arrow[d,"\id",swap]& E \arrow[r,"\pi^E"]\arrow[d,"\tildetau",swap] & G \arrow[r]  \arrow[d,"\tau"] & 1 \\
    1 \arrow[r]  & \LieU(1)  \arrow[r] & \LieU(\hilbertH) \arrow[r,"\pi",swap] & \LieU(\P\hilbertH) \arrow[r] & 1   
    \end{tikzcd}
  \]
  we want to construct a section $s: G\to E$ of $\pi^E : E \to G$ 
  which is a splitting meaning that $s$ is a group homomorphism 
  and $\pi^E \circ s = \id_G$. With the construction of such an $s$ we are done
  because then the unitary representation $ \tildetau \circ s $
  is a lifting of the projective representation $\tau:G\to \LieU (\projH)$. 

  Observe that $E$ is a Lie group by Kuranishi's theorem, see \cite[\S 4.3]{MonZipTTG},  since $E$ is 
  central extension of a Lie group, hence locally compact, 
  and there exist local continuous sections $\sigma :U\to E$ 
  that is $U\subset G$ is open and $\pi^E \circ \sigma = \id_U$. 

  The short exact sequence of Lie groups 
  \[
   1 \longrightarrow \LieU(1) \longrightarrow E \xrightarrow{\pi^E} G\longrightarrow 1
  \] 
  induces a short exact sequence of Lie algebras 
  \begin{equation}
  \label{eq:bargmann-theorem-exact-lie-algebra-sequence}
    0 \longrightarrow \fldR \longrightarrow \liee \xrightarrow{T\pi^E} \lieg\longrightarrow 0 \ ,
  \end{equation}
  where $\liee$ is the Lie algebra of $E$ and $\lieg$ the one of $G$.
  Observe that $T\pi^E$ is surjective with kernel $\fldR$ being in the center 
  of $\liee$. Choose a linear map $\lambda:\lieg\to \liee$ such that 
  $\pi^E\circ\lambda = \id_\lieg$. Put $\Theta(x,y) = [\lambda (x),\lambda (y)] -\lambda([x,y])$
  for all $x,y\in\lieg$. Then 
  \[ 
     T\pi^E\circ\Theta(x,y) = [T\pi^E \circ \lambda (x),T\pi^E \circ \lambda (y)]-
     T\pi^E\circ \lambda ([x,y])  = [x,y]-[x,y] = 0 \ .
  \]
  Hence $\Theta (x,y)$ is in the kernel of $T\pi^E$ which means that $\Theta$ is a map $\lieg\times\lieg \to \R$. 
  By definition, $\Theta:\lieg\times \lieg \to \fldR$ is skew symmetric. Let us show that it satisfies the 
  Jacobi identity. Compute, using the Jacobi identity for the Lie algebra bracket and the fact that 
  $\Theta$ has image in the center of $\liee$, 
  \begin{equation*}
    \begin{split}
      \Theta ([x,y]) ,z) + \, & \Theta ([y,z],x) +  \Theta ([z,x],y) = \\
      = \, & [\lambda ([x,y]),\lambda (z)]  + [\lambda ([y,z]),\lambda (x)]  + [\lambda ([z,x]),\lambda (y)] -
         \\
      &  -\lambda([[x,y],z]) -\lambda([[y,z],x]) - \lambda([[z,x],y]) = \\
      = \, &  [[\lambda(x),\lambda(y)],\lambda (z)] +  [[\lambda(y),\lambda(z)],\lambda (x)]  +  [[\lambda(z),\lambda(x)],\lambda (y)] - \\
      &  - [\Theta ([x,y]),\lambda (z)] - [\Theta ([y,z]),\lambda (x)] - [\Theta ([z,x]),\lambda (y)] = 0 \ .
    \end{split}
  \end{equation*}
  Therefore, $\Theta$ is a Lie algebra 2-cocycle. By $H^2(\lieg,\fldR) = 0 $, there exists a linear $\theta :\lieg \to \fldR$ such that 
  $\Theta(x,y) = \theta ([x,y])$ for all $x,y\in \lieg$. Put $\mu(x) = \lambda(x) + \theta (x)$. Then, since $\theta$ has values in the center of $\liee$,
   \begin{equation*}
    \begin{split} 
    [\mu(x),\mu(y)] & = [\lambda(x)+\theta(x),\lambda(y)+\theta(y)] =  
    [\lambda(x),\lambda(y)]  = \\
    & = \Theta (x,y) + \lambda ([x,y])  = \theta([x,y])+\lambda([x,y]) = \mu([x,y]) \ .
   \end{split}
  \end{equation*}
  Hence $\mu :\lieg \to \liee$ is a Lie-Algebra homomorphism and fulfills
  \[
    T\pi^E\circ\mu(x) = T\pi^E(\lambda(x)+\theta(x)) = T\pi^E(\lambda(x)) = x \quad\text{for all } x\in \lieg \ . 
  \]
  So $\mu$ is also a section of $T\pi^E$ which shows that the short exact sequence of Lie algebras 
  \eqref{eq:bargmann-theorem-exact-lie-algebra-sequence} is split. 
 
  By $\pi_1(G) = 1$, the Lie algebra homomorphism $\mu:\lieg \to \liee$ has a lifting to a group homomorphism $s:G\to E$
  such that $\pi^E\circ s = \id_\lieg$. The proof is finished.
\end{proof}