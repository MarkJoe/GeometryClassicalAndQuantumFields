% Copyright 2017 Markus J. Pflaum
% main author: 
%   Markus J. Pflaum 
%
\section{A combinatorial interlude}
\label{sec:combinatorial-interlude}
\subsection*{Multi-Indices}

\para 
Assume that $\mathscr{I}$ is a non-empty set which we call \emph{index set}. 
By a \emph{multi-index} over $\mathscr{I}$ we then understand an element $\alpha \in \N^{(\mathscr{I})}$ that is a family 
$\alpha = (\alpha_i)_{i\in \mathscr{I}}$ of natural numbers such that only finitely many $\alpha_i$ are non-zero. 
The \emph{order} of such a multi-index is defined by $|\alpha|:=\sum_{i\in \mathscr{I}} \alpha_i$. For $k\ \in \N$ 
and $ k_1 \in \N$ and $k_2 \in \N \cup\{ \infty\}$ with $k_1 \leq k_2$ we denote by $\N^{(\mathscr{I})}_{k}$ 
the set of all multi-indices over $\mathscr{I}$ of order $k$ and by $\N^{(\mathscr{I})}_{k_1,k_2}$
the set of all multi-indices of  order less or equal $k_2$ which have order  greater or equal $k_1$.
For reasons of clarity, which will be become obvious below, we sometimes also refer to an element of 
$\N^{(\mathscr{I})}$ as a \emph{Greek multi-index}.

\begin{example}
  In most cases the index set $\mathscr{I}$ will be of the form $\mathscr{I} = \{ 1,\ldots , d\}$  or of the form 
  $\mathscr{I} = \{ 0 ,\ldots , d-1\}$ for some positive integer $d$. One then has $\N^{(\mathscr{I})} = \N^\mathscr{I} = \N^d$
  and multi-indices are given by  $d$-tuples of the form $\alpha = (\alpha_1,\ldots ,\alpha_d)$ or
  $\beta = (\beta_0,\ldots ,\beta_{d-1})$, respectively. 
\end{example}

\para The space of multi-indices $\N^{(\mathscr{I})}$ carries the structure of a module over the semiring $\N$ in the sense of \cite{JohManMS}
that is $\N^{(\mathscr{I})}$ together with componentwise addition is an abelian monoid, componentwise multiplication with scalars 
is associative, $0$ acts as zero map, $1$ acts as identity, and the distributivity laws hold true.    
Moreover,  $\N^{(\mathscr{I})}$ is free over the family of multi-indices $(1_i)_{i\in \mathscr{I}}$ defined by
\[ 
  1_i (j) := 
  \begin{cases}
    1 & \text{for } j = i,\\
    0 & \text{else}.
  \end{cases}
\]

\para By a \emph{Roman multi-index}  of a given \emph{order} $k\in \N_{>0}$ over some index set $\mathscr{I}$ we understand an element 
$\indI$ of the cartesian product $\mathscr{I}^k$. For $k=0$ we define $\mathscr{I}^0$ as the set $\{ \zerO \}$, where $\zerO$ is a fixed set not appearing 
as an element of $\mathscr{I}$. We call $\zerO$  the \emph{Roman multi-index of order} $0$ over $\mathscr{I}$. We sometimes write $|\indI|$ for the order 
of a Roman multi-index. Note that we denote elements of $\mathscr{I}^k$ by capital Roman letters $\indI,\indJ,\ldots$ and their components by 
their respective  small Roman letters $i_l$, $j_l$,  and so on.

For $k\geq 1$ the symmetric group $S_k$ acts in a canoncial way on $\mathscr{I}^k$. We denote the orbit space of this action by $\overline{\mathscr{I}^k}$
and the orbit through a Roman multi-index $\indI\in \mathscr{I}^k$ by $\overline{\indI}$. In other words $\overline{\indI}$ is the equivalence class
of all Roman multi-indices obtained from $\indI$ by permutation of its components. For $k=0$ 
we identify $\overline{\mathscr{I}^0}$ with $\mathscr{I}^0$ and $\overline{\zerO}$ with $\zerO$. 

For Roman multi-indices $\indI =(i_1,\ldots , i_k) \in \mathscr{I}^k$ and $\indJ =(j_1,\ldots , j_l) \in \mathscr{I}^l$ of positive order we denote by
$\indI+\indJ$ the multi-index $(i_1,\ldots,i_k,j_1, \ldots , j_l) \in \mathscr{I}^{k+l}$. Obviously, the equivalence class
$\overline{\indI + \indJ}$ depends only on the equivalence classes $\overline{\indI}$ and $\overline{\indJ}$, hence
the operation $+$ descends to a map 
\[ +: \mathscr{I}^k/S_k \times \mathscr{I}^l/S_l \to \mathscr{I}^{k+l}/S_{k+l} \ . \] 
It is straightforward to see that this operation is associative and commutative.
Next define
\[ \indI+ \zerO = \zerO + \indI = \indI \quad \text{and} \quad
  \overline{\indI} + \overline{\zerO} = \overline{\zerO} +\overline{\indI} = \overline{\indI} \]
for all Roman multi-indices $\indI$ and put $\overline{\mathscr{I}^\bullet} := \mathscr{I}^\bullet /\!\!°\sim$, where 
$\mathscr{I}^\bullet := \bigsqcup_{k\in \N}  \mathscr{I}^k$ and $\sim$ is orbit equivalence which defines
two Roman multi-indices 
$\indI \in \mathscr{I}^k$ and $\indJ\in \mathscr{I}^l$ as equivalent if $k=l$ and $\overline{\indI} = \overline{\indJ}$.  
Then $\overline{\mathscr{I}^\bullet} = \bigsqcup_{k\in \N}  \overline{\mathscr{I}^k}$.
Moreover, $\overline{\mathscr{I}^\bullet}$ together with $+$ as binary operation and $\zerO$ as zero element becomes 
an abelian monoid. 

Note that every $i\in \mathscr{I}$ can be regarded as a Roman multi-index of order $1$ and that $\overline{i}=i$, so we have the sums  
$\indI+i = (i_1,\ldots, i_k,i)$ and $\overline{\indI}+i =\overline{(i_1,\ldots, i_k,i)}$. 
Sometimes we also write  $\indI i$ respectively $\overline{\indI} i$ for these sums.
 
\begin{lemma}
  Assume that $\mathscr{I}$ is totally ordered by some order relation $\leq$. Then every element of $\overline{\mathscr{I}^k}$
  of order $k\in \N_{>0}$ has a unique representative $\indI=(i_1,\ldots ,i_k) \in \mathscr{I}^k$ such that
  $i_1 \leq i_2\leq \ldots \leq i_k$. We call such  a representative an
  \emph{ordered Roman multi-index} or an \emph{increasing representative}. 
\end{lemma}
 
\begin{proof}
  One proves the claim by induction on the order $k$. For $k=1$ the claim is obvious.
  Assume that it holds for some $k$ and let $\overline{\indJ}$ be a Roman multi-index of order $k+1$.
  Let $j_m$ be the maximum of the components $j_1, \ldots , j_{k+1}$, and let $\sigma \in S_{k+1}$ be 
  the permutation switching $m$ and $k+1$ and acting by identity on the rest. 
  By hypothesis there exists a permutation $\tau\in S_k$ such that 
  $j_{\sigma \tau (1)} \leq \ldots \leq j_{\sigma \tau (k)}$. Put $\tau (k+1) = k+1$. Then
  $\tau \in S_{k+1}$ and $\indI =(j_{\sigma \tau (1)} , \ldots , j_{\sigma \tau (k+1)})$ is a representative
  of $\overline{\indJ}$ with the desired properties.  
  This finishes the inductive step and the claim is proved.   
\end{proof}

\begin{proposition}
  Let $\mathscr{I}$ be an index set with a total order $\leq$ on it and  
  $\kappa : \N^{(\mathscr{I})} \to \mathscr{I}^\bullet$ the map which maps the zero map 
  $0_\mathscr{I} : \mathscr{I} \to \N$ to $\zerO$ and a Greek multi-index $\alpha$ of positive order to the Roman multi-index 
  \[
    \big( \underbrace{i_1,\ldots,i_1}_{\alpha_{i_1}\text{ times}},  \underbrace{i_2 , \ldots , i_2}_{\alpha_{i_2} \text{times}} , \ldots ,  \underbrace{i_l, \ldots , i_l}_{\alpha_{i_l} \text{times}} \big) \ , 
  \]
  where $i_1 < \ldots < i_l $ are the \textup{(}pairwise distinct and ordered\textup{)} elements $i \in \mathscr{I}$ with 
  non-vanishing component $\alpha_i$. Then the induced map
  $\overline{\kappa} : \N^{(\mathscr{I})} \to \overline{\mathscr{I}^\bullet}$, $\alpha\mapsto \overline{\kappa(\alpha)}$
  is an isomorphism of monoids and maps the space $\N^{(\mathscr{I})}_{k}$ of Greek multi-indices of order $k$ onto $\overline{\mathscr{I}^k}$.
  Moreover, if $\mathscr{I}$ is finite, then  $\N^{(\mathscr{I})}_{k}$ and $\overline{\mathscr{I}^k}$ are finite as well and both have cardinality given by
  \[
     \card{\N^{\mathscr{I}}_k} = \card{\overline{\mathscr{I}^k}} = \frac{1}{k!} \prod_{l=0}^{k-1} \big( \card{\mathscr{I}} + l \big) \ . 
  \] 
\end{proposition}
\begin{proof}
First we need to show that $\overline{\kappa}$ is a bijection.  To this end let us 
make our notation somewhat more precise and choose 
for each $\alpha \in \N^{(\mathscr{I})}\setminus \{ 0_\mathscr{I}\}$ 
the elements $i^{\alpha}_1 , \ldots , i^{\alpha}_{l_\alpha} \in \mathscr{I}$ so that 
$i^{\alpha}_1 <  \ldots < i^{\alpha}_{l_\alpha}$, $\alpha_{i^{\alpha}_j} > 0$ for $j =1,\ldots,l_\alpha$ and 
$\alpha_i =0$ for all $i \in \mathscr{I} \setminus\{ i^{\alpha}_1 , \ldots , i^{\alpha}_{l_\alpha}\}$.
Then 
\[
 \kappa (\alpha) = 
  \big( \underbrace{i^\alpha_1 , \ldots , i^\alpha_1}_{\alpha_{i^\alpha_1} \text{times}},  
   \underbrace{i^\alpha_2 , \ldots , i^\alpha_2}_{\alpha_{i^\alpha_2} \text{times}} , \ldots ,  
   \underbrace{i^\alpha_{l_\alpha}, \ldots , i^\alpha_{l_\alpha}}_{\alpha_{i^\alpha_{l_\alpha}} \text{times}} \big) \ .
\]
By construction, $\kappa (\alpha)$ is of increasing form. 
For each element $\overline{\indI} \in \overline{\mathscr{I}^\bullet}$ let $\indI$ be the representative of 
increasing form. Define $\lambda (\overline{\indI}) \in  \N^{(\mathscr{I})}$ as follows. If 
$\overline{\indI}=\overline{\zerO}$, put $\lambda (\overline{\indI}) = 0_\mathscr{I}$. 
If $\overline{\indI}\neq \overline{\zerO}$, let $i^\indI_1 < \ldots < i^\indI_{l_I}$ be the elements 
of $\mathscr{I}$ which appear in $\indI$. Then, for each $j=1,\ldots,l_I$ define $\alpha^\indI_{i^\indI_j}$ 
to be the number of times the index $i^\indI_j$ appears in $\indI$. For $i \in \mathscr{I}$ 
not appearing among the $i^\indI_j$ put $\alpha^\indI_i = 0$. Then define
\[
 \lambda (\overline{\indI}) = \alpha^\indI = \big( \alpha^\indI_i\big)_{i\in \mathscr{I}} \ .
\]
So we obtain a map $\lambda:  \overline{\mathscr{I}^\bullet}\to \N^{(\mathscr{I})}  $.
For given $\indI \neq \zerO$ one has by definition $l_{\alpha^\indI} = l_I$ and 
$i^{\alpha^\indI}_1 = i^\indI_1 , \ldots , i^{\alpha^\indI}_l = i^\indI_l$ where $l= l_{\alpha^\indI} = l_I$. Moreover,
the index $i= i^{\alpha^\indI}_j$, $j=1,\ldots ,l$ appears in 
$\overline{\kappa} \big(\lambda( \overline{\indI} ) \big)$ 
exactly $\alpha^\indI_i$ times which coincides with the number $i$ appears in $\indI$. 
Hence $\overline{\kappa} \big( \lambda( \overline{\indI}) \big) = \overline{\indI} $.
Now assume $\alpha \in \N^\mathscr{I} \setminus \{ 0_\mathscr{I} \}$ to be given
and let $\indI =\kappa (\alpha)$. Then $l_I = l_{\alpha} $ and 
$ i^\indI_1 = i^{\alpha}_1  , \ldots ,   i^\indI_l = i^{\alpha}_l$ for $l= l_I = l_{\alpha} $.
For each of the indices $i= i^{I}_j$, $j=1,\ldots ,l$, the $i$-th component 
of $\lambda \big(\overline{\kappa} (\alpha)  \big)$ 
coincides with $\alpha_i$. Hence $\lambda \big(\overline{\kappa} (\alpha)  \big) = \alpha$,
which finishes the proof that  $\overline{\kappa} $ is a bijection with inverse $\lambda$. 

By construction of $\kappa$ one has $|\kappa (\alpha)| = |\alpha|$ for all Greek multi-indices $\alpha$
which entails that for every $k\in\N$ the bijection $\kappa$ maps 
$\N^{(\mathscr{I})}_{k}$ onto $\overline{\mathscr{I}^k}$.

Also by construction it is clear that $\kappa (\alpha + \beta) = \kappa (\alpha) + \kappa (\beta)$ 
for all $\alpha,\beta \in \N^{(\mathscr{I})}$ and that
$\kappa (0_\mathscr{I}) = \zerO$. Hence $\kappa$ is a morphism of monoids. 

Now we will prove the formula for the cardinality of $\N^{\mathscr{I}}_k$ by double induction on $k$
and the cardinality of the index set $\mathscr{I}$. Obviously $\card{\N^{\mathscr{I}}_0}=1$, so the claim holds 
for $k=0$ and all finite index sets. Assume that it holds for some natural $k$ and all finite index sets. 
Now let $\mathscr{I}$ be an index set of cardinality $1$. Then
$\card{\N^{\mathscr{I}}_{k+1}}=1$ since there is only one natural number with absolute value $k+1$.
Next assume that the claim holds for $k+1$ and all index sets of cardinality less than $d$. 
Let  $\mathscr{I}$ be an index set of cardinality $d$. Order the elements of $\mathscr{I}$
in some way so that $\mathscr{I} = \{i_1,\ldots,i_d\}$ and $i_1 < \ldots < i_d$. 
The set $\N^{\mathscr{I}}_{k+1}$ is then the disjoint union of the set of all $\alpha\in \N^{\mathscr{I}}_{k+1}$ 
such that $\alpha_{i_d} = 0$ and the set of all $\alpha\in \N^{\mathscr{I}}_{k+1}$ such that $\alpha_{i_d} \geq 1$.
The first of these sets has cardinality 
\[
  \card{\N^{\{i_1,\ldots,i_{d-1}\}}_{k+1}} = \frac{1}{{k+1}!} \prod_{l=0}^{k} \big( d-1 + l \big) \ ,
\]
the second has cardinality
\[
  \card{\N^{\mathscr{I}}_{k}} = \frac{1}{k!} \prod_{l=0}^{k-1} \big( d + l \big)
\]
since the map 
\[
  \big\{ \alpha \in  \N^{\mathscr{I}}_{k+1}  \bigmid \alpha_{i_d} \geq 1 \big\} \to \N^{\mathscr{I}}_{k}: \: 
  \alpha \mapsto (\alpha_{i_1},\ldots, \alpha_{i_{d-1}},\alpha_{i_d}-1) \in \N^{\mathscr{I}}_{k}
\]
is a bijection. Hence
\begin{equation*}
  \begin{split}
     \card{\N^{\mathscr{I}}_{k+1}} \, & = \frac{1}{{k+1}!} \prod_{l=0}^{k} \big( d-1 + l \big) +
     \frac{1}{k!} \prod_{l=0}^{k-1} \big( d + l \big) = \\
     & = \frac{1}{{k+1}!} (d-1+k+1) \prod_{l=0}^{k-1} \big( d + l \big) 
     = \frac{1}{(k+1)!} \prod_{l=0}^{k} \big( d + l \big) 
  \end{split}
\end{equation*}
and the induction step is finished. 
The claim is proved.  
\end{proof}

\para 
 By a \emph{block} of a Roman multi-index $\indI$ of positive order we mean a Roman multi-index
 of the form 
 \[
    \indI_B = (i_{b_1},\ldots , i_{b_{\card{B}}}) \ ,
 \]     
 where  $B$ is a subset of $ \{1,\ldots k\}$ and the $b_1, \ldots , b_{\card{B}} \in \{ 1, \ldots , k\}$ are the elements of
 $B$ in increasing order. 
 One can now decompose a multi-index $\indI$ into blocks as follows. 
 Let  $\{ B_1, \ldots , B_r\}$ be a partition of $\{ 1, \ldots , k\}$ which we assume to be 
 lexicographically ordered  that means that $b_{11} < b_{21} < \ldots < b_{r1}$,
 where $B_j = \{b_{j1}, \ldots , b_{j\card{B_j}}\}$ and $b_{jm} < b_{jn}$ for $j=1, \dots , r$
 and $1 \leq m < n \leq \card{B_j}$.      
 To express that $\{ B_1, \ldots , B_r\}$ is a lexicographically ordered partition of 
 $\{ 1, \ldots , k\}$ by $r$ non-empty sets we write   
 \[
   B_1 \sqcup \ldots \sqcup B_r = \{1, \ldots , k \} \quad \& \quad \emptyset <  B_1 < \ldots < B_r \ .
 \] 
 Now put $\indI_j := \indI_{B_j}$ for $j=1, \dots , r$. Then the Roman multi-indices $\indI$ and 
 $\indI_1 + \ldots + \indI_r$ are equivalent which can be interpreted as  $\indI$ being decomposed into the 
 $r$ blocks $\indI_1, \ldots , \indI_r$. 
 More precisely, we call the $r$-tupel of pairs $\big( (\indI_1,B_1), \ldots , (\indI_r,B_r)\big)$ 
 a \emph{decomposition of\, $\indI$ into $r$ blocks} 
 and denote the space of such decompositions by $\operatorname{Block}^r(\indI)$.
 Note that the cardinality of $\operatorname{Block}^r (\indI)$ coincides with the Sterling number 
 of the second kind $\left\{\tiny\begin{matrix}k\\r\end{matrix}\tiny\right\}$ which gives the number 
 of ways the set $\{ 1, \ldots , k\}$ can be partitioned into $r$ subsets.



    
\subsection*{Multipowers and multiderivatives}

\para
Let $M$ be a manifold and $x = (x^1, \ldots , x^d) : U \to \R^d$ a local coordinate system. Let $\indI \in \{ 1, \ldots , d\}^k$
be a  Roman multi-index of positive order $k$. Then the product
\begin{equation}
\label{Eq:DefMultipowers} 
   x^\indI := x^{i_1} \cdot \ldots \cdot x^{i_k} 
\end{equation}
and, for every $f \in \shSmoothFcts (U)$, the higher derivative
\begin{equation}
  \label{Eq:DefMultiderivatives}
\frac{\partial^{|\indI|}f}{\partial x^\indI} := 
  \frac{\partial^kf}{\partial x^{i_1} \cdot \ldots \cdot \partial x^{i_k}} 
\end{equation}
are both invariant under permutations of the components of $\indI$, hence depend only on the equivalence class $\overline{\indI}$.
We therefore sometimes write $x^{\overline{\indI}}$ for $ x^\indI$ and $\frac{\partial^{|\overline{\indI}|}f}{\partial x^{\overline{\indI}}}$
for $ \frac{\partial^{|\indI|}f}{\partial x^\indI}$. In order $0$ one puts
$x^{\overline{\zerO}} := x^\zerO := 1$ and $\frac{\partial^{|\overline{\zerO}|}f}{\partial x^{\overline{\zerO}}} :=\frac{\partial^{|\zerO|}f}{\partial x^\zerO} := f$.
For a multi-index $\alpha \in \N^d$ one defines as usual
\begin{equation*}
   x^\alpha := (x^1)^{\alpha_1} \cdot \ldots \cdot (x^d)^{\alpha_d} 
\end{equation*} 
and
\begin{equation*}
  \frac{\partial^{|\alpha|}f}{\partial x^\alpha} := 
  \frac{\partial^{|\alpha|}f}{(\partial x^1)^{\alpha_1} \cdot \ldots \cdot (\partial x^d)^{\alpha_d}} :=
  \left( \frac{\partial}{\partial x^1} \right)^{\alpha_1} \cdot \ldots \cdot  \left( \frac{\partial}{\partial x^d} \right)^{\alpha_d} f        \ .
\end{equation*}  
If now $\alpha$ and $\indI$ are related by $\overline{\indI} = \kappa (\alpha)$, then
\[ x^{\overline{\indI}} =x^\alpha \quad \text{and} \quad \frac{\partial^{|\overline{\indI}|}f}{\partial x^{\overline{\indI}}} = \frac{\partial^{|\alpha|}f}{\partial x^\alpha} \]
by definition of $\kappa$ and invariance of the product respectively the higher derivative under
permutations of components of the multi-index.    
\begin{remark}
  Occasionally we need multipowers and multiderivatives over more general index sets. So let $\mathscr{I}$ 
  be an arbitrary but still finite 
  index set. Assume that the components of a coordinate system $x:U \to \R^\mathscr{I}$ are labelled $x^i$ where $i$ runs through the elements of $\mathscr{I}$.
  For $\indI \in \mathscr{I}^k$ equations \eqref{Eq:DefMultipowers} and  \eqref{Eq:DefMultiderivatives} then 
  can be used again to define  multipowers $ x^\indI$ and multiderivatives $\frac{\partial^{|\indI|}f}{\partial x^\indI}$.
  Note that both objects  are invariant under permutations of components of $\indI$, too, so 
  the corresponding expressions where $\indI$ is replaced by $\overline{\indI}$ are also well-defined. 
  Now let $\alpha = (\alpha_i)_{i\in \mathscr{I}}\in \N^\mathscr{I}$  be a Greek multi-index
  and $f \in \shSmoothFcts (U)$. One then defines 
  \[
    x^\alpha := \prod_{i\in \mathscr{I}} (x^i)^{\alpha_i}   
  \]
  and 
  \[
   \frac{\partial^{|\alpha|} f}{\partial x^\alpha} := 
   \prod_{i\in \mathscr{I}} \left( \frac{\partial}{\partial x^i} \right)^{\alpha_i} f \ .
  \]  
  One finally checks that when $\overline{\kappa}(\alpha) = \overline{\indI}$
  the equalities 
  $ x^\alpha = x^{\overline{\indI}}$ and
  $\frac{\partial^{|\alpha|} f}{\partial x^\alpha} = \frac{\partial^{|\overline{\indI}|}f}{\partial x^{\overline{\indI}}}$
  still hold true in this  more general situation. 
\end{remark}

\subsection*{The formula of Fa\`a-di-Bruno}

\begin{theorem}[Combinatorial form of Fa\`a-di-Bruno's formula]\label{thm:combinatorial-faa-di-bruno-formula}
  \hspace{2mm}
  Let\  $\mathcal{I}$\ and $\mathcal{J}$ denote finite index sets. Assume that $M$ and $N$ are smooth 
  manifolds and that we are given smooth charts $x: U \hookrightarrow \R^{\mathcal{I}}$ 
  and $y: V \hookrightarrow \R^{\mathcal{J}}$ over open domains $U \subset M$ and $V\subset N$.
  Assume further that $\varphi : U \to V$ is a smooth map. Denote by 
  $\varphi_j : U \to \R$ for $j\in \mathcal{J}$ its components that means that 
  $\varphi = (\varphi^j)_{j \in \mathcal{J}} $. Finally let $\indI \in \mathcal{I}^k$ be a
  Roman multi-index of positive order $k$. Then for every $f\in \shSmoothFcts (V)$ the following 
  equality holds true:
  \begin{equation}
    \label{eq:combinatorical-formula-faa-di-bruno}
    \frac{\partial^{|\indI|} (f\circ \varphi)}{\partial x^\indI} =
    \sum_{r=1}^k \sum_{\indJ \in \mathcal{J}^r\atop \indJ = (j_1 ,\ldots , j_r)} \sum_{B_1 \sqcup \ldots \sqcup  B_r = \{ 1, \ldots  , k\} \atop  \emptyset < B_1 < \ldots < B_r}
    \left( \frac{\partial^{|\indJ|} f}{\partial y^\indJ} \! \circ \! \varphi \right) \cdot 
    \frac{\partial^{|\indI_{B_1}|} \varphi^{j_1}}{\partial x^{\indI_{B_1}}} \cdot \ldots \cdot 
     \frac{\partial^{|\indI_{B_r}|} \varphi^{j_r}}{\partial x^{\indI_{B_r}}} \ .
  \end{equation}
\end{theorem}

\begin{proof}
  We prove the claim by induction on the length of the multi-index $\indI$. Assume to be given a Roman multi-index 
  $\indI \in \mathcal{I}^k$ of length $k= |\indI|=1$.
  Then there exists a unique $i\in\mathcal{I}$ such that $\indI=  ( i )$.  
  By the chain rule one computes
  \begin{equation*}
  \begin{split}
     \frac{\partial^{|\indI|} (f\circ \varphi)}{\partial x^\indI} =   \frac{\partial (f\circ \varphi)}{\partial x^i} =
     \sum_{j \in\mathcal{J}} \left( \frac{\partial f}{\partial y^j}  \circ \varphi \right) \cdot  \frac{\partial \varphi^j}{\partial x^i}  
     = 
     \sum_{\indJ \in \mathcal{J}^1 \atop \indJ = ( j_1) } \sum_{B_1 = \{ 1 \}} 
     \left( \frac{\partial^{|\indJ|} f}{\partial y^\indJ} \! \circ \! \varphi \right) \cdot 
     \frac{\partial^{|\indI_{B_1}|} \varphi^{j_1}}{\partial x^{\indI_{B_1}}}  \ ,
  \end{split}
  \end{equation*}
  hence the claim holds true for $k=1$. 

  Now assume that for some $k\geq 1$ the claim holds for all 
  Roman multi-indices   of order $\leq k$ over $\mathcal{I}$. 
  Assume that $\indI =( i_1,\ldots ,i_{k+1}) $ is a Roman multi-index of order $k+1$ over  $\mathcal{I}$. 
  Then $\indI = \indK + i_{k+1}$, where $\indK = (i_1,\ldots,i_k)$ is a Roman multi-index of order $k$. 
  Using the induction hypothesis for $\indK$, the product and the chain rule one obtains 
  \begin{equation*}
  \begin{split}
     & \frac{\partial^{|\indI|} (f\circ \varphi)}{\partial x^\indI}  = 
     \frac{\partial}{\partial x^{i_{k+1}}} \frac{\partial^{|\indK|} }{\partial x^{\indK}}  f\circ \varphi = \\
     & \: =  \frac{\partial}{\partial x^{i_{k+1}}} 
     \sum_{r=1}^k \sum_{\indJ \in \mathcal{J}^r\atop \indJ = (j_1, \ldots, j_r)} \sum_{B_1 \sqcup \ldots \sqcup  B_r = \{ 1, \ldots  , k\} \atop  \emptyset < B_1 < \ldots < B_r}
     \left( \frac{\partial^{|\indJ|} f}{\partial y^\indJ} \! \circ \! \varphi \right) \cdot 
     \frac{\partial^{|\indK_{B_1}|} \varphi^{j_1}}{\partial x^{\indK_{B_1}}} \cdot \ldots \cdot 
     \frac{\partial^{|\indK_{B_r}|} \varphi^{j_r}}{\partial x^{\indK_{B_r}}} = \\
     & \: =  \frac{\partial}{\partial x^{i_{k+1}}} 
     \sum_{r=1}^k \sum_{\indJ \in \mathcal{J}^r\atop \indJ = (j_1, \ldots, j_r)} \sum_{B_1 \sqcup \ldots \sqcup  B_r = \{ 1, \ldots  , k\} \atop  \emptyset < B_1 < \ldots < B_r}
     \left( \frac{\partial^{|\indJ|} f}{\partial y^\indJ} \! \circ \! \varphi \right) \cdot 
     \frac{\partial^{|\indI_{B_1}|} \varphi^{j_1}}{\partial x^{\indI_{B_1}}} \cdot \ldots \cdot 
     \frac{\partial^{|\indI_{B_r}|} \varphi^{j_r}}{\partial x^{\indI_{B_r}}} = \\
     & \: = \sum_{r=1}^k \sum_{\indJ \in \mathcal{J}^r\atop \indJ = (j_1 ,\ldots, j_r)}\sum_{j_{r+1} \in \mathcal{J}} 
     \sum_{B_1 \sqcup \ldots \sqcup  B_r = \{ 1, \ldots  , k\} \atop  \emptyset < B_1 < \ldots < B_r}   
     \left( \frac{\partial^{|\indJ|+1} f}{\partial y^{\indJ j_{r+1}}} \! \circ \! \varphi \right) \cdot 
     \frac{\partial^{|\indI_{B_1}|} \varphi^{j_1}}{\partial x^{\indI_{B_1}}} \cdot \ldots \cdot 
     \frac{\partial^{|\indI_{B_r}|} \varphi^{j_r}}{\partial x^{\indI_{B_r}}} \frac{\partial \varphi^{j_{r+1}}}{\partial x^{i_{k+1}}} + \\
     & \: + 
     \sum_{r=1}^k  \sum_{l=1}^r \! \sum_{\indJ \in \mathcal{J}^r\atop \indJ = (j_1 ,\ldots, j_r)} \!\!
     \sum_{B_1 \sqcup \ldots \sqcup  B_r = \{ 1, \ldots  , k\} \atop  \emptyset < B_1 < \ldots < B_r} \!  
     \left( \frac{\partial^{|\indJ|} f}{\partial y^\indJ} \! \circ \! \varphi \right) \! \cdot 
     \frac{\partial^{|\indI_{B_1}|} \varphi^{j_1}}{\partial x^{\indI_{B_1}}} \cdot \ldots \cdot 
     \frac{\partial^{|\indI_{B_l}|+1} \varphi^{j_l}}{\partial x^{\indI_{B_l}i_{k+1}}} \cdot \ldots \cdot 
     \frac{\partial^{|\indI_{B_r}|} \varphi^{j_r}}{\partial x^{\indI_{B_r}}} = \\
     & \: =  \sum_{r=2}^{k+1} \sum_{\indJ \in \mathcal{J}^r\atop \indJ = (j_1 ,\ldots, j_r)} 
     \sum_{B_1 \sqcup \ldots \sqcup  B_r = \{ 1, \ldots  , k+1\} \atop  \emptyset < B_1 < \ldots < B_r = \{ k +1 \} } 
     \left( \frac{\partial^{|\indJ|} f}{\partial y^\indJ} \! \circ \! \varphi \right) \cdot 
     \frac{\partial^{|\indI_{B_1}|} \varphi^{j_1}}{\partial x^{\indI_{B_1}}} \cdot \ldots \cdot 
     \frac{\partial^{|\indI_{B_r}|} \varphi^{j_r}}{\partial x^{\indI_{B_r}}}  + \\
     &+  \sum_{r=1}^{k}  \sum_{\indJ \in \mathcal{J}^r\atop \indJ = (j_1 ,\ldots, j_r)} 
     \sum_{B_1 \sqcup \ldots \sqcup  B_r = \{ 1, \ldots  , k+1\} \atop  \emptyset < B_1 < \ldots < B_r \neq \{ k+1 \}  } 
     \left( \frac{\partial^{|\indJ|} f}{\partial y^\indJ} \! \circ \! \varphi \right) \! \cdot 
     \frac{\partial^{|\indI_{B_1}|} \varphi^{j_1}}{\partial x^{\indI_{B_1}}} \cdot \ldots \cdot 
     \frac{\partial^{|\indI_{B_r}|} \varphi^{j_r}}{\partial x^{\indI_{B_r}}} = \\ 
     & = \sum_{r=1}^{k+1} \sum_{\indJ \in \mathcal{J}^r\atop \indJ = (j_1 ,\ldots , j_r)} 
     \sum_{B_1 \sqcup \ldots \sqcup  B_r = \{ 1, \ldots  , k+1 \} \atop  \emptyset < B_1 < \ldots < B_r}
     \left( \frac{\partial^{|\indJ|} f}{\partial y^\indJ} \! \circ \! \varphi \right) \cdot 
     \frac{\partial^{|\indI_{B_1}|} \varphi^{j_1}}{\partial x^{\indI_{B_1}}} \cdot \ldots \cdot 
     \frac{\partial^{|\indI_{B_r}|} \varphi^{j_r}}{\partial x^{\indI_{B_r}}} \ .
  \end{split}
  \end{equation*}
  This concludes the induction step and the theorem is proved.
\end{proof}