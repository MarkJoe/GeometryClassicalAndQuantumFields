% Copyright 2018 Markus J. Pflaum
% main author: 
%   Markus J. Pflaum 
%
\section{Linear differential operators over commutative rings}

\para
In this section, $A$ will always denote  a commutative unital algebra over a field of characteristic zero $\fldk$. The identity element of $A$ will be denoted  by $1$. 
Let $M,N$ be two $A$-modules. An element $a\in A$ then acts in  two natural ways on the space $\Hom_\fldk(M,N)$ of 
$\fldk$-linear maps from $M$ to $N$, namely by 
\begin{equation}
  \label{eq:canonical-left-action-linear-maps}
  a_* : \Hom_\fldk(M,N) \to \Hom_\fldk(M,N), \: f \mapsto a_* f = a f = \big( M \ni m \mapsto a f(m) \in N \big)
\end{equation}
and  
\begin{equation}
  \label{eq:canonical-right-action-linear-maps}
  a^* : \Hom_\fldk(M,N) \to \Hom_\fldk(M,N), \: f \mapsto a^* f = f a = \big( M \ni m \mapsto f (a m) \in N \big) \ . 
\end{equation}

\begin{propanddef}
  The actions $a_*$ and $a^*$ define two $A$-module structures on $\Hom_\fldk(M,N)$ which are called the 
  \emph{canonical left} and the \emph{canonical right} $A$-\emph{module structures}, respectively.  
  These module structures commute.  
\end{propanddef}

\begin{proof}
  In the following let $a,b\in A$ and $f, g \in \Hom_\fldk (M,N)$.
  Then one computes for $m\in M$ 
  \begin{equation*} 
    \begin{split}
      \big( (a+b)_* f \big) (m) & = (a+b) \big( f(m) \big) =   a \big( f(m) \big) + b \big( f(m) \big)
      \\ & = \big( a_* f \big) (m) + \big( b_* f \big) (m) =  \big( a_* f  + b_* f \big) (m) \ , \\[2mm]
      \big( a_* (f+g) \big) (m) & = a \big( f(m) + g(m)\big) = a f(m) + a g(m) = \big( a_*f + a_*g\big)(m) \ ,    \\[2mm]  
      \big( a_*b_* f \big) (m) & = a \big( b f(m)\big) = (ab) \big( f(m) \big) = \big( (ab)_* f\big) (m)  \ , \\[2mm]
      \big( 1_* f \big) (m) & = 1 \cdot f(m) = f(m)  \ , 
    \end{split}
  \end{equation*}
 and
  \begin{equation*} 
    \begin{split}
      \big( (a+b)^* f \big) (m) & =  f\big( (a+b)m\big) =   f( am)+ f(bm) 
       \\ & = \big( a^* f \big) (m) + \big( b^* f \big) (m) =  \big( a^* f  + b^* f \big) (m) \ ,  \\[2mm]
      \big( a^* (f+g) \big) (m) & = f(am) + g(am) = a^* f(m) + a^* g(m) = \big( a^*f + a^*g\big)(m) \ ,   \\[2mm]  
      \big( a^*b^* f \big) (m) & = \big( b^* f \big) (am) = f \big( ( b (am) \big) \big) =  f \big( (ab)m \big) = \big( (ab)_* f\big) (m)  \ , \\[2mm]
      \big( 1^* F \big) (m) & = F(1\cdot m) = F(m) \ .
    \end{split}
  \end{equation*}
  This proves the module properties.  It remains to show  that $a_* b^* f = b^* a_* f$.
  But that is clear since for all $m\in M$
  \[ \big( a_* b^* f \big)  (m) =  a \big( ( b^* f )  (m) \big) =  a \big( f(bm)  \big) =  \big( a_* f \big) (bm) = \big( b^* a_* f \big) (m) \ . \]
\end{proof}

\begin{remark}
  By the preceding proposition  $\Hom_\fldk(M,N)$ becomes an $A$-bimodule which is not symmetric, in general,  
  unless for example $M=N=A$. We regard $\Hom_\fldk(M,N)$ always as an object in the 
  category of $A$-bimodules. 
  When we want to consider only  the canonical left or the canonical right $A$-module structure on the space of $\fldk$-linear maps from $M$ to $N$
  we write  ${}_A\!\Hom_\fldk(M,N)$ and  $\Hom_{\fldk,A}(M,N)$, respectively, for the resulting objects in the category of $A$-modules. 
\end{remark}

\begin{definition}
  For every $a\in A$ denote by $\ad_a : \Hom_\fldk (M,N) \to \Hom_\fldk (M,N)$ the $\fldk$-linear map 
  $a_* - a^*$ and call it the \emph{adjoint action} of $a$. 
\end{definition}

\begin{lemma}
  Let $M,N,P$ be $A$-modules. Then one has for all $f \in \Hom_\fldk (M,N)$, $g\in \Hom_\fldk (N,P)$ and all $a,b \in A$
  \begin{align}
    \label{eq:adjoint-action-product-homomorphisms}
    \ad_{ab} f  & =  a_* (\ad_b f) + b^* (\ad_a f) =  a^* (\ad_b f) + b_* (\ad_a f) \ , \\
    \label{eq:adjoint-action-composition-homomorphisms}
    \ad_a ( g \circ f ) & =  (\ad_a g )\circ f + g \circ (\ad_a f) \ .
  \end{align}
\end{lemma}

\begin{proof}
  Compute by observing  that the left and right $A$-module structures commute:
  \begin{equation*}
    \begin{split}
      \ad_{ab} f = (ab)_* f - (ab)^*f = a_* (b_* f - b^* f) + b^* (a_* f - a^*f)  =  a_* (\ad_b f) + b^* (\ad_a f) \ . 
    \end{split}
  \end{equation*}
  By symmetry in $a$ and $b$ the first claimed equality follows. For the second observe that 
  $(a^*g) \circ f = g \circ (a_* f)$ and compute
  \begin{equation*}
   \begin{split}
      \ad_a (g \circ f) & = a_*(g\circ f) - a^*(g\circ f) = (a_*g - a^*g) \circ f + g \circ (a_* f - a^*f) = \\
      & = (\ad_a g )\circ f + g \circ (\ad_a f) \ .
    \end{split}
  \end{equation*}
\end{proof}

\begin{definition}
  For all $A$-modules  $M,N$ the space $\diffOps^0 (M,N)$ of \emph{linear differential operators} 
  of \emph{order} $0$  from $M$ to $N$ is defined as the set of $D \in \Hom_\fldk(M,N)$ such that 
  \[
    \ad_{a} D = 0 \quad \text{for all } a \in A \ .
  \] 
  Recursively, one defines  the space $\diffOps^k (M,N)$ of \emph{linear differential operators} 
  of \emph{order} $\leq k+1$  from $M$ to $N$ as the set of all $D \in \Hom_\fldk(M,N)$ such that 
   \[
    \ad_{a} D\in \diffOps^k  (M,N) \quad \text{for all } a \in A \ .
  \]  

  The space  $\Der_\fldk (A ,N)$ of \emph{derivations} in $N$ is defined as
  the set of all   $D\in \Hom_\fldk(A,N)$ for which the Leibniz rule holds that is for which 
  \[
     D (ab) = a D(b) + bD(a) \quad \text{for all } a,b \in A \ .
  \]
\end{definition}

\begin{remark}
    By definition,  $\diffOps^0 (M,N)$ coincides with the space $\Hom_A (M,N)$ of $A$-module maps 
    from $M$ to $N$. By induction on $k$ it becomes clear that $\diffOps^k (M,N)$ can be equivalently described 
    as the set of all $D \in \Hom_\fldk (M,N)$ such that
    \[
      \left( \ad_{a_0} \circ \ldots \circ \ad_{a_k}\right)  D = 0  \quad \text{for all } a_0,\ldots , a_k \in A \ . 
    \]   
\end{remark}


\begin{proposition} \label{thm:properties-linear-differential-operators}
  Let $M,N,P$ be two $A$-modules. Then the following holds true for all $k,l\in \N$.
  \begin{romanlist}
  \item
    The space $\diffOps^k (M,N)$ inherits from $\Hom_\fldk (M,N)$ both $A$-module  structures so 
    is an $A$-subbimodule of $\Hom_\fldk (M,N)$. The two $A$-module structures coincide on $\diffOps^0 (M,N)$ but 
    in general not on  spaces of differential operators of higher order.
   \item\label{ite:inclusion-differential-operators-higher-orders}
    One has a canonical inclusion
      \[ \diffOps^k (M,N) \subset \diffOps^{k+1} (M,N) \ . \]
   \item
      The composition of a differential operator $\Delta \in \diffOps^k (N,P)$
      with a differential operator $D \in \diffOps^l (M,N)$ is a linear differential operator of degree $\leq k+l$.
   \item\label{ite:derivations-left-no-right-submodule-differential-operators}
    The space of derivations $\Der_\fldk (A ,N)$ is an $A$-submodule of $\diffOps^1 (M,N)$ with respect to the 
    canonical left $A$-module structure but in general not an $A$-submodule of $\diffOps^1 (M,N)$ with respect to the canonical 
    right $A$-module structure. 
\end{romanlist}
\end{proposition}

\begin{proof}
\begin{adromanlist}
\item
    The claim for $\diffOps^0 (M,N)$ holds since for every $D \in \diffOps^0 (M,N)$ and $a\in A$
    the operators $a_*D$ and $a^*D$ coincide and are both $A$-linear again  
     by the following equalities.
    \begin{equation*}
    \begin{split} 
        (a^* D) (m) & = D (a m) = D( am) = a(D(m)) = (a_*D)(m) \quad \text{for all } m\in M \text{ and} \\
        (a_* D) (bm) & = a (D (bm)) = ab (D(m)) = b(aD(m)) = b (a_*D(m)) \quad \text{for all } b \in A, \: m\in M \ .  
%       (a^* D) (m) & = D (a m) = D( am) = a(D(m)) = a_*D(m) \quad \text{for all } b\in A, \: m\in M \ .
    \end{split}
    \end{equation*} 
    Under the assumption that $\diffOps^k (M,N)$ inherits the $A$-bimodule structure from $\Hom_\fldk (M,N)$ 
    one checks  for $ D \in \diffOps^{k+1} (M,N)$ 
    \begin{equation*}
    \begin{split}
        \ad_b (a_*D) & = b_*a_* D - b^*a_*D = a_*(b_* D- b^*D) = a_* (\ad_b D)  \in \diffOps^k (M,N) \quad \text{and} \\
        \ad_b (a^* D) & = b_*a^* D - b^*a^*D = a^*(b_* D- b^*D) = a^* (\ad_b D)  \in \diffOps^k (M,N)  \ .
    \end{split}
    \end{equation*} 
    By induction $\diffOps^k (M,N)$ therefore is an $A$-subbimodule of $\Hom_\fldk (M,N)$
    for all $k\in \N$. Even though the two $A$-module structures coincide on $\diffOps^0 (M,N)$ they 
    do not on spaces of differential operators of order $1$ (and higher) as 
    \Cref{ex:right-multiplied-derivation-n--derivation} below shows.
\item
    This is obvious by definition and an inductive argument.  
\item  
    If $k+l =0$ the claim is clear since then both $\Delta$ and $D$ are $A$-linear, hence their composition is so, too.
    Assume that for some natural $n$ the claim holds for all $k,l \in \N$ with $k+l\leq n$.
    Then assume  $k+l = n+1$ and let  $\Delta \in \diffOps^k (N,P)$ and $D \in \diffOps^l (M,N)$.
    Now compute  using \Cref{eq:adjoint-action-composition-homomorphisms}
    \begin{equation*}
    \begin{split}
        \ad_a (\Delta \circ D)  & = (\ad_a \Delta) \circ D + \Delta \circ (\ad_a D) \ . 
    \end{split}
    \end{equation*} 
    By inductive hypothesis the right hand side is a differential operator of order $\leq n$, hence
    $\Delta \circ D  \in \diffOps^{k+l} (M,P)$.
  \item
    The space of derivations $\Der_\fldk (A ,N)$ is an $A$-submodule of $\diffOps^1 (M,N)$ with respect to the 
    canonical left $A$-module structure. Namely, if $D \in \Der_\fldk (A ,N)$ and $a,b,c\in A$, then
    \[
      (a_* D) (bc) = aD(bc) = ab D(c) + ac D(b) = b (aD(c)) + c(a D(b))  = b (a_*D) (c) + c (a_*D)(b) \ .
    \] 
    In general, $\Der_\fldk (A ,N)$ is not an $A$-submodule of $\diffOps^1 (M,N)$ with respect to the canonical 
    right $A$-module structure. 
\end{adromanlist}
\end{proof}  

\begin{example}\label{ex:right-multiplied-derivation-n--derivation}
    Let  $A =\fldk [X_1,\ldots , X_n]$ be the polynomial ring over $\fldk$ in $n$ indeterminates 
    and $\Omega^1_{A/\fldk}$ the space of K\"ahler differentials of $A$ that is the space $I/I^2$, where $I$ is the kernel of the 
    multiplication map $\mu:A\otimes_\fldk A\to A$.
    The canonical map $d: A \to \Omega^1_{A/\fldk}$, $ a \mapsto da = 1\otimes a - a \otimes 1 + I^2$ then is a derivation 
    and $\Omega^1_{A/\fldk}$ an $A$-module which is free over the elements $dX_1,\ldots , dX_n $. 
    If now $a \in A \: \setminus \: \fldk$, then
    \[
      a^* d (1) = d a \neq 0 \ ,
    \]  
    so $a^*d$ can not be a derivation. Note that $a_*d$ is a derivation by 
    \Cref{thm:properties-linear-differential-operators} \ref{ite:derivations-left-no-right-submodule-differential-operators}. 
\end{example}


\para
    By \Cref{thm:properties-linear-differential-operators} one has a (filtered) diagram in the category of $A$-bimodules
    \begin{equation}
      \label{eq:filtered-diagram-linear-differential-operators-commutative-ring}
      \diffOps^0(M,N) \hooklongrightarrow \diffOps^1(M,N) \hooklongrightarrow \ldots \hooklongrightarrow \diffOps^k(M,N)\hooklongrightarrow \ .
    \end{equation}
    Its colimit  exists and coincides with the union of the $\diffOps^k(M,N)$, $k\in \N$.
    We will denote it by $\diffOps(M,N)$ and call it the $A$-bimodules of linear differential operators from $M$ to $N$.

\begin{remark}
    In case we want to consider the spaces $\diffOps^k (M,N)$ and $\diffOps (M,N)$ with their canonical left 
    $A$-module structure, only, we write ${}_A\diffOps^k(M,N)$ and ${}_A\diffOps(M,N)$, respectively.
    Analogously, when we regard $\diffOps^k (M,N)$ and $\diffOps (M,N)$ as objects in the category of $A$-modules  
    with their canonical right  $A$-module structure we denote them by  
    $\diffOps^k_A(M,N)$ and  $\diffOps_{\!A}(M,N)$, respectively. By $\diffOps(M)$ 
\end{remark}
  



\begin{proposition}
  Assigning to every pair  of $A$-modules $(M,N)$ the $A$-bimodule $\diffOps^k(M,N)$ and
  to every pair of $A$-module maps $f:M'\to M$ and $g:N\to N'$ the $A$-bimodule map
  $(f^*,g_*) : \diffOps^k(M,N) \to \diffOps^k(M',N')$, $D \mapsto g\circ D \circ f$ 
  comprises a bifunctor which is contravariant in the first
  and covariant in the second argument. Analogously, the assignment $(M,N) \to \diffOps (M,N)$ becomes 
  a bifunctor.   
\end{proposition}
\begin{proof}
  By definition, $\big( (\id_M)^*,(id_N)_*\big) D = D$ for every $D \in \diffOps^k(M,N)$, so
  \[ \big( (\id_M)^*,(id_N)_*\big) =\id_{\diffOps^k(M,N)} \ . \]
  Let $M_1,M_2,M_3, N_1,N_2,N_3$ denote $A$-modules and assume to be given
  $A$-modules maps $f_1:M_2\to M_1$, $f_2:M_3\to M_2$, $g_1 :N_1\to N_2$, and $g_2:N_2\to N_3$.
  Then
  \begin{equation*}
    \begin{split}
    \Big( \big( f_2^*,g_{2*}\big) \circ \big( f_1^*,g_{1*}\big) \Big) D & =
    \big( f_2^*,g_{2*}\big) \big( g_1\circ D \circ f_1\big) = (g_2\circ g_1)\circ D\circ (f_1\circ f_2) = \\
    & = \big( (f_1\circ f_2)^*, (g_2\circ g_1)_* \big) D \ .      
    \end{split}
  \end{equation*}
  This proves that $\diffOps^k(-,-)$ and $\diffOps (-,-)$ are bifunctors contravariant in the first and
  covariant in the second argument. 
\end{proof}

\begin{theorem}
  Let $N$ be an $A$-module. Then the functors $\diffOps^k ( - , N) : {}_A\category{Mod} \to {}_A\category{Mod}_A$
  and $\diffOps ( - , N) : {}_A\category{Mod} \to {}_A\category{Mod}_A$
  are representable. Representing objects are given by the $A$-modules ${}_A\diffOps^k (N)$ and  ${}_A\diffOps (N)$, respectively.
\end{theorem}

$\De$
