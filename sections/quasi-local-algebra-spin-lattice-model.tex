% Copyright 2020 Markus J. Pflaum, licensed under CC BY-NC-ND 4.0
% main author: 
%   Markus J. Pflaum 
%
\section{The quasi-local algebra of a spin lattice model}
\label{sec:quasi-local-algebra-spin-lattice-model}

\para
By a \emph{Bravais lattice} or briefly just a \emph{lattice} one understands a subgroup $\Lambda$ 
of the additive group $\R^d$ of the form
\[
  \Lambda = \left\{ \sum_{i=1}^d \lambda_i a_i \bigmid \lambda_i \in \Z \text{ for } i=1,\ldots,d \right\} \ ,
\]
where $(a_1,\ldots , a_d)$ is a basis of $\R^d$. We then say that $\Lambda$ is the lattice 
\emph{induced} by the basis $(a_1,\ldots , a_d)$. The length $d$ of an inducing basis will be called
the \emph{dimension} of the lattice. Note that the dimension is uniquely determined by a given lattice
but that there might be several bases by which the lattice is induced. 
The lattice $\Z^d$ will be called the \emph{standard} or \emph{cubic lattice} in dimension $d$. 
It is induced by the standard basis $(e_1,\ldots,e_d)$ of $\R^d$.
\para
Let $\Lambda$ be a lattice of dimension $d$, and denote by $\powerf{\Lambda}$ the set of all finite
subsets of $\Lambda$. Fix a natural number $N \geq 1$ and call $\frac N2$ the \emph{spin degree}
of the spin lattice model we are going to define. For each $x\in \Lambda$ let $\hilbertH_x$ be
the $N+1$-dimensional complex Hilbert space $\C^{N+1}$. Now put for $\scrO\in \powerf{\Lambda}$
\[
  \hilbertH_\scrO = \bigotimes\limits_{x\in \scrO} \hilbertH_x 
\]
and define the \emph{local algebra} over $\scrO$ as the $C^*$-algebra
\[
  \falgA_\scrO = \blinOps (\hilbertH_\scrO  ) \ .
\]
Note that due to their finite dimensionality the tensor product of finitely many Hilbert spaces
$\hilbertH_x$ coincides here with their Hilbert tensor product. 
If $\scrO_1$ and $\scrO_2$ are two finite subsets of $\lambda$ such that   $\scrO_1$ is a subset of
$\scrO_2$, then one has the natural embedding
\[
  \alpha_{\scrO_1 ,\scrO_2} : \falgA_{\scrO_1} \hookrightarrow \falgA_{\scrO_2 }
\]
which, under the natural identification
$\falgA_\scrO \cong \bigotimes\limits_{x\in \scrO} \blinOps (\hilbertH_x)$, maps a tensor
of the form $\otimes_{x\in \scrO_1} A_x$ with $A_x \in \blinOps (\hilbertH_x)$ for all
$x \in \scrO_1$ to the simple tensor $\otimes_{x\in \scrO_2} A_x$, where $A_x$ is defined to be
$\idn_{\hilbertH_x}$ whenever $x \in \scrO_2\setminus \scrO_1$. In more abstract terms,
$\alpha_{\scrO_1 ,\scrO_2}$ is the unique linear map making the diagram


commute where
$ \pi_\scrO : \prod_{x\in \scrO} \falgA_x \to \bigotimes_{x\in \scrO} \falgA_x$
is the canonical projection mapping the family $(A_x)_{x\in \scrO}$ to
$\otimes_{x\in \scrO} A_x$ and $\overline{\alpha}_{\scrO_1 ,\scrO_2}$ is the map
\[
  \overline{\alpha}_{\scrO_1 ,\scrO_2} : \prod_{x\in \scrO_1}\falgA_x \to \bigotimes_{x\in \scrO_2}\falgA_x , \: (A_x)_{x\in \scrO_1} \mapsto
  (\otimes_{x\in \scrO_1} A_x) \otimes (\otimes_{x\in \scrO_2\setminus \scrO_1} \idn_{\hilbertH_x}) \ .
\]