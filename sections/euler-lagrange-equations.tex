% Copyright 2021 Markus J. Pflaum
% main author: 
%   Markus J. Pflaum 
%
\section{Euler-Lagrange equations}
\label{sec:euler-lagrange-equations}
\subsection*{Regular domains}
%
%\para
Before we come to the Euler-Lagrange equations of a variational problem we need to explain the kind of
domains over which we want to consider variational problems. To this end
recall first that by a triangulation of a topological space $X$ one understands a homeomorphism
of the form
\[ \kappa: |K| \rightarrow X \ , \]
where $|K|$  is the underlying topological space of
a (geometric) simplicial complex in some euclidean space $\R^n$. In the case where the topologial space $X$
is a closed subset of a compact manifold-with-boundary $\closure{M}$ we call the triangulation
\emph{piecewise smooth}
if for every simplex $\sigma \in K$ the restriction $\kappa|_\sigma : \sigma \to \kappa (\sigma)$ is a
diffeomorphism onto its image which means that the following two conditions hold.
\begin{romanlist}
\item
  For every smooth $f$ defined on an open neighborhood of $\kappa(\sigma)$,
  the pullback $\big(\kappa|_\sigma\big)^*f$ can be extended to a smooth function on the euclidean
  space $\R^n$ in which the simplicial complex $K$ lies.
\item
  For every smooth $g$ defined on an open neighborhood of the simplex $\sigma \subset \R^n$,
  the pullback $\big(\kappa|_\sigma^{-1}\big)^*g $ has a smooth extension to $\closure{M}$.
\end{romanlist}

After these preparatory remarks let $M$ be a smooth manifold of finite type that is let $M$ be
diffeomorphic to the interior of a compact manifold-with-boundary $\closure{M}$. Denote by $d$ the dimension
of $M$ and assume that $d > 0$. By a \emph{regular domain} in $M$ we now understand a
non-empty open connected subset $\Omega\subset M$ such that its closure $\closure{\Omega}$ in $\closure{M}$ possesses a piecewise smooth triangulation $\kappa : |K| \rightarrow \closure{\Omega}$, where $|K|$ is the
underlying topological space of a finite (geometric) simplicial complex.
It is further assumed that $\kappa^{-1} (\partial \Omega)$ and  $\kappa^{-1} (\partial \Omega \cap \partial M)$ are
simplicial subcomplex of $K$ of dimension $< d$ where $\partial \Omega $ denotes the topological boundary of
$\Omega$ in $\closure{M}$ and $\partial M $ is the boundary $\closure{M}\setminus M$. 

Regular domains will comprise the domains over which we consider variational problems.
In most applications, $\Omega$ will be the interior of a submanifold-with-corners of $\closure{M}$;
see \ref{sec-cring-project:manifolds-with-corners} and \cite{JoyMC} for details on manifolds-with-corners and
their submanifolds. In this section we will therefore consider mostly this particular case and only briefly
indicate how the argument goes in the more general situation.
For ease of exposition we will call a regular domain $\Omega \subset M$ such that
$\closure{\Omega} \subset M$ is a submanifold-with-corners a \emph{strongly regular domain}. If additionally the boundary $\partial\Omega$ is even smooth,
then we call $\Omega$ a \emph{strongly regular domain with smooth boundary}. 

In most cases we choose $M$ to coincide with the euclidean space $\R^d$. Note that $\R^d$
is a manifold of finite type and that it is diffeomorphic to the interior of the $d$-dimensional
closed unit ball $\closedball^d$ around the origin. A diffeomorphism between the euclidean space $\R^d$ and the interior $\ball^d$ of $\closedball^d$ is given by the smooth map
$\varphi: \R^d \to \ball^d$, $x \mapsto \frac{1}{\sqrt{1 + \| x\|^2}}\, x$. It has inverse
$\psi : \ball^d \to\R^d$, $y \mapsto \frac{1}{\sqrt{1 -\| y \|^2}}\, y$ as the following two
equalities show. 
\[
  \varphi(\psi(y))= \frac{1}{\sqrt{1 + \frac{\| y \|^2}{1 -\| y \|^2}}} \frac{1}{\sqrt{1 -\| y \|^2}}y=y  
\]
\[
  \psi(\varphi(x))= \frac{1}{\sqrt{1 - \frac{\| x \|^2}{1 +\| x \|^2}}} \frac{1}{\sqrt{1 +\| x \|^2}}x=x 
\]
In the remainder of this section we will identify $\R^d$ with its image in the $d$-dimensional
closed ball $\closedball^d$. Under this identification one can understand $\closedball^d$ as a certain
compactification of euclidean space $\R^d$. It is termed the \emph{radial compactification} of $\R^d$
and sometimes denoted by $\closedball^d_\infty$.
Last, we call the \emph{boundary} $\partial \closedball^d_\infty = \closedball^d_\infty \setminus \varphi (\R^d)$
the $(d-1)$-\emph{sphere at infinity} and denote it by the symbol $\sphere^{d-1}_\infty$. 

\subsection*{The local case}
\para Assume that $\Omega \subset \R^d$ is an open subset which can be identified with the
interior of a compact submanifold-with-boundary $\closure{\Omega} \subset \closedball^d_\infty$
under the above identification $\varphi :\R^d \to \ball^d_\infty$.
In particular this means that the boundary  $\partial{\Omega}$ is a closed submanifold of $\closedball^d_\infty$.
Later we will relax the assumptions and allow $\Omega$ to be a regular domain in $\R^d$.
We interpret the preimages $\varphi^{-1} (\closure{\Omega})$ and  $\varphi^{-1} (\partial{\Omega})$
as intersections $\closure{\Omega} \cap \R^d$ and 
$\partial{\Omega}\cap \R^d$, respectively. Note that both of these spaces are submanifolds of $\R^d$,
the first one possibly with boundary. 
Let $(x^1,\ldots,x^d): \Omega \to \R^d$ be the canonical coordinates of $\Omega$.
Observe  that $\closure{\Omega} \cap \R^d$ and $\Omega$ are oriented by the restriction of the
canonical volume form $dx^1\wedge \ldots \wedge dx^d$ to $\closure{\Omega} \cap \R^d$.
We denote that restriction  by $\omega$. 
% Note that the canonical charts $x^l$, $l=1,\ldots , d$ of the regular domain $\Omega$ 
% and the canonical volume form have unique extensions to the closure
% $\closure{\Omega}$. We will denote those extensions by the same symbols as the unextended
% functions.

Further we assume to be given a trivial smooth fiber bundle
$\pi: E = \closure{\Omega} \times F  \to \closure{\Omega} $ with typical fiber $F$ being a connected
open subset of some euclidean space $\R^n$. The canonical fiber coordinates 
will be denoted by $(u^1,\ldots,u^n):  F \to \R^n$. 
The canonical charts of the interior of the base and the fiber give rise to a fibered chart 
$(x,u): E  = \Omega \times F \to \R^d \times \R^n$. The fiber bundle
$\pi: E \to \closure{\Omega} $ and its associated jet bundles give rise to various kinds of section spaces which we
need in the following and which we briefly now recall. 
Assume to be given some  \emph{order} $m \in \N \cup\{\infty\}$ and a locally closed subset $X \subset \closure{\Omega}$.
Let $\tilde{E} \to  \closure{\Omega}$ be one of the bundles $E \to  \closure{\Omega}$ or $\Jet^kE \to \closure{\Omega}$,
where $k\in \N \cup\{\infty\}$. By $\Gamma^m (X; \tilde{E})$ we then denote the space of $m$-times continuously
differentiable sections of $\tilde{E}$ over $X$ that is of all continuous sections $s: X \to \tilde{E}$ which have an
$m$-times continuously differentiable extension to an open neighborhood of $X$ in $\closure{\Omega}$. 
The  subspace of all $s \in \Gamma^m (X; \tilde{E})$
with support compactly contained in $X\cap \R^d$ will be denoted by $\Gamma^m_0 (X; \tilde{E})$.
We often write $\Gamma (X; \tilde{E})$ instead of $\Gamma^0 (X; \tilde{E})$ for the space of continuous sections.
The space $\mathscr{E}^m (X; E)$ of \emph{Whitney fields} of order $m$ over $X$ with values in $E$ is defined by 
\[
  \mathscr{E}^m (X; E) = \big\{ S \in \Gamma (X; \Jet^m E) \bigmid
  \exists s \in \Gamma^m ( \closure{\Omega};E) :  \: \jet^m s|_X = S \big\} \ .
\] 
Analogously as for $\Gamma^m$ we denote by $\mathscr{E}^m_0 (X; E)$ the space of all Whitney fields $S\in \mathscr{E}^m (X; E) $
which have support compactly contained in $X\cap\R^d$. Note that each of the section spaces $\Gamma^m (X; \tilde{E})$
and $\mathscr{E}^m (X; E)$ can be written as the quotient of some function space $\shContFcts^m (U,F\times \R^l)$,
where $l\in \N\cup\{\infty\}$ and $U \subset \closure{\Omega}$ is open.
Therefore, each of those sections spaces  inherits from the corresponding $\shContFcts^m (U,F\times\R^l)$
the structure of a Fr\'echet space. 
As a consequence of this observation, the section spaces $\Gamma^m_0 (X; \tilde{E})$ and $\mathscr{E}^m_0 (X; E)$
become LF-spaces in a natural way. 

The next ingredient we need is a \emph{lagrangian function} that is a function
$L \in \shSmoothFcts_\textup{loc} \big( \Jet^\infty \pi \big)$.
Since $L$ is a local function on the jet bundle, it can be regarded as an element of
$\shSmoothFcts \big(\Jet^k \pi \big)$
for some natural $k$. Let $\order (L)$ be the smallest of such numbers and call it
the \emph{order} of the langragian  function.
The canonical volume form $\omega$ together with the lagrangian $L$ give rise to  the
\emph{lagrangian density} $\lagrangian = L \, \omega$ on the jet bundle $\Jet^\infty \pi$.
Before we can write down the action functional induced by the 
lagrangian density $\lagrangian$ we need to fix some boundary conditions. For now, we will restrict
to \emph{Cauchy boundary conditions} \emph{with compact support}  of some given \emph{order}
$m \in \N \cup\{\infty\}$. These are encoded by Whitney fields
$F \in \mathscr{E}^m (\partial\Omega; E) \subset  \Gamma (\partial\Omega ; \Jet^m E)$
with support being compact and contained in $\partial{\Omega} \cap \R^d$.
More precisely, define the \emph{space of Cauchy boundary data} of order $m$ over the regular domain $\Omega$ by
\begin{equation*}
\begin{split}  
  \mathscr{B}^m_\textup{Cauchy} & (\partial\Omega; E) := 
  \mathscr{E}^m (\partial\Omega; E) \cap \Gamma^\infty_0 (\partial\Omega; \Jet^m E ) = \\
   & =  \big\{ B \in  \Gamma^\infty (\partial\Omega; \Jet^m E) \bigmid
    \exists b\in  \Gamma^\infty (\closure{\Omega};  E):
  \jet^m b|_{\partial\Omega} = B  \:\&\: \supp b \Subset \closure{\Omega} \cap \R^d\big\} \ .
\end{split}
\end{equation*}
Given an element $B\in \mathscr{B}^m_\textup{Cauchy} (\partial\Omega;E)$, we single out the space
$\tvsX_B$ of \emph{allowable sections} of $E$: 
\[
  \tvsX_B = \big\{ s \in \Gamma_0^\infty (\closure{\Omega};E)\bigmid \jet^m s|_{\partial\Omega} = B \big\} \ .
\]
In other words, $\tvsX_B$ consists of all smooth sections $s :\closure{\Omega} \to E$
which fulfill the support condition $\supp s \Subset \closure{\Omega}\cap \R^d$ and the
Cauchy boundary condition $\jet^m s|_{\partial \Omega} = B$.
Observe that by construction  $\tvsX_B$ is an affine space over the vector  space
\[ \tvsV^m = \big\{ v \in \Gamma^\infty_0 (\closure{\Omega};E) \bigmid \jet^m v|_{\partial \Omega} = 0 \big\} \ .\]
That space carries a natural locally convex topology given by the locally convex colimit topology of
the strict inductive system of Fr\'echet spaces
\[
  \tvsV^m_N = \big\{ v \in \tvsV^m \bigmid \supp v \subset \closure{\Omega}\cap \closure{\ball}_N (0, \R^d) \big\}\ ,
  \quad N \in \N \ .
\]  
The affine space $\tvsX_B$ inherits the locally convex topology from $\tvsV^m$
and thus becomes a manifold globally modeled on $\tvsV^m$. The tangent bundle of $\tvsX_B$ then is
canonically isomorphic to the product manifold $\tvsX_B\times \tvsV^m$.

Now we can write down  the \emph{action functional} associated to the lagrangian density $\lagrangian$:
\begin{equation}
  \label{eq:action-functional-local-case}
   \action : \tvsX_B \to \R, \: s \mapsto \int_{\closure{\Omega}\cap \R^d} \big(\jet^\infty s\big)^* \lagrangian
  =\int_{\closure{\Omega}\cap \R^d} \big( L \circ \jet^\infty s \big) \cdot \omega \ .
\end{equation}
% 
Note that even though the domain of integration might be unbounded, the integral is well-defined for
every $s\in \tvsX_B$ since  $L \circ \jet^\infty s $ has compact support contained in
$\closure{\Omega}\cap \R^d$ whenever $s$ has that property.

\begin{proposition}
  Assume that $\Omega\subset \R^d$ is an open subset such that its closure $\closure{\Omega}$
  in $\closedball^d_\infty$ is a submanifold-with-boundary.  Denote by $\omega$ the canonical volume
  element on $\Omega$ induced from $\R^d$.  Assume further that 
  $\pi : E = \closure{\Omega} \times F \to\closure{\Omega}$ a trivial fiber bundle with typical fiber $F$
  being an open and connected subset of some $\R^n$.
  Let $L\in \shSmoothFcts_\textup{loc} \big( \Jet^\infty \pi \big) $ be a lagrangian,
  and  $B$ an element of the space $\mathscr{B}^m_\textup{Cauchy} (\Omega; E)$ of Cauchy boundary 
  data of order $m \geq \ord (L) -1$. 
  Then the action functional $\action : \tvsX_B \to \R$ associated to the lagrangian density
  $\lagrangian = L\omega$ is continuous. % on the space $\tvsX_B$ of allowable sections.
  Moreover, $\action$ is Gateaux differentiable. The corresponding functional derivative
  $ \delta \action : T \tvsX_B = \tvsX_B \times \tvsV^m \to \R$ is linear in $\tvsV^m$, continuous and
  given by
  \begin{equation}
    \label{eq:gateaux-derivative-action-functional-euclidean-space}
      \langle \delta \action (s) ,v \rangle =  \sum_{\inda=1}^n \int_{\closure{\Omega}\cap \R^d}
       v^\inda \cdot \jet^\infty (s)^* \left( \frac{\partial L}{\partial u^\inda} 
      + \sum_{\indI \in \overline{\mathscr{I}}^\bullet, \: |\indI| > 0} (-1)^{|I|}
      \frac{\partial^{|\indI|}}{\partial x^{\indI}}
      \left( \frac{\partial L}{\partial u^\inda_{\indI}} \right)\right) \cdot \omega \ , 
  \end{equation}
     where $(s,v)\in \tvsX_B \times \tvsV^m$ and where $v^\inda$ is the composition $u^\inda \circ v$. 
\end{proposition}
\begin{proof}
 We first show that the functional $\action$ is sequentially continuous with respect to the
 locally convex topology on $\tvsX_B$ which means that for each $s\in \tvsX_B$ and each sequence $(s_k)_{k\in \N}$ in $\tvsX_B$
 converging to $s$  the sequence  $\big( \action(s_k) \big)_{k\in\N}$ converges to $\action (s)$. 
 Since $\tvsV^m$ is an LF space that is the locally convex colimit of a
 countable strict inductive system of Fr\'echet spaces, there exists a positive natural number $N$ such that
 $s_k-S \in \tvsV^m_N$ for all $k\in \N$. Since the support of $s$ is compactly
 contained in $\closure{\Omega} \cap \R^d$, we can assume after possibly increasing $N$ that
 $\supp s \subset \closedball_N (0,\R^d)$. Hence the supports all $s_k$ are
 contained in
 $\closure{\Omega} \cap \closedball_N (0,\R^d)$, and for every $\alpha \in \N^d$ the sequence
 $\left( \frac{\partial^{|\alpha|}s_k}{\partial x^{\alpha}}\right)_{k\in \N}$ converges uniformly on 
 $\closure{\Omega} \cap \closedball_N (0,\R^d)$ to $\frac{\partial^{|\alpha|}s}{\partial x^{\alpha}}$.
 Since the lagrangian function $L$ has finite order,
 the compositions $L \circ \jet^\infty s_k$ and $L \circ \jet^\infty s$  also have compact support contained in
 $\closure{\Omega} \cap \closedball_N (0,\R^d)$, and the sequence
 $\left( L \circ \jet^\infty s_k\right)_{k\in \N} $  converges uniformly on
 $\closure{\Omega} \cap \closedball_N (0,\R^d)$ to $L \circ \jet^\infty s$. Hence the sequence of integrals
 $\int_{\closure{\Omega}\cap \R^d} \big( L \circ \jet^\infty s_k \big) \, \omega$ converges to
 $\action(s) = \int_{\closure{\Omega}\cap \R^d} \big( L \circ \jet^\infty s \big) \, \omega$, and the action  functional
 is sequentially continuous.

 The proof of sequential continuity  can not be extended
 to also show continuity of the action just by replacing sequences  with nets.
 The reason is that a net in an LF space, e.g.\ one labeled by the first uncountable
 ordinal, might not have any subsequences at all.
 Hence, unlike a converging sequences, a net in an LF space need not eventually be
 contained in one of the Fr\'echet spaces of the strict inductive system
 defining the LF structure. This observation makes the main ingredient in 
 the above argument fail for the case of converging nets. 
 One therefore needs another approach to prove continuity of $\action$.
 We will use the observation from
 \ref{sec-fancy-project:lf-structure-defining-seminorms-space-test-function} that
 the locally convex topology of the LF space $\tvsV^m$ is defined by the collection of
 all seminorms 
 \[
   p_{N,\theta} : \: \tvsV^m\to\R_{\geq 0} , \: v \mapsto
   \sup_{1\leq \inda \leq n} \sup_{\indI \in \overline{\mathscr{I}}^\bullet, \, |\indI|\leq N}
   \left\| \theta_I \frac{\partial^{|\indI|} v^\inda}{\partial x^\indI} \right\|_{\closure{\Omega}} \ .
 \]  
 Let $s \in  \tvsX_B$ and choose $N \in \N$ large enough so that
 $\supp s \Subset \closure{\Omega}\cap \ball_N(0,\R^d)$.
 
 
 Next we prove Gateaux differentiability. To this end let
 $s_{\bullet} =(s_t)_{t\in I_\varepsilon}$ be a smooth path in $\tvsX_B$ defined on some open interval $I_\varepsilon$
 of the form  $\openint{-\varepsilon,\varepsilon} $  with $\varepsilon >0$ and which fulfills $s_0=s$.
 Note that the tangent vector $\dot{s}_0$ is an element of the space $\tvsV^m$ and that every
 $v \in \tvsV$ can be obtained as the tangent vector of a smooth path in $\tvsX_B$, e.g.\
 of the path $\R\to \tvsX_B$, $t\mapsto s + t v$.
 Denote by $o$ the order of the langrangian and by $\mathscr{I}$ the index set $\{ 1,\ldots,d\}$.
 Recall Equation \ref{eq:vertical-derivative-jet-map} for the vertical
 derivative of the jet map:
 \begin{equation}
   \label{eq:vertical-derivative-jet-map.sec:euler-lagrange-equations}
   T\jet^\infty (\dot{s}_0) =   \left.\frac{d}{dt} \jet^\infty s_t \right|_{t=0}  =  
   \sum_{\inda=1}^n\sum_{\indI \in \overline{\mathscr{I}}^\bullet}
   \frac{\partial^{|\indI|} \dot{s}^\inda_0}{\partial x^{\indI}} 
   \frac{\partial}{\partial u^\inda_{\indI}} \ .
 \end{equation}
 In this formula, $\overline{\mathscr{I}}^\bullet$ denotes the set of \emph{ordered Roman multi-indices} in
 the set $\mathscr{I} = \{ 1, \ldots , d\}$ that is $\overline{\mathscr{I}}^\bullet$ consists of a
 zero element $\zerO$ and
 all finite sequences of integers of the form
 \[
   \indI = (i_1,\ldots,i_k) \ , \quad \text{where } k \in \N_{>0} \text{ and } 
   1\leq i_1\leq i_2\leq \ldots \leq i_k \leq d \ .
 \]  
 The number $k$ is called the \emph{length} of the ordered Roman multi-index $\indI$.
 The  \emph{length} of the zero element $\zerO$ is defined to be $0$.
 See Section \ref{sec:combinatorial-interlude} for details on Roman multi-indices
 and their combinatorial properties.
 After these preparations we now compute:
 \begin{equation*}
   \begin{split} 
     \left.\frac{d}{dt} \action( s_t) \right|_{t=0}   \, & =
     \int_{\closure{\Omega}\cap \R^d}
    \left. \frac{d}{dt}  L \circ \jet^\infty (s_t)\right|_{t=0}  \, \omega 
      =  \sum_{\inda=1}^n \int_{\closure{\Omega}\cap \R^d}\sum_{\indI \in \overline{\mathscr{I}}^\bullet}
      \frac{\partial^{|\indI|} \dot{s}^\inda_0}{\partial x^{\indI}} 
      \left( \frac{\partial L}{\partial u^\inda_{\indI}} \circ \jet^\infty (s_0)\right) \omega = \\
      & =  \sum_{\inda=1}^n \int_{\closure{\Omega}\cap \R^d}
      \left( \dot{s}^\inda_0 \left( \frac{\partial L}{\partial u^\inda} \circ \jet^\infty (s_0)\right)
      + \sum_{\indI \in \overline{\mathscr{I}}^\bullet, \: |\indI| > 0}
      \frac{\partial^{|\indI|} \dot{s}^\inda_0}{\partial x^{\indI}}
      \left( \frac{\partial L}{\partial u^\inda_{\indI}} \circ \jet^\infty (s_0)\right)\right) \omega \ . 
   \end{split}
 \end{equation*}

 % $\gamma : \openint{-\varepsilon,\varepsilon} \to \tvsX_B$
 % a differentiable path with $\gamma (0) =s$ and derivative $v =\gamma^\prime (0)\in \tvsV$ at $0$. 

\end{proof}


\para
We now want to find the extremal points  of the functional $\action$, if such exist.
To this end we first derive a necessary condition for $s_0\in \tvsX_B$ to be an extremal point of
$\action$. 
