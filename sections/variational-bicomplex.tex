% Copyright 2021 Markus J. Pflaum
% main author: 
%   Markus J. Pflaum 
%
\section{The variational bicomplex}
\label{sec:variational-bicomplex}
\subsection*{The Cartan distribution}
\para 
We start with a smooth fiber bundle $\pi : E \to M$ over a $d$-dimensional manifold $M$.
The typical fiber is denoted $F$ and assumed to have dimension $n$. Consider the \emph{infinite jet bundle}  
$\pi_\infty: \Jet^\infty E \to M$ and recall that $\left( \Jet^\infty E , \shSmoothFcts\right)$ is the pro-manifold defined as 
the limit of the (cofiltered) diagram 
\begin{equation}
  \label{eq:filtered-diagram-finite-jet-bundles}
  \big(E,\shSmoothFcts\big) \overset{\pi_{0,1}}{\longleftarrow} 
  \big(\Jet^1 E,\shSmoothFcts\big) \overset{\pi_{1,2}}{\longleftarrow}
  \ldots \overset{\pi_{k-1,k}}{\longleftarrow} \big(\Jet^k E ,\shSmoothFcts \big) 
  \overset{\pi_{k,k+1}}{\longleftarrow} \ldots \ .
\end{equation}
in the category of commutative locally $\R$-ringed spaces. This means that in the category of topological spaces  
$\Jet^\infty E$ coincides with $\lim\limits_{k\in \N} \Jet^k E $ and that the structure sheaf 
$\shSmoothFcts_{\Jet^\infty E}$ is given by $\colim\limits_{k\in\N} \pi_{k,\infty}^* \shSmoothFcts_{\Jet^k E}$, where
the $\pi_{k,\infty} : \Jet^\infty E \to \Jet^k E$ are the natural maps from the (topological) limit to the objects of the diagram.
The projection $\pi_\infty : \Jet^\infty E \to M$ is uniquely determined by the property that $\pi_\infty = \pi_k \circ \pi_{k,\infty}$ for all $k\in \N$,
where $\pi_k$ is the canonical projections of the finite jet bundles $\Jet^k E$. Note that  
the family of canonical projections $\pi_k : \Jet^k E \to M$ is compatible with the diagram 
\Cref{eq:filtered-diagram-finite-jet-bundles} in the sense that
$\pi_l = \pi_k \circ \pi_{k,l}$ for all $k\leq l$. 
Next recall that $\shSmoothFcts_{\textup{loc},\Jet^\infty E}$, or just $\shSmoothFcts_\textup{loc}$ when no confusion can arise, 
stands for the presheaf of \emph{local functions} on the infinite jet bundle. Its space of sections over some open $ U \subset  \Jet^\infty E$
consists of all continuous maps $f:U \to \R$ for which there exists a $k\in \N$, an open  $U_k \subset \Jet^k E$
and a smooth function $f_k :U_k \to \R$ such that $U \subset \pi_{k,\infty}^{-1} (U_k)$ and $f =  f_k \circ \pi_{k,\infty}|_U$.

The diagram \Cref{eq:filtered-diagram-finite-jet-bundles} of jet bundles of finite order induces  
another filtered diagram by taking tangent bundles and tangent maps:
\begin{equation}
  \label{eq:filtered-diagram-tangent-bundles-finite-jet-bundles}
  \big(TE,\shSmoothFcts\big) \overset{T\pi_{0,1}}{\longleftarrow} 
  \big(T\Jet^1 E,\shSmoothFcts\big) \overset{T\pi_{1,2}}{\longleftarrow}
  % \big(T\Jet^2 E ,\shSmoothFcts\big) \overset{T\pi_{2,3}}{\longleftarrow} 
  \ldots \overset{T\pi_{k-1,k}}{\longleftarrow} \big(T\Jet^k E ,\shSmoothFcts \big) 
  \overset{T\pi_{k,k+1}}{\longleftarrow} \ldots \ .
\end{equation}
The resulting limit in the category of commutative locally $\R$-ringed spaces is called the \emph{tangent bundle} of the 
pro-manifold $\left(\Jet^\infty E,\shSmoothFcts\right)$ and is denoted $\left(T\Jet^\infty E,\shSmoothFcts\right)$.
One writes $T\pi_{k,\infty}: T\Jet^\infty E \to T\Jet^k E$ for the natural maps of the limit and obtains
the tangent map $T\pi_\infty : T\Jet^\infty E \to TM$ uniquely determined by the property that
$T\pi_\infty = T\pi_k \circ T\pi_{k,\infty}$ for all $k\in \N$. 

As the last prerequisite we need the concept of Roman multi-indices 
and their combinatorial properties from  Section \ref{sec:combinatorial-interlude}.
As there we denote by $\overline{\mathscr{I}}^\bullet$ the set of \emph{ordered Roman multi-indices} in
an ordered index set $\mathscr{I}$. In our situation, the index set is $\mathscr{I} = \{ 1, \ldots , d\}$
which entails that $\overline{\mathscr{I}}^\bullet$ consists of a
zero element $\zerO$ and all finite sequences of integers of the form
 \[
   \indI = (i_1,\ldots,i_k) \ , \quad \text{where } k \in \N_{>0} \text{ and } 
   1\leq i_1\leq i_2\leq \ldots \leq i_k \leq d \ .
 \]  
 The number $k$ is called the \emph{length} of the ordered Roman multi-index $\indI$
 and is denoted by $|\indI|$.
 The  \emph{length} of the zero element $\zerO$ is defined to be $0$.
 
Now we have all the tools to define the main object of this section, the Cartan distribution. 

\para
Let $p$ be a point of the base manifold $M$. Choose an open contractible neighborhood $U\subset M$ of $p$ over which 
there exists a coordinate system $x: U \to \R^d$. Denote by $\mathscr{E}_U$ the space of smooth sections of the bundle $\pi:E \to M$ over $U$
and by $I_\varepsilon$ for $\varepsilon >0$ the open interval $\openint{-\varepsilon,\varepsilon}$ around $0$. 
By Borel's theorem, the jet map $\jet^\infty_q :\mathscr{E}_U \to \Jet^\infty_q E$ is surjective for every $q\in U$. 
Call a smooth path 
\[ \gamma = (\sigma,\mu) :I_\varepsilon \to \mathscr{E}_U \times U, \: t \mapsto (\sigma_t,\mu_t) \] 
with $\mu_0=p$ \emph{vertical} \emph{over} $p$ if $\mu$ is a constant path and \emph{horizontal} \emph{over} $p$
if $\sigma$ is a constant path. Smoothness of $\sigma$ hereby means that 
$\sigma^\vee :I_\varepsilon \times U \to E$, $(t,q) \mapsto \sigma_t(q)$ 
is smooth. The composition 
\[\jet^\infty \comp \gamma  :I_\varepsilon \to \Jet^\infty E, \: t \mapsto \jet^\infty_{\mu_t} (\sigma_t)\]
then is a smooth path in the jet bundle and the derivative 
\[
  \left( \jet^\infty \comp \gamma\right)^\prime (0) = \frac{d}{dt} \left( \jet^\infty \comp \gamma\right) (t)\Big|_{t=0} =
  \frac{d}{dt} \left( \jet^\infty_{\mu_t}(\sigma_t)\right)\Big|_{t=0}
\]  
an element of the tangent space $T_{\theta}\Jet^\infty E$ over the footpoint $\theta= \jet^\infty_p (\sigma_0)$. 
If $\gamma$ is vertical, the path $\pi_\infty \comp \jet^\infty \comp \gamma$ is constant with value $p$ which implies that the
tangent vector $\left( \jet^\infty \comp \gamma\right)^\prime (0)$ has to be  an element  
of the vertical bundle $\Vertical \pi_\infty = \ker T \pi_\infty \subset T\Jet^\infty E$. Let us show that every vertical tangent vector 
with footpoint $\theta$ can be obtained that way. So assume that $v \in \Vertical_{\theta} \pi_\infty$ is represented by a smooth path 
$ \varrho :\openint{-\varepsilon,\varepsilon} \to \Jet^\infty E$ such that $\pi_\infty (\varrho(t))=p$ for all $t$.  
After possibly shrinking $U$ and $\varepsilon$ one can assume that there exists a fibered chart $(x,u) : \widetilde{U} \to \R^d \times \R^n$
over some open $\widetilde{U} \subset E$ such that $\pi (\widetilde{U})=U$, $(x,u)$ is trivialising in the sense that its image coincides 
with the cartesian product of $x(U)$ and an open $V \subset \R^n$ and such that $\pi_{0,\infty} (\varrho (t)) \in \widetilde{U}$ for all $t$.      
One obtains a family of smooth real valued functions 
$u^\inda \comp \varrho,  u^\inda_i \comp \varrho , \ldots , u^\inda_\indI \comp \varrho  , \ldots $, where 
the index $\inda$ runs through $\{1 , \ldots ,  n\}$, the index $i$ through $\mathscr{I}= \{ 1,\ldots , d\}$,  and  $\indI$ through all ordered Roman multi-indices 
in $\mathscr{I}$ of order $\geq 2$.  By Borel's Theorem with parameters \cite[15.4]{KriMicCSGA}, there exists a smooth function 
$s=(s^1,\ldots,s^n) : I_\varepsilon  \times U \to V$
such that
\[ \frac{\partial^{|\indI|}s^\inda}{\partial x^\indI}(t,p) = u^\inda_\indI \comp \varrho(t) \quad\text{for all } t\in I_\varepsilon, \: \inda \in \{1 , \ldots ,  n\}
  \text{ and } \indI \in \overline{\mathscr{I}}^\bullet  \ .
\]
Let $\sigma : I_\varepsilon \to \mathscr{E}_U$ be the smooth path of sections $t \mapsto s(t,-)$,
$\mu: I_\varepsilon  \to M$ the constant path at $p$ and let $\gamma = (\sigma,\mu)$. Then
$\gamma$ is vertical and, by construction, 
\[ \left( \jet^\infty \comp \gamma\right)^\prime (0) =  \varrho^\prime (0) = v \ . \]  
This shows the claim. 

Next assume to be given a jet $\theta \in \Jet^\infty_p E$. Define the \emph{horizontal space} at that jet by
\[ 
   \mathsf{C}_{\theta} \Jet^\infty E = 
   \big\{  \left( \jet^\infty \comp \gamma\right)^\prime (0) \in T_{\theta} \Jet^\infty E \bigmid 
   \gamma = (\sigma,\mu) \text{ is horizontal over } p \text{ and } \jet^\infty \sigma_0 = \theta \big\} \ .
\]
One calls $\mathsf{C}\Jet^\infty E = \bigcup_{\theta \in \Jet^\infty E}\mathsf{C}_{\theta} \Jet^\infty E$
the \emph{Cartan distribution} on the jet bundle $\Jet^\infty E$. In the following we will study its properties
and will show  that it is an involutive distribution on the jet bundle which is complementary to the vertical bundle.  

\begin{lemma}
   Let $\theta \in \Jet^\infty_p E$ be a jet and choose a trivialising fibered chart $(x,u): \widetilde{U} \to \R^d \times \R^n$  
   around an open  neighborhood of $e = \pi_{1,\infty} (\theta)$. 
   Let $\mu: I_\varepsilon \to U $ be a smooth path with $\mu_0=p$,
   $\sigma : I_\varepsilon \to \mathscr{E}_U$ a smooth path of sections and finally $s : U \to E$ a smooth section
   such that that the images of all $\sigma_t$ and $s$ are in $\widetilde{U}$ 
   and such that $\jet^\infty_p (\sigma_0) = \jet^\infty_p(s) = \theta$. Denote by 
   $\mu^i$ the composition $x^i \comp \mu$ and by $\sigma^\inda$ and $s^\inda$ the compositions $u^\inda\comp \sigma$
   and $u^\inda\comp s$, respectively. Then the
   tangent vector of the vertical path $(\sigma,p)$ is given by
   \begin{equation}
   \label{eq:vertical-derivative-jet-map}   
     \left( \jet^\infty_{p} \sigma_t \right)^\prime (0) =  
     \sum_{\inda =1}^n\sum_\indI   \frac{\partial^{|\indI|} (\sigma^\inda)^\prime (0)}{\partial x^\indI} (p)  
     \frac{\partial}{\partial u^\inda_\indI}
   \end{equation}
   and the tangent vector of the horizontal path $(s,\mu)$ by
   \begin{equation}
   \label{eq:horizontal-derivative-jet-map}   
     \left( \jet^\infty_{\mu_t} s \right)^\prime (0) = 
     \sum_{i=1}^d (\mu^i)^\prime (0) \left( \frac{\partial}{\partial x^i}
     + \sum_{\inda =1}^n\sum_\indI   \frac{\partial^{|I|+1} s^\inda}{\partial x^i \partial x^\indI} (p)  
     \frac{\partial}{\partial u^\inda_\indI}\right) \ .
   \end{equation}
   In these formulas, $\indI$ runs through all ordered Roman multi-indices in the index set $\mathscr{I} = \{1,\ldots , d\} $.
\end{lemma}

\begin{proof}
  Let $\gamma = (\sigma,\mu)$. Then in the selected fibered chart 
  \[  x^i \comp\jet^\infty \comp \gamma  = \mu^i   \quad \text{and} \quad 
  \left( u^\inda_\indI \comp \jet^\infty \comp \gamma \right)(t) =  
  \frac{\partial^{|\indI|} \sigma^\inda_t}{\partial x^i} \left( \mu_t \right) \ , \]
  from which the claim follows by specialization to $\mu_t =p$ respectively $\sigma_t=s$ and the chain rule. 
\end{proof}

\begin{lemma}
  Let $\theta \in \Jet^\infty_p E$ be a jet and $s_1,s_2 : U \to E$ two smooth sections 
  such that \[ \theta = \jet^\infty_p (s_1) = \jet^\infty_p(s_2) \ . \]
%  Let $\sigma,\tau :I_\varepsilon \to  \mathscr{E}_U$ be the constant paths with value $\sigma_0$ and $tau_0$, respectively. 
  Then  for every smooth path $\mu :I_\varepsilon \to M$ with $\mu_0=p$ the equality
  \[
     \left( \jet^\infty_{\mu_t} s_1 \right)^\prime (0) = \left( \jet^\infty_{\mu_t} s_2 \right)^\prime (0)
  \]  
  holds true, where ${}^\prime$ denotes the derivative with respect to the parameter $t$.  Hence,
  \begin{equation}
  \label{eq:independence-cartan-horizontal-space-choice-section}
    \begin{split}
       \mathsf{C}_{\theta} \Jet^\infty E & = \big\{  \left( \jet^\infty_{\mu_t} s_1 \right)^\prime (0) \in T_{\theta} \Jet^\infty E \bigmid 
       \mu \in \shSmoothFcts \left(I_\varepsilon, M\right) \:\&\: \mu_0 = p  \big\}  \\
       & =  \big\{  \left( \jet^\infty_{\mu_t} s_2 \right)^\prime (0) \in T_{\theta} \Jet^\infty E \bigmid 
       \mu \in \shSmoothFcts \left(I_\varepsilon, M\right)\:\&\: \mu_0 = p \big\} \ .
    \end{split}
  \end{equation}
\end{lemma}
\begin{remark}
  The lemma implies  in particular that the horizontal space $ \mathsf{C}_{\theta} \Jet^\infty E$ does not depend on the choice of a 
  section representing $\theta$.
\end{remark}
\begin{proof}
  After possibly shrinking $U$ and $\varepsilon$ choose a trivialising fibered chart $(x,u): \widetilde{U} \to \R^d \times \R^n$ around an open 
  neighborhood of $s_1(p)=s_2(p)$ as
  above. Moreover, we can assume after possible shrinking $U$ and $\varepsilon$ again that both $s_1(U)$ and $s_2(U)$ are 
  contained in $\widetilde{U}$.
  Then compute 
  \begin{equation*}
    \begin{split}
    \left( \jet^\infty_{\mu_t} s_1 \right)^\prime (0) & = \sum_{i=1}^d (\mu^i)^\prime (0) \left( \frac{\partial}{\partial x^i}
    + \sum_{\inda =1}^n\sum_\indI   \frac{\partial^{|I|+1} s_1^\inda}{\partial x^i \partial x^\indI} (p)  
    \frac{\partial}{\partial u^\inda_\indI}\right) \\
     & = \sum_{i=1}^d (\mu^i)^\prime (0) \left( \frac{\partial}{\partial x^i}
    + \sum_{\inda =1}^n\sum_\indI   \frac{\partial^{|I|+1} s_2^\inda}{\partial x^i \partial x^\indI} (p)  
    \frac{\partial}{\partial u^\inda_\indI}\right) =  \left( \jet^\infty_{\mu_t} s_2 \right)^\prime (0) \ ,
   \end{split}
  \end{equation*}   
  where $\mu^i = x^i\comp \mu$, $s_j^\inda = u^\inda\comp s_j$ for $j=1,2$, and where $\indI$ runs through the Roman multi-indices 
  in the index set $\mathscr{I}$. This proves the claim.
\end{proof}

\begin{lemma}
\label{thm:projection-horizontal-tangent-vectors-jet-bundle-base}
  For every section $s \in \mathscr{E}_U$ the map 
  \[
    T_pM \to T_pM , \: \mu^\prime (0) \mapsto \left(\pi_\infty\comp \jet^\infty_{\mu_t} (s)\right)^\prime (0)
  \] 
  is  the identity map, where tangent vectors at $p$ are represented as derivatives at the base point $0$ of smooth paths $\mu :I_\varepsilon \to M$
  based at $p$ that is which fulfill $\mu_0 =p$. 
\end{lemma}

\begin{proof}
  This is trivial, since $\pi_\infty\comp \jet^\infty_{\mu_t} (s) = \mu_t$ for all $t\in I_\varepsilon$.
\end{proof}

Despite the lemma being trivial, some of its consequences are not. 

\begin{proposition}
  For every smooth fiber bundle $\pi:E \to M$ the Cartan distribution is a smooth involutive vector subbundle of the tangent bundle on 
  $\Jet^\infty E$. The Cartan distribution has   fiber dimension $d = \dim M$. 
  In a fibered chart $(x,u): \widetilde{U} \to \R^d\times \R^n$, a local frame for the Cartan distribution 
  is given by the family of vector fields 
  \[
     D_i = \frac{\partial}{\partial x^i}
     + \sum_{\inda =1}^n\sum_\indI u^\inda_{\indI \indi} \frac{\partial}{\partial u^\inda_\indI} \ , \quad i =1,\ldots , d \ ,
  \]  
  where the right summation is taken over all Roman multi-indices $\indI$ in the index set $\mathscr{I} = \{1,\ldots , d\} $.    
\end{proposition}

\begin{proof}
  By \Cref{thm:projection-horizontal-tangent-vectors-jet-bundle-base} it is clear that 
  $\dim \mathsf{C}_{\theta} \Jet^\infty E = d$ for every $\theta\in \Jet^\infty E$.
  
\end{proof}


% For the moment assume that $\pi$ is a vector bundle which in particular means that $F$ is a finite dimensional vector space. 
 
