\section{The Gra{\ss}mann manifold of a Banach space}
\para
Throughout this section we denote by $\banachE$ a Banach space over
the field $\fldK = \R$ or $=\C$. The main object of study of this section then
is the space $\Grass\banachE$ of closed $\fldK$-linear
subspaces of $\banachE$. It is called the
\emph{Gra{\ss}mann manifold} or \emph{Gra{\ss}mannian} of $\banachE$.
Let us equip $\Grass\banachE$ with a natural topology by defining a metric on it.
For elements $\banachV,\banachW \in \Grass\banachE$, the \emph{gap distance}
$d_\textup{gap} (\banachV,\banachW)$
between $\banachV$ and $\banachW$ is defined as the Haudorff distance of their respective
closed unit balls $\closedball_\banachV$ and $\closedball_\banachW$. More precisely that means 
\begin{equation}
  \label{eq:definition-gap-distance}
  d_\textup{gap} (\banachV,\banachW) = d_\textup{H}  (\closedball_\banachV,\closedball_\banachW) =\max
    \left\{
      \sup\limits_{v\in \closedball_\banachV} d(v,\closedball_\banachW),
      \sup\limits_{w\in \closedball_\banachW} d(w,\closedball_\banachV)
    \right\}  \ ,
\end{equation}
where, as usual, $d (v,B) = \inf\limits_{w\in B} \| v -w\|$ denotes the \emph{distance} 
between a point $v\in \banachE$ and a closed $B \subset \banachE$.
\begin{lemma}\label{thm:properties-directed-gap}
  Let
  \[
    \vec{d}_\textup{gap} (\banachV,\banachW) =
    \sup\limits_{v\in \closedball_\banachV} d (v,\closedball_\banachW)
  \]
  denote the \emph{directed} or \emph{one-sided gap}
  between $\banachV,\banachW \in \Grass\banachE$.
  Then the following holds true.
  \begin{romanlist}
  \item\label{ite:directed-gap-zero-vector-space}
    $\vec{d}_\textup{gap} (0 ,\banachV) = \vec{d}_\textup{gap} ( \banachV , 0) =1$
    whenever $\banachV  \neq 0$.
  \item\label{ite:directed-gap-zero}
    $\vec{d}_\textup{gap} (\banachV,\banachW) = 0$ if and only if $\banachV \subset \banachW$.
  \item\label{ite:directed-gap-triangle-inequality}
    For all $x \in \banachE$,
    \[ d (x,\closedball_\banachV) \leq   d (x,\closedball_\banachW) + \vec{d}_\textup{gap} (\banachW,\banachV) \ . \]
  \end{romanlist}
\end{lemma}  
\begin{proof}
  \ref{ite:directed-gap-zero-vector-space} follows immediately by definition and
  \ref{ite:directed-gap-zero} holds true since $d (v,\closedball_\banachW)=0$
  if and only if $v \in \closedball_\banachW$. It remains to show
  \ref{ite:directed-gap-triangle-inequality}. To this end let $x\in \banachE$,
  $v\in \closedball_\banachV$ and $w\in \closedball_\banachW$. Then, by the triangle inequality
  for the distance $d :\banachE \times\banachE \to \R$, $(x,y)\mapsto \| x-y\|$,
  \[
     d (x,v) \leq  d (x,w) + d (w,v) \ .
  \]
  This entails, by taking the infimum with respect to  $v\in \closedball_\banachV$,
  \[
    d (x,\closedball_\banachV) \leq  d (x,w) + d (w,\closedball_\banachV)
    \leq  d (x,w) + \vec{d}_\textup{gap} (\banachW,\banachV) \ .
  \]
  Since $w\in \closedball_\banachW$ was arbitrary, \ref{ite:directed-gap-triangle-inequality}
  follows.
\end{proof}


\begin{proposition}
  The gap distance on the Gra{\ss}mannian $\Grass\banachE$ of a Banach space is
  a metric.
\end{proposition}

\begin{proof}
  By definition, the gap distance is symmetric. 
  By \ref{ite:directed-gap-zero} of Lemma \ref{thm:properties-directed-gap}, one has
  $d_\textup{gap} (\banachV,\banachW) = 0$ if and only if $\banachV = \banachW$.
  It remains to show the triangle inequality. 
  Let $\banachV,\banachW,\banachX\in \Grass\hilbertH$ and use \ref{ite:directed-gap-triangle-inequality}
  in  the preceding lemma to verify
  \begin{align}
    \nonumber
    \vec{d}_\textup{gap} (\banachX,\banachV) = \sup_{x\in \closedball_\banachX}
    d (x,\closedball_\banachV) \leq
    \sup_{x\in \closedball_\banachX} d (x,\closedball_\banachW) + \vec{d}_\textup{gap} (\banachW,\banachV)
    \leq \vec{d}_\textup{gap}(\banachX,\banachW) + \vec{d}_\textup{gap} (\banachW,\banachV) \ , \\
    \nonumber
    \vec{d}_\textup{gap} (\banachV,\banachX) = \sup_{v\in \closedball_\banachV}
    d (v,\closedball_\banachX) \leq
    \sup_{v\in \closedball_\banachV} d (v,\closedball_\banachW) + \vec{d}_\textup{gap} (\banachW,\banachX)
    \leq \vec{d}_\textup{gap}(\banachV,\banachW) + \vec{d}_\textup{gap} (\banachW,\banachX) \ .
  \end{align}
  This entails the triangle inequality for $d_\textup{gap}$.
\end{proof}

\para
Recall that to every closed linear subspace $\vectorspV \subset \hilbertH$
of a Hilbert space $\hilbertH$ there exists a unique orthogonal projection
$P_\vectorspV :\hilbertH \to \hilbertH$ whose image is $\vectorspV$.
The kernel of the projection $P_\vectorspV$ coincides with the orthogonal
complement $\vectorspV^\perp$. One thus obtains a canonical embedding
of $\Grass\hilbertH\hookrightarrow \blinOps(\hilbertH)$. The restriction of
the operator norm distance to $\Grass\hilbertH$ endows $\Grass\hilbertH$
with another metric which we denote by $\delta$. 

\begin{proposition}[{{\cite[Sec.~34]{AkhGlaTLOHS}}}]
  For every Hilbert space $\hilbertH$ the metric
  \[
    \delta: \Grass\hilbertH\times \Grass\hilbertH \to \R, \: (\banachV ,\banachW)
    \mapsto \| P_\banachV -P_\banachW\|
  \]
  coincides with the gap metric $d_\textup{gap}: \Grass\hilbertH\times \Grass\hilbertH \to \R$. Moreoever,
  for all $\banachV,\banachW \in \Grass\hilbertH$,
  \begin{romanlist}
  \item\label{ite:upper-bound-gap}
    $ d_\textup{gap} (\banachV,\banachW) \leq 1$, 
  \item\label{ite:representation-directed-gap-operatornorm-projections}
    $\vec{d}_\textup{gap}(\banachV ,\banachW) = \| (I-P_\banachW)P_\banachV \|$, and
  \item\label{ite:representation-gap-maxium-operatornorm-projections}
    $d_\textup{gap} (\banachV,\banachW) = 
    \max \big\{ \| (I-P_\banachW)P_\banachV \| , \| (I-P_\banachV)P_\banachW \|  \big\} $ .    
  \end{romanlist}
  
\end{proposition}

\begin{proof}
  First note that 
  \[
    \| (I-P_\banachW)P_\banachV \| = \sup_{v\in \closedball_\banachV} \| v -P_\banachW v\|
    = \vec{d}_\textup{gap}(\banachV ,\banachW) 
  \]
  since $d(v, \banachW) = \| v -P_\banachW v\|$ for all $v\in \closedball_\banachV$ by
  the orthogonal decomposition theorem, \ref{thm:orthogonal-decomposition-theorem}.
  This proves \ref{ite:representation-directed-gap-operatornorm-projections} and
  \ref{ite:representation-gap-maxium-operatornorm-projections}.
  Next observe that
  \[
     P_\banachV - P_\banachW = P_\banachV(I - P_\banachW) - (I - P_\banachV) P_\banachW \ .
  \]
  By orthogonality of the images of $P_\banachV(I - P_\banachW)$ and $(I - P_\banachV) P_\banachW$
  this implies for all $x\in \hilbertH$
  \begin{equation}
    \label{eq:norm-action-difference-projections-vector}
    \begin{split}
    \| (P_\banachV - P_\banachW) x \|^2 & =
    \| P_\banachV(I - P_\banachW) x\|^2 + \| (I - P_\banachV) P_\banachW x\|^2 \leq \\
    & \leq \| (I - P_\banachW) x\|^2 + \| P_\banachW x\|^2 = \| x\|^2 \ ,   
    \end{split}
  \end{equation}
  hence
   \begin{equation}
    \label{eq:estimate-delta-distance}
    \delta (\banachV,\banachW)  = \| P_\banachV - P_\banachW\| \leq 1 \ .
  \end{equation}
  One also obtains 
  \begin{equation}
    \label{eq:delta-distance-square-root}
    \delta (\banachV,\banachW) = \sup_{x\in \closedball_\hilbertH} \| (P_\banachV - P_\banachW)x\|
     = \sup_{x\in \closedball_\hilbertH} \sqrt{\| P_\banachV(I - P_\banachW) x\|^2 + \| (I - P_\banachV) P_\banachW x\|^2}.
  \end{equation}
  By restricting $x$ to the closed ball of $\banachW$ this formula entails
  \[
    \delta (\banachV,\banachW)  \geq \sup_{x\in \closedball_\banachW} \| (I - P_\banachV) P_\banachW x\|
    = \sup_{x\in \closedball_\banachW} \| (I - P_\banachV) x\|
    = \vec{d}_\textup{gap}(\banachV ,\banachW) \ .
  \]
  By switching $\banachV$ and $\banachW$ in \eqref{eq:estimate-delta-distance} one gets
  \[
    \delta (\banachV,\banachW)  \geq \sup_{x\in \closedball_\banachV} \| (I - P_\banachW) P_\banachV x\|
    = \sup_{x\in \closedball_\banachV} \| (I - P_\banachW) x\|
    = \vec{d}_\textup{gap}(\banachW ,\banachV) \ .
  \]  
  Consequently,
   \begin{equation}
    \label{eq:estimate-delta-distance-upper-bound-gap}
    \delta (\banachV,\banachW) \geq d_\textup{gap}(\banachV ,\banachW) \ .
  \end{equation}
  Let us show that
  \begin{equation}
    \label{eq:estimate-delta-distance-lower-bound-gap}\delta (\banachV,\banachW) \leq
    d_\textup{gap}(\banachV ,\banachW) \ .
  \end{equation}  
  To this end observe that  for all $x \in \closedball_\hilbertH$
  by \ref{ite:representation-directed-gap-operatornorm-projections}  and $ P_\banachW^2 = P_\banachW$
  \begin{equation}
    \label{eq:estimate-norm-representation-directed-gap}
    \| (I - P_\banachV) P_\banachW x\| \leq \vec{d}_\textup{gap}(\banachW ,\banachV) \cdot  \| P_\banachW x\| \ .
  \end{equation}
  Moreover,
  \begin{equation*}
    \begin{split}
      \|  P_\banachV (I - P_\banachW) x\|^2 & = \inprod{P_\banachV (I - P_\banachW) x , P_\banachV (I - P_\banachW) x } =
      \inprod{P_\banachV^2 (I - P_\banachW) x , (I - P_\banachW)^2 x } = \\
      & = \inprod{ (I - P_\banachW) P_\banachV^2 (I - P_\banachW) x , (I - P_\banachW) x } \leq \\
      & \leq \vec{d}_\textup{gap}(\banachV ,\banachW) \, \| P_\banachV (I - P_\banachW) x \|  \, \| (I - P_\banachW) x\| \ , 
    \end{split}
  \end{equation*}
  and therefore
  \begin{equation}
    \label{eq:estimate-permuted-norm-representation-directed-gap}
      \|  P_\banachV (I - P_\banachW) x\| \leq \vec{d}_\textup{gap}(\banachV ,\banachW)  \, \| (I - P_\banachW) x\| \ . 
  \end{equation}
  Inserting this estimate  and \eqref{eq:estimate-norm-representation-directed-gap}
  into the squared right side of \eqref{eq:delta-distance-square-root} then gives 
  \begin{equation*}
    \begin{split}
      \| P_\banachV(I - P_\banachW) x\|^2 & + \| (I - P_\banachV) P_\banachW x\|^2 \leq
      \vec{d}_\textup{gap}^{\: 2} (\banachW ,\banachV) \cdot  \| P_\banachW x\|^2 + 
      \vec{d}_\textup{gap}^{\: 2} (\banachV ,\banachW)  \, \| (I - P_\banachW) x\|^2 \leq \\
      & \leq d_\textup{gap}^2 (\banachW ,\banachV) \cdot \left(  \| P_\banachW x\|^2 + \| (I - P_\banachW) x\|^2 \right)
      = d_\textup{gap}^2 \, \| x\|^2 \ .
    \end{split}
  \end{equation*}
  Comparing with the left side of \eqref{eq:delta-distance-square-root} shows
  \eqref{eq:estimate-delta-distance-lower-bound-gap}, and the equality of $\delta$ and $d_\textup{gap}$ follows.
  By \eqref{eq:estimate-delta-distance} the latter also yields \ref{ite:upper-bound-gap}.
\end{proof}

\begin{remark}
  We will use the symbols $d_\textup{gap}$ and $\delta$ interchangeably to denote the gap metric
  on the Gra{\ss}mannian of a Banach space .
\end{remark}

\begin{theorem}
  Eqipped with the gap metric the Gra{\ss}mann manifold of a Banach space is a complete metric space. 
\end{theorem}

\begin{proof}
  We present the proof for the underlying Banach space being a  Hilbert space $\hilbertH$.
  Then the claim followsimmediately from the fact that  $\blinOps(\hilbertH)$ is complete and that the limit of
  a Cauchy sequence of orthogonal projections $(P_n)_{n\in \N} \subset \blinOps(\hilbertH)$
  is again an orthogonal projection. 
  The general case is more tricky. 
\end{proof}
