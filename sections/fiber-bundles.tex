% Copyright 2017 Markus J. Pflaum
% main author: 
%   Markus J. Pflaum 
%
\section{Fiber bundles}

\subsection*{Fibered manifolds and fibered charts}
\begin{definition}
  By a \emph{locpro-fibered manifold} we understand a smooth surjective submersion
  $\pi: E \to M$ from a locpro-manifold $E$ onto a manifold $M$.
  If $E$ is a finite dimensional manifold, one calls a surjective submersion 
  $\pi: E \to M$ just a \emph{fibered manifold}. 
  One usually calls $\pi$ the \emph{projection}, $E$ the \emph{total space} and $M$ the 
  \emph{base} of the (pro-)fibered manifold.  A (locpro-) fibered manifold
  is often denoted as a triple $(E,\pi,M)$. 

  A \emph{morphism of \textup{(}locpro-\textup{)} fibered manifolds} $\pi_1 : E_1 \to M_1$ and
  $\pi_2 : E_2 \to M_2$ consists of a pair
  $(\varphi,f)$ of smooth maps $\varphi : E_1 \to E_2$ and 
  $f : M _1\to M_2$ such that the diagram
  \[
  \begin{tikzcd}
   E_1 \arrow[r,"\pi_1"]  \arrow[d,"\varphi",swap] & M_1 \arrow[d,"f"]\\
   E_2 \arrow[r,"\pi_2",swap] & M_2
  \end{tikzcd}
  \]
  commutes. One sometimes also says in this situation that $\varphi$ is a 
  \emph{morphism of \textup{(}locpro-\textup{)} fibered manifolds over the map} $f: M_1\to M_2$. 
  We in particular make use of this language when the base manifolds $M_1$ and 
  $M_2$ coincide and $f$ is the identity map. We then just say that 
  $\varphi$ is a  \emph{morphism of \textup{(}locpro-\textup{)} fibered manifolds}. 
\end{definition}

\para 
  Obviously, the identity map $\id_E$ on the total space of a (locpro-) fibered
  manifold $(E,\pi,M)$ is a morphism.  Moreover, the composition 
  \[ (\varphi_2,f_2) \circ ( \varphi_1 , f_1):= (\varphi_2 \circ \varphi_1 , f_2 \circ f_1)\] 
  of morphisms 
  \[ (\varphi_1,f_1) : (E_1,\pi_1,M_1) \to (E_2,\pi_2,M_2)\quad\text{and} 
  \quad (\varphi_2,f_2) : (E_2,\pi_2,M_2) \to (E_3,\pi_3,M_3)\] 
  is a morphism of (pro-)fibered
  manifolds from $(E_1,\pi_1,M_1)$ to $(E_2,\pi_2,M_2)$, and $\id_E$ acts as identity morphism. 
  One concludes that (locpro-)fibered manifolds and their morphisms  form a  category. 
 


\begin{definition}
  Let $(E,\pi,M)$ be a fibered manifold. By a \emph{fibered chart} of $(E,\pi,M)$
  or a \emph{chart adapted to} $\pi :E \to M$  one understands a 
  chart $(U, \psi) : $.
\end{definition}


\begin{proposition}
  Given  a locpro-fibered manifold $(E,\pi,M)$, the fiber $F_p := \pi^{-1} (p)$ over an element $p\in M$ 
  is a locpro-manifold.   
\end{proposition}

\begin{proof}
  In the case where $E$ is finite dimensional the claim is an immediate consequence of the submersion theorem
  So assume that $E$ is infinite dimensional.
  Since the claim is local,  we can assume that 
  there exists a smooth projective representation $(E_i,\eta_{ij}, \eta_i)_{i,j\in \N,\, i\leq j}$ of $E$. 
  Since $M$ is finite dimensional and using again that the claim is local we can assume that the 
  the smooth map $\pi :E \to M$ factors in a neighborhood of $F_p$ 
  through some smooth map $\pi_i : E_i \to M$ that means that $\pi = \pi_i \circ \eta_i$.  
  Since $\pi$ is a smooth surjective submersion, $\pi_i$ is so, too. Therefore, $F_i := \pi_i^{-1} (p)$ 
  is a submanifold of $E_i$ by the submersion theorem. 
  Now put $\pi_j := \pi_i \circ \eta_{ij}$ for all $j>i$. As a composition of surjective submersions 
  each such $\pi_j$ is a surjective submersion as well. Hence for every $j > i$ the preimage
  $F_j := \pi_j^{-1} (p) = \eta_{ij}^{-1} (F_i)$ is a submanifold of $E_j$.
  Since $\pi = \pi_i \circ \eta_i = \pi_i \circ \eta_{ij}\circ \eta_j = \pi_j \circ \eta_j$, 
  the fiber $F_p$ coincides with $\eta_j^{-1} (F_j)$ for each $j\geq i$. 
  Hence we obtain a smooth projective representation 
  $(F_j, \varphi_{jk},\varphi_j)_{j,k \in \N \, j\leq k}$ of $F_p$, when defining $\varphi_{jk}$  as the restriction 
  $\eta_{jk}|_{F_k}$  and $\varphi_j$ as the restriction $\eta_j|_{F_p}$.
  So $F_p$ is a pro-manifold and the claim is proved.  
\end{proof}

\begin{proposition}
  A locpro-fibered manifold has local smooth sections that is for every locpro-fibered manifold $(E,\pi,M)$ 
  and every point $p\in M$ there exists as smooth map $s:U \to E$ defined on an open neighborhood $U$ of 
  $p$ in $M$ such that $\pi \circ s = \id_U$.
\end{proposition}