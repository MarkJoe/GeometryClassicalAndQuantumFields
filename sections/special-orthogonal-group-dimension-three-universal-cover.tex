% Copyright 2021 Markus J. Pflaum
% main author: 
%   Markus J. Pflaum 
%
\section{The Lie group $\LieSO (3)$ and its universal cover $\LieSU (2)$}
\para 
Recall that the \emph{orthogonal group} in real dimension $3$ is given by 
\[
  \LieO (3) = \big\{ g \in \LieGL (3,\R) \bigmid \forall \vec{x},\vec{y}\in \R^3 : \: \inprod{g\vec{x},g\vec{y}} = \inprod{\vec{x},\vec{y}} 
  \big\} \ .
\]
The \emph{special orthogonal group} in dimension $3$ is the subgroup 
\[
  \LieSO (3) = \big\{ g \in \LieO (3) \bigmid \det g = 1 \big\} \ .
\] 
Let us show that both are Lie groups. Consider the map
\[
  f : \LieGL (3,\R) \to \symMat (3,\R) , \quad g \mapsto g^\trans g \ , 
\] 
where $g^\trans$ is the transpose of $g$ and $\symMat (n,\R)$ denotes the space of 
real symmetric  $n\times n$ matrices. Note that $\dim \symMat (n,\R) = \frac{n(n+1)}{2}$
and that $f$ is well-defined since $(g^\trans g)^\trans = g^\trans g$. We show that $f$ is a submersion. 
To this end check first that for every $g\in \LieGL (3,\R)$ the tangent map of $f$ at $g$ is 
\[
  T_gf : \liegl (3, \R) \to  \symMat (3,\R), \quad A \mapsto A^\trans g + g^\trans A \ . 
\]
For given $S \in  \symMat (3,\R)$ put $A = \frac 12 \left( g^\trans \right)^{-1} S$ and compute
\[
  T_gf (A) = \frac 12 \left( S^\trans + S \right) = S \ . 
\]  
Hence $f$ is a submersion, and  $\LieO (3)  = f^{-1} (I_3)$ is a submanifold of $\LieGL (3,\R)$ 
of dimension $\dim_\R \LieGL(3,\R) - \dim_\R \symMat (3,\R) = 9 - 6 = 3$. Because the group multiplication 
and inverse on $\LieGL (3,\R)$ are smooth, their restriction to $\LieO (3)$ is so, too, and 
$\LieO (3)$ is a Lie group. Since $(\det g)^2 = \det g \, \det g^\trans =1$ for all $g\in \LieO (3)$,
the subgroup $\LieSO (3) = \LieO (3) \cap \det^{-1} (\R_{>0})$ is open in $\LieO (3)$, and $\LieO (3)$ 
is the disjoint union of $\LieSO (3)$ and $-\LieSO (3)$. Moreover, $\LieSO (3)$ becomes a Lie group. 

The Lie algebra $\lieo (3)$ of $\LieO (3)$ coincides with the Lie algebra $\lieso (3)$ of $\LieSO (3)$ 
and can be determined via the submersion $f$, too. More precisely
\[
   \lieo (3) = \lieso (3) = \ker T_\idnum f = \big\{ A \in \liegl (3,\R)  \bigmid A^\trans + A = 0 \big\} \ ,  
\]
and $\lieso (3)$ is the space of all skew-symmetric real $3\times 3$ matrices. Note that 
$\tr A = 0$ for every element $A \in \lieso (3)$.

\begin{theorem}
\label{thm:isomorphism-lie-algebras-three-dimensional-euclidean-space-special-orthogonal-lie-algebra-three-dimensions}
  The Lie algebras $\left( \R^3,\times\right)$ and $\lieso(3)$ are isomorphic. An isomorphism is given by the map
  \[
     M : \R^3 \to \lieso(3), \quad \vec{x} = \begin{pmatrix} x^1\\ x^2\\ x^3\end{pmatrix}  \mapsto 
     M_{\vec{x}}  = \left( \begin{array}{ccc}
       0 & -x^3 & x^2 \\ x^3 & 0 & -x^1 \\ -x^2 & x^1 & 0
     \end{array} \right)
  \]
  Denoting by $\vec{e}_1, \vec{e}_2,\vec{e}_3$ the standard basis of $\R^3$, the elements 
  \begin{equation*}
    \begin{split}
       J_x & = J_1 = M_{\vec{e}_1} =
   \begin{pmatrix}
     0 & 0& 0 \\
     0 & 0 & -1\\
     0 & 1 & 0 
   \end{pmatrix}, \\
     J_y & = J_2 = M_{\vec{e}_2} = 
    \begin{pmatrix}
     0 & 0& 1 \\
     0 & 0 & 0 \\
     -1 & 0 & 0 
   \end{pmatrix}, \\
   J_z & = J_3 = M_{\vec{e}_3} =
    \begin{pmatrix}
     0 & -1& 0 \\
     1 & 0 & 0 \\
     0 & 0 & 0 
   \end{pmatrix}
    \end{split}
  \end{equation*}
   form a basis of the Lie algebra $\lieso(3)$. These elements are sometimes called the 
   \emph{(standard) infinitesimal generators of rotations}. 
\end{theorem}
\begin{proof}
  By definition $M$ is linear. Moreover, the images $M_{\vec{e}_k}$, $k= 1,2,3$, are linearly independent, so 
  by dimension reasons the map $M$ is a linear isomorphism. It remains to show that $M$ preserves the Lie brackets. 
  To this end compute for $\vec{x}, \vec{y} \in \R^3$
  \begin{equation*}
  \begin{split}
    \vec{x} \times \vec{y} =  
    \begin{pmatrix} x^1\\ x^2\\ x^3\end{pmatrix} \times  \begin{pmatrix} y^1\\ y^2\\ y^3\end{pmatrix} =
    \begin{pmatrix} x^2 \, y^3 - x^3 \, y^2 \\ x^3 \, y^1 - x^1 \, y^3 \\  x^1 \, y^2 - x^2 \, y^1 \end{pmatrix} \ ,
  \end{split}
  \end{equation*}
  and then 
   \begin{equation*}
  \begin{split}
    M_{\vec{x}} \cdot M_{\vec{y}} \, & =  
    \begin{pmatrix}  0 & -x^3 & x^2 \\ x^3 & 0 & -x^1 \\ -x^2 & x^1 & 0 \end{pmatrix} \cdot  
    \begin{pmatrix}  0 & -y^3 & y^2 \\ y^3 & 0 & -y^1 \\ -y^2 & y^1 & 0 \end{pmatrix} = \\
    & = \begin{pmatrix}  - x^3 \, y^3 - x^2 \, y^2 & x^2 \, y^1 & x^3\, y^1 \\ 
    x^1 \, y^2 & - x^3 \, y^3 - x^1 \, y^1 & x^3 \, y^2 \\
    x^1 \, y^3 & x^2 \, y^3  & - x^2 \, y^2 - x^1 \, y^1  \end{pmatrix} \ .
  \end{split}
  \end{equation*}
  Forming the commutator gives
  \begin{equation*}
  \begin{split}
     \left[  M_{\vec{x}} ,  M_{\vec{y}}  \right] = 
      \begin{pmatrix} 0 & x^2 \, y^1 - x^1 \, y^2 & x^3\, y^1 - x^1\, y^3\\ 
    x^1 \, y^2 - x^2 \, y^1 & 0 & x^3 \, y^2 - x^2 \, y^3\\
    x^1 \, y^3 - x^3 \, y^1 & x^2 \, y^3 - x^3 \, y^2 & 0  \end{pmatrix} =
    M_{\vec{x}\times\vec{y}} \ .
  \end{split}
  \end{equation*}
  Hence $M$ preserves Lie brackets and the claim is proved.
\end{proof} 

\para
Now let us consider  the \emph{special unitary group} 
\[  
   \LieSU(2) =  \big\{ g \in \LieGL (2,\C) \bigmid  \forall v,w\in \C^2 : \:  
   \inprod{gv,gw} = \inprod{v,w} \: \& \: \det g = 1 \big\} \ .
\]
To verify that $\LieSU(2)$ is a Lie group let $f$ be the map
\[
   f: \LieGL(2,\C) \to \hermMat (2), \quad g   \mapsto g^* g  \ ,
\] 
where $\hermMat (n)$ denotes the space of hermitian $n\times n$ matrices. The tangent map of $f$ at 
$g\in \LieGL(2,\C) $ is given by 
\[
  T_gf : \liegl(2,\C) \to \hermMat (2), \quad  A \mapsto A^* g +  g^* A \ .
\]
For given $H \in \hermMat (2)$ let $A = \frac 12 (g^*)^{-1} H$. Then
\[
  T_gf (A) = \frac 12 \left( H^* + H \right) = H \ ,
\]
which entails that $f$ is a submersion. Hence $\LieU(2) = f^{-1} (I_2)$ is a real Lie group of dimension 
$\dim_\R \LieGL (2,\C) - \dim_\R  \hermMat (2) = 8 - 4 = 4$. Recall that $\LieU (2)$ is the 
\emph{unitary group} in dimension $2$. The Lie algebra of $\LieU (2)$ is given by 
$\lieu (2)= \ker T_\idnum f$, the space of all skew-hermitian $2\times 2$ matrices.  
The determinant function $\det : \LieU(2) \to \sphere^1$
is a smooth group homomorphism and a submersion. The latter is true because for all $A \in \lieu (2)$ 
\[
  T_\idnum \det (A) = \left.\frac{\partial}{\partial t}\right|_{t=0} \det (\exp(tA)) 
  = \left.\frac{\partial}{\partial t}\right|_{t=0} e^{t\tr A}  = \tr A \ , 
\]
and because the matrix $A =  \cplxi \idnum \in \liegl (2,\C)$ is skew-hermitian and 
its trace $\tr A =  2 \cplxi$ spans $\lie (\sphere^1) = \R \cplxi$. 
Therefore, $\LieSU(2)$ is a real Lie group of dimension $\dim_\R \LieU(2) - \dim_\R \R \cplxi  = 3$ and with Lie algebra 
$\liesu(2)$ given by the skew-hermitian matrices of trace $0$.  

\begin{proposition}
  The Lie group $\LieSU(2)$ is homeomorphic to $\sphere^3$, so in particular compact and simply connected. 
  A homeomorphism is given by 
  \[
     \Psi : \sphere^3 \mapsto \LieSU(2), \: (x^0,x^1,x^2,x^3) \mapsto 
     \begin{pmatrix}
       x^0 + x^1 \cplxi & x^2 + x^3 \cplxi \\ 
       - x^2 + x^3 \cplxi & x^0 - x^1 \cplxi 
     \end{pmatrix} \ .
  \]
\end{proposition}
\begin{proof}
   One has for $(x^0,x^1,x^2,x^3)\in \sphere^3$
   \[
    \begin{pmatrix}
       x^0 + x^1 \cplxi & x^2 + x^3 \cplxi \\ 
       - x^2 + x^3 \cplxi & x^0 - x^1 \cplxi 
     \end{pmatrix}
     \cdot
     \begin{pmatrix}
       x^0 - x^1 \cplxi & - x^2 - x^3 \cplxi \\ 
        x^2 - x^3 \cplxi & x^0 + x^1 \cplxi 
     \end{pmatrix}
     = \idnum \ ,
  \]
  hence the matrix $\Psi (x^0,x^1,x^2,x^3)$ is unitary. So $\Psi$ is well-defined. 
  The map $\Psi$ is obviously continuous and injective. It remains to show that 
  $\Psi$ is surjective, because then, by compactness of the $3$-sphere, the map $\Psi$ is 
  a homeomorphism and $\LieSU(2)$ has to be compact.
  Let 
  \[  
    g = \begin{pmatrix}
     z & u \\ v & w  
    \end{pmatrix}
  \]  
  be a unitary matrix with determinant  being $1$ that is $z w - uv =1$.
  By unitarity and the formula for the inverse of a  $2\times 2$  matrix one obtains the equality
  \[  
    \begin{pmatrix}
     w &  -u \\  - v & z  
    \end{pmatrix}
    =
     \begin{pmatrix}
     \overline{z} & \overline{v} \\ \overline{u} & \overline{w}  
    \end{pmatrix} \ ,
  \]   
  hence $w = \overline{z}$ and $v = - \overline{u}$. Inserting this in the equation for the determinant 
  entails that $|z|^2 + |u|^2 =1$. Now write $z = x^0+ x^1\cplxi$ and $u = x^2+x^3\cplxi$ with real 
  $x^0,x^1,x^2,x^3$. Then  $( x^0,x^1,x^2,x^3)\in \sphere^3$ and $g = \Psi ( x^0,x^1,x^2,x^3)$, so
  $\Psi$ is surjective and the proposition is proved. 
\end{proof}

\begin{proposition}
  Consider the space 
  \[
   \hermtrzMat (2) =  \cplxi \, \liesu(2) = \big\{ X \in \liegl(2,\C)\bigmid X^* = X \: \& \: \tr X = 0 \big\}
  \]
  of all traceless hermitian $2\times 2$ matrices. Then $\hermtrzMat (2)$ is 
  a real vector space of dimension $3$ with a basis given by the \emph{Pauli matrices}
  \[
     \sigma_1 = 
       \left(\begin{array}{cc}
         0 & 1 \\ 1 & 0
       \end{array}\right),   \quad 
       \sigma_2 =  \left(\begin{array}{cc}
         0 & -\cplxi \\ \cplxi & 0 
       \end{array}\right),  \quad 
        \sigma_3 = \left(\begin{array}{cc}
         1 & 0 \\ 0 & -1 
       \end{array}\right)  
  \]
  and with inner product 
  \[
    \inprod{\cdot,\cdot}_{\mathfrak{h}^{\tr\! 0}} :
     \hermtrzMat(2) \times \hermtrzMat(2) \to \R, \quad (X,Y)\mapsto 
   - \frac 12 \left( \det (X+Y)  - \det X - \det Y \right) 
 \]
 and corresponding norm
  \[
    \|\cdot \|_{\mathfrak{h}^{\tr\! 0}} : \hermtrzMat(2) \to \R_{\geq 0}, \quad X\mapsto 
    \sqrt{- \det (X)} \ . 
 \]
  An isometric isomorphism between $\left(\R^3,\inprod{\cdot,\cdot}\right)$ and
  $\left(\hermtrzMat(2),\inprod{\cdot,\cdot}_{\mathfrak{h}^{\tr\! 0}}\right)$ is given by
  \[
     \vec{\sigma} : \R^3 \to \hermtrzMat (2), \quad \vec{x} \mapsto  \vec{\sigma} \cdot \vec{x} = \sum_{k=1}^3 x^k \, \sigma_k \ .
  \]
  Its inverse  maps $X \in \hermtrzMat(2)$ to the vector $\vec{x}$ with 
  components $x^k = \frac 12 \tr (X\sigma_k)$, where $k=1,2,3$. 
\end{proposition}

\begin{proof}
  Let $X = \left( \begin{array}{cc}
      a & z \\ w & d
      \end{array} \right)\in \hermtrzMat(2)$. Then  $ a, d\in \R $ and $w = \overline{z}$, since $X$ is hermitian. 
  The assumption $\tr X = 0$ implies $d=-a$.  Hence $X$ is of the form 
  \[
    \left( \begin{array}{cc}
      a & b +   c \, \cplxi \\  b - c \, \cplxi & -a
    \end{array} \right) 
    = a \sigma_3 + c \sigma_1 + b \sigma_2 
  \]
  with $a,c,d \in \R$, and any such matrix is an element of $\hermtrzMat(2)$. Since the Pauli matrices are 
  obviously linearly independent, they therefore form a basis of $\hermtrzMat(2)$.

  Next compute for $\vec{x} = (x^1,x^2,x^3) \in \R^3$
  \begin{equation}
    \label{eq:negative-determinant-norm}
    \det \left(\vec{\sigma} \cdot \vec{x} \right) = \det  
    \left( \begin{array}{cc}
      x^3 & x^2 +   x^1 \, \cplxi \\  x^2 - x^1 \, \cplxi & -x^3
    \end{array} \right) =
      - (x^3)^2 - (x^1)^2 - (x^2)^2 = - \| \vec{x}\|^2 \ .   
  \end{equation}
  Hence the map $\hermtrzMat(2) \to \R_{\geq 0}$, $X \mapsto \sqrt{-\det (X)}$ is a norm on $\hermtrzMat(2)$ which 
  has to fullfill the parallelogram identity since the euclidean norm $\| \cdot \|$ does. 

  The norm on $\hermtrzMat(2)$ is therefore induced by an inner product which can be recovered by 
  the polarization identity \eqref{eq:real-polarization-identity} that is by 
  \[
    \inprod{X,Y}_{\mathfrak{h}^{\tr\! 0}} = - \frac 12 \left( \det (X+Y)  - \det X - \det Y \right) \quad \text{for all } 
    X,Y \in \hermtrzMat(2) \ . 
  \]
  Moreover, $\vec{\sigma}$ preserves norms by \eqref{eq:negative-determinant-norm}, hence is an isometry. 

  For the remaining part of the claim check first that  $(\sigma_k)^2  = \idnum$ for $ k=1,2,3$ and that 
  \begin{equation}
    \label{eq:cyclic- product-sigma-matrices}
     \sigma_1 \, \sigma_2 =    \left( \begin{array}{cc}
       \cplxi &  0 \\ 0  & - \cplxi
    \end{array} \right) =  \cplxi \sigma_3 , \:
     \sigma_2 \, \sigma_3 =  \left( \begin{array}{cc}
      0 & \cplxi \\ \cplxi  & 0
    \end{array} \right) = \cplxi \sigma_1 , \: 
      \sigma_3 \, \sigma_1  = \left( \begin{array}{cc}
      0 & 1 \\ -1  & 0
    \end{array} \right) = \cplxi \sigma_2 \ .
  \end{equation}
  Since the Pauli matrices are hermitian, forming the hermitian conjugate on 
  both sides of these equations entails
  \begin{equation}
    \label{eq:anticyclic- product-sigma-matrices}
     \sigma_2 \, \sigma_1 =  - \cplxi \sigma_3 , \quad
     \sigma_3 \, \sigma_2 =  - \cplxi \sigma_1 , \quad
      \sigma_1 \, \sigma_3  = - \cplxi \sigma_2 \ .
  \end{equation}
  Now compute for $\vec{x} \in \R^3$ and $k= 1,2,3$
  \[
     \frac 12 \tr \left( (\vec{x} \cdot \vec{\sigma}) \, \sigma_k \right)  = 
     \frac 12 \tr \left( x_k \,  (\sigma_k)^2 \right) = x_k \ .
  \]
  The proposition is  proved. 
\end{proof}

% For later purposes let us determine the commutation relations of the Pauli matrices.  

\begin{lemma}
  For $i,j \in \{ 1,2,3\}$ the Pauli matrices satisfy the following commutation relations:
  \[
     [\sigma_i , \sigma_j] = 2 \cplxi \sum_{k=1}^3 \varepsilon_{ijk} \, \sigma_k \ ,
  \]
  where for $i,j,k \in \{ 1,2,3\}$ the Levi-Civita symbol $\varepsilon_{ijk}$ is defined by
  \[
    \varepsilon_{ijk} =
    \begin{cases}
      1 & \text{if } (i,j,k) \text{ is an even permutation of } (1,2,3), \\
     -1 & \text{if } (i,j,k) \text{ is an odd  permutation of } (1,2,3), \text{ and} \\
     0  & else. 
    \end{cases}
  \]
\end{lemma}

\begin{remark}
  Recall that a permutation of $(1,2,3)$ is even if and only if it is cyclic. 
\end{remark}

\begin{proof}
  The commutation relations follow immediately from equations \eqref{eq:cyclic- product-sigma-matrices} and 
   \eqref{eq:anticyclic- product-sigma-matrices} in the proof of the preceding proposition. 
\end{proof}

\begin{theorem}
\label{thm:isomorphism-lie-algebras-special-unitary-lie-algebra-two-complex-dimensions-special-orthogonal-lie-algebra-three-dimensions}  
  The matrices 
   \[
     \tau_1 = \frac{1}{\cplxi} \sigma_1 = 
       \left(\begin{array}{cc}
         0 & -\cplxi  \\ -\cplxi  & 0
       \end{array}\right),   \quad 
       \tau_2 = \frac{1}{\cplxi}\sigma_2 =  \left(\begin{array}{cc}
         0 & -1 \\ 1 & 0 
       \end{array}\right),  \quad 
       \tau_3 = \frac{1}{\cplxi}  \sigma_3 = \left(\begin{array}{cc}
         - \cplxi  & 0 \\ 0 & \cplxi  
       \end{array}\right) 
  \]
  form a basis of the Lie algebra $\liesu (2)$
  and obey the commutation relations 
  \begin{equation}
    \label{eq:commutation-relations-tau-matrices}
    \left[ \tau_i,\tau_j \right] =  2 \tau_k  \quad\text{for every cyclic permutation } (i,j,k) \text{ of } (1,2,3) \ .
  \end{equation}
  Moreover, the linear map $\Phi: \liesu (2) \to \R^3$ uniquely defined by  $\tau_k \mapsto 2 e_k$ for $k=1,2,3$ 
  is an isomorphism of Lie algebras, where $\R^3$ carries the Lie algebra structure given by the cross product $\times$.  
  In particular, the Lie algebras $\liesu(2)$ and $\lieso(3)$ are isomorphic with an isomorphism given by
  the composition
  \[
    M\circ \Phi : \liesu(2) \to \lieso(3), \quad \sum_{k=1}^3 x^k \tau_k = \begin{pmatrix}
         - x^3 \cplxi  & -x^2 - x^1 \cplxi \\ x^2 - x^1 \cplxi & x^3 \cplxi  
       \end{pmatrix} \mapsto 2
       \begin{pmatrix}
          0 & -x^3 & x^2 \\ x^3 & 0 & -x^1 \\ -x^2 & x^1 & 0
       \end{pmatrix} \ ,
  \]
  where $M: \R^3 \to \lieso(3)$ is the isomorphism from
  \Cref{thm:isomorphism-lie-algebras-three-dimensional-euclidean-space-special-orthogonal-lie-algebra-three-dimensions}.    
\end{theorem}

\begin{proof}
  Since multiplication by $-\cplxi$ is a real linear isomorphism from  $\hermtrzMat(2)$ to  $\liesu (2)$ 
  and since the Pauli matrices form a basis of $\hermtrzMat(2)$, the  matrices $\tau_k$, $k=1,2,3$, form a basis of
  $\liesu (2)$.
  The commutation relations \eqref{eq:commutation-relations-tau-matrices} are an immediate consequence of the preceding lemma.
  The Lie bracket is preserved by $\Phi$ since 
  \[
      (2e_i) \times (2e_j) = 2 (2e_k) \quad \text{for every cyclic permutation } (i,j,k) \text{ of } (1,2,3) \ .
  \] 
  The rest of the claim now follows by definition of $\Phi$ and 
  \Cref{thm:isomorphism-lie-algebras-three-dimensional-euclidean-space-special-orthogonal-lie-algebra-three-dimensions}.  
\end{proof}

\begin{theorem}
  For every $g \in \LieSU(2)$ the linear map 
  \[
    \pi_g : \R^3 \to \R^3 ,\quad 
    \vec{x} \mapsto \vec{\sigma}^{-1} \big( g  (\vec{\sigma}\cdot \vec{x}) g^* \big)
  \]
  is an orthogonal transformation. Moreover, the map
  \[
   \pi: \LieSU(2) \to \LieSO(3), \quad g \mapsto \pi_g 
  \]
  is a differentiable surjective group homomorphism with kernel $\{ \pm I_2 \} \cong \Z/2$.
  In particular,  $\pi$ is the universal covering map of $\LieSO(3)$. 
  Finally, the tangent map $T_\idnum \pi : \liesu(2) \to \lieso (3)$ coincides with the isomorphism 
  $M\circ \Phi$ from \Cref{thm:isomorphism-lie-algebras-special-unitary-lie-algebra-two-complex-dimensions-special-orthogonal-lie-algebra-three-dimensions}.
 
\end{theorem}

\begin{proof}
  Observe that $\det (g A g^*) = \det A$ and $\tr (g A g^*) = \tr (A)$ for all $g \in \LieSU(2)$ and
  $A \in \hermtrzMat (2)$. This together with the fact that $\vec{\sigma}$ is an isometric isomorphism
  from $(\R^3, \|\cdot\|)$ to $(\hermtrzMat (2), \sqrt{- \det ( \, \cdot\, )})$ entails
  that the transformations $\pi_g$ are orthogonal.  
\end{proof}