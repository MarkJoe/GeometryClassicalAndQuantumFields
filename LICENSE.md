Permission is granted to copy and distribute this document under the terms of the
Creative Commons License Attribution-NonCommercial-NoDerivatives 4.0 International
(CC BY-NC-ND 4.0).

See licenses/CC_BY_NC_4.0.md or https://creativecommons.org/licenses/by-nc-nd/4.0/
for the wording of the license.