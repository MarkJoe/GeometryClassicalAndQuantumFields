title: The Geometry of Classical and Quantum Fields
tex_title: The Geometry of Classical and Quantum Fields
ref_title: GeometryClassicalAndQuantumFields
publication_type: compendium
authors:
  - name: Markus J. Pflaum
    role: main_author
  - name: Jonathan Belcher
    role: contributor
  - name: Daniel Spiegel
    role: author_1
copyright: Markus J. Pflaum 
external_projects: 
  - title: The FANCy Project
    label: FANCyProject   
    path: standard
parts:
  - title: Front Matter
    numbering: nonumber
    label: part:front-matter
    chapters: 
    - title: Titelage
      numbering: nonumber 
      label: chpt:titelage
      sections:
        - title: Copyright
          numbering: nonumber  
          label: sec:copyright
        - title: Authors 
          numbering: nonumber 
          label: sec:authors
        - title: Content
          numbering: nonumber 
          label: sec:content
        - title: Attribution
          numbering: nonumber     
          label: sec:attribution
    - title: Introduction
      numbering: nonumber 
      label: chpt:introduction

  - title: Classical Fields
    numbering: Roman
    label: part:classical-fields
    chapters:
      - title: Variational Calculus
        numbering: arabic
        label: chpt:variational-calculus 
        sections:
          - title: Euler--Lagrange equations
            numbering: arabic
            label: sec:euler-lagrange-equations
          - title: The variational bicomplex 
            numbering: arabic
            label: sec:variational-bicomplex
      - title: Semi-riemannian geometry     
        numbering: arabic
        label: chpt:semi-riemannian-geometry
        sections:
          - title: Causal structures
            numbering: arabic
            label: sec:causal-structures
  - title: Quantum Mechanics
    numbering: Roman
    label: part:quantum-mechanics
    chapters:
      - title: The postulates of quantum mechanics
        numbering: arabic
        label: chpt:postulates-quantum-mechanics
        sections:
          - title:  The geometry of projective Hilbert spaces
            numbering: arabic
            label: sec:geometry-projective-hilbert-spaces
          - title: Quantum mechanical symmetries
            numbering: arabic
            label: quantum-mechanical-symmetries 
      - title: Deformation quantization
        numbering: arabic
        label: chpt:deformation-quantization     
        sections:
          - title: Fedosov's construction of star-products
            numbering: arabic
            label: sec:fedosovs-construction-star-products
      - title: Quantum spin systems
        numbering: arabic
        label: chpt:quantum-spin-systems
        sections:       
          - title: The quasi-local algebra of a spin lattice model
            numbering: arabic
            label: sec:quasi-local-algebra-spin-lattice-model
      - title: Molecular quantum mechanics
        numbering: arabic
        label: chpt:molecular-quantum-mechanics
        sections:
         - title: The von Neumann--Wigner no crossing-rule
           numbering: arabic
           label: sec:von-neumann-wigner-no-crossing-rule 
  - title: Quantum Fields
    numbering: Roman
    label: part:quantum-fields
    chapters:
      - title: Representations of the Lorentz and Poincare groups
        tex_title: Representations of the Lorentz and Poincar\'e groups
        numbering: arabic
        label: chpt:representations-lorentz-poincare-groups
        sections:
          - title: The Lorentz invariant measure on a mass hyperboloid
            numbering: arabic
            label:  sec:lorentz-invariant-measure-mass-hyperboloid
      - title: Axiomatic quantum field theory a la Wightman and Garding
        tex_title: Axiomatic quantum field theory \`a la Wightman and G{\aa}rding
        numbering: arabic
        label: chpt:axiomatic-quantum-field-theory-wightman-garding
        sections:
          - title: Wightman axioms
            numbering: arabic
            label: sec:wightman-axioms 
          - title: Fock space
            numbering: arabic
            label: sec:fock-space
          - title: The free scalar field
            numbering: arabic
            label: sec:free-scalar-field 
      - title: Axiomatic quantum field theory a la Haag--Kastler
        tex_title: Axiomatic quantum field theory \`a la Haag--Kastler
        numbering: arabic
        label: chpt:axiomatic-quantum-field-theory-haag--kastler   
        sections:
          - title: Haag--Kastler axioms
            numbering: arabic
            label: sec:haag-kastler-axioms                
  - title: Mathematical Toolbox
    numbering: nonumber
    toc_entry: Mathematical Toolbox
    label: part:mathematical-toolbox
    chapters:
      - title: Topological Vector Spaces
        numbering: arabic
        label: chpt:topological-vector-spaces  
        sections:   
          - title: The category of topological vector spaces
            numbering: arabic
            external: FANCyProject
            label: sec:category-topological-vector-spaces    
      - title: Manifolds  
        numbering: arabic
        label: chpt:manifolds
        sections:    
          - title: Charts and atlases
            numbering: arabic
            label: sec:charts-atlases  
          - title: The category of pro-manifolds
            numbering: arabic
            label: sec:category-pro-manifolds
          - title: Tangent bundles
            numbering: arabic
            label: sec:tangent-bundles
          - title: Cotangent bundles and forms
            numbering: arabic
            label: sec:cotangent-bundles-forms
          - title: Tensor bundles
            numbering: arabic
            label: sec:tensor-bundles
          - title: Tangent distributions and integrability 
            numbering: arabic
            label: sec:tangent-distributions-integrability
          - title: Graßmann manifolds of Banach spaces
            tex_title: Gra{\ss}mann manifolds of Banach spaces
            numbering: arabic
            label: sec:grassmann-manifolds-banach-spaces
      - title: Lie groups
        numbering: arabic
        label: chpt:lie-groups
        sections:  
          - title: Symmetry groups of bilinear and sesquilinear forms
            numbering: arabic
            label: sec:symmetry-groups-bilinear-sesquilinear-forms
      - title: Jets  
        numbering: arabic
        label: chpt:jets
        sections:
          - title: A combinatorical interlude 
            numbering: arabic
            label: sec:combinatorical-interlude 
          - title: Jet bundles
            numbering: arabic
            label: sec:jet-bundles
            
