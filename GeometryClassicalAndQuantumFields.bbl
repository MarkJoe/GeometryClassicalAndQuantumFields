\def\cprime{$'$}
\begin{thebibliography}{}
\addcontentsline{toc}{chapter}{Bibliography}

\bibitem[\protect\citeauthoryear{Akhiezer \& Glazman}{Akhiezer \&
  Glazman}{1993}]{AkhGlaTLOHS}
Akhiezer, N.~I. \& Glazman, I.~M. (1993).
\newblock {\em Theory of linear operators in {H}ilbert space}.
\newblock Dover Publications, Inc., New York.
\newblock Translated from the Russian and with a preface by Merlynd Nestell,
  Reprint of the 1961 and 1963 translations, Two volumes bound as one.

\bibitem[\protect\citeauthoryear{Atiyah \& Segal}{Atiyah \&
  Segal}{2004}]{AtiSegTKT}
Atiyah, M. \& Segal, G. (2004).
\newblock Twisted {$K$}-theory.
\newblock {\em Ukr. Mat. Visn.}, {\em 1\/}(3), 287--330.

\bibitem[\protect\citeauthoryear{Baez}{Baez}{1997}]{BaeHDAII2HS}
Baez, J. (1997).
\newblock Higher-{D}imensional {A}lgebra {II}. 2-{H}ilbert {S}paces.
\newblock {\em Adv.\ Math.}, {\em 127}, 125--189.

\bibitem[\protect\citeauthoryear{Bargmann}{Bargmann}{1954}]{BarURRCG}
Bargmann, V. (1954).
\newblock On unitary ray representations of continuous groups.
\newblock {\em Ann. of Math. (2)}, {\em 59}, 1--46.

\bibitem[\protect\citeauthoryear{Bargmann}{Bargmann}{1964}]{BarNWTSO}
Bargmann, V. (1964).
\newblock Note on {W}igner's theorem on symmetry operations.
\newblock {\em J. Mathematical Phys.}, {\em 5}, 862--868.

\bibitem[\protect\citeauthoryear{Blackadar}{Blackadar}{1977}]{BlaITPC*A}
Blackadar, B.~E. (1977).
\newblock Infinite tensor products of {$C\sp*$}-algebras.
\newblock {\em Pacific J. Math.}, {\em 72\/}(2), 313--334.

\bibitem[\protect\citeauthoryear{Bratteli \& Robinson}{Bratteli \&
  Robinson}{1997}]{BraRobOAQSM2}
Bratteli, O. \& Robinson, D.~W. (1997).
\newblock {\em Operator algebras and quantum statistical mechanics. 2\/}
  (Second ed.).
\newblock Texts and Monographs in Physics. Springer-Verlag, Berlin.
\newblock Equilibrium states. Models in quantum statistical mechanics.

\bibitem[\protect\citeauthoryear{Chevalley}{Chevalley}{1956}]{CheFCA}
Chevalley, C. (1956).
\newblock {\em Fundamental concepts of algebra}.
\newblock Academic Press Inc., New York.

\bibitem[\protect\citeauthoryear{Cohen}{Cohen}{1993}]{CohCCANT}
Cohen, H. (1993).
\newblock {\em A {C}ourse in {C}omputational {A}lgebraic {N}umber {T}heory},
  volume 138 of {\em Graduate Texts in Mathematics}.
\newblock Springer-Verlag, Berlin.

\bibitem[\protect\citeauthoryear{Cook}{Cook}{1953}]{CooMSQ}
Cook, J.~M. (1953).
\newblock The mathematics of second quantization.
\newblock {\em Trans. Amer. Math. Soc.}, {\em 74}, 222--245.

\bibitem[\protect\citeauthoryear{Coutinho}{Coutinho}{1995}]{CouPADM}
Coutinho, S.~C. (1995).
\newblock {\em A primer of algebraic {$D$}-modules}, volume~33 of {\em London
  Mathematical Society Student Texts}.
\newblock Cambridge University Press, Cambridge.

\bibitem[\protect\citeauthoryear{Emch}{Emch}{2009}]{EmcAMSMQFT}
Emch, G.~G. (2009).
\newblock {\em Algebraic Methods in Statistical Mechanics and Quantum Field
  Theory}.
\newblock Dover Publications, Inc., New York.

\bibitem[\protect\citeauthoryear{Frobenius}{Frobenius}{1878}]{FroLSBF}
Frobenius, F.~G. (1878).
\newblock {\"U}ber lineare {S}ubstitutionen und bilineare {F}ormen.
\newblock {\em {J}ournal f{\"u}r die reine und angewandte {M}athematik}, {\em
  84}, 1--63.

\bibitem[\protect\citeauthoryear{Geh\'er}{Geh\'er}{2014}]{GehEPNBVWT}
Geh\'er, G.~P. (2014).
\newblock An elementary proof for the non-bijective version of {W}igner's
  theorem.
\newblock {\em Phys. Lett. A}, {\em 378\/}(30-31), 2054--2057.

\bibitem[\protect\citeauthoryear{Gouv\^{e}a}{Gouv\^{e}a}{1997}]{GouAN}
Gouv\^{e}a, F.~Q. (1997).
\newblock {\em {$p$}-adic numbers\/} (Second ed.).
\newblock Universitext. Springer-Verlag, Berlin.
\newblock An introduction.

\bibitem[\protect\citeauthoryear{Grothendieck}{Grothendieck}{1955}]{GroPTTEN}
Grothendieck, A. (1955).
\newblock Produits tensoriels topologiques et espaces nucl\'eaires.
\newblock {\em Mem. Amer. Math. Soc.}, {\em No. 16}, 140.

\bibitem[\protect\citeauthoryear{Guichardet}{Guichardet}{1966}]{GuiPTIRRA}
Guichardet, A. (1966).
\newblock Produits tensoriels infinis et repr\'{e}sentations des relations
  d'anticommutation.
\newblock {\em Ann. Sci. \'{E}cole Norm. Sup. (3)}, {\em 83}, 1--52.

\bibitem[\protect\citeauthoryear{Gustafson \& Sigal}{Gustafson \&
  Sigal}{2011}]{GusSigMCQM}
Gustafson, S.~J. \& Sigal, I.~M. (2011).
\newblock {\em Mathematical concepts of quantum mechanics\/} (Second ed.).
\newblock Universitext. Springer, Heidelberg.

\bibitem[\protect\citeauthoryear{Hirzebruch \& Scharlau}{Hirzebruch \&
  Scharlau}{1991}]{HirScharEF}
Hirzebruch, F. \& Scharlau, W. (1991).
\newblock {\em Einf\"{u}hrung in die {F}unktionalanalysis}, volume 296 of {\em
  B.I.-Hochschultaschenb\"{u}cher [B.I. University Paperbacks]}.
\newblock Bibliographisches Institut, Mannheim.
\newblock Reprint of the 1971 original.

\bibitem[\protect\citeauthoryear{Johnson \& Manes}{Johnson \&
  Manes}{1970}]{JohManMS}
Johnson, J.~S. \& Manes, E.~G. (1970).
\newblock On modules over a semiring.
\newblock {\em J. Algebra}, {\em 15}, 57--67.

\bibitem[\protect\citeauthoryear{Jost}{Jost}{1965}]{JosGTQF}
Jost, R. (1965).
\newblock {\em The general theory of quantized fields}, volume 1960 of {\em
  Mark Kac, editor. Lectures in Applied Mathematics (Proceedings of the Summer
  Seminar, Boulder, Colorado}.
\newblock American Mathematical Society, Providence, R.I.

\bibitem[\protect\citeauthoryear{Joyce}{Joyce}{2012}]{JoyMC}
Joyce, D. (2012).
\newblock On manifolds with corners.
\newblock In {\em Advances in geometric analysis}, volume~21 of {\em Adv. Lect.
  Math. (ALM)}  (pp.\ 225--258). Int. Press, Somerville, MA.

\bibitem[\protect\citeauthoryear{Kadison \& Ringrose}{Kadison \&
  Ringrose}{1997}]{KadRinFTOAII}
Kadison, R.~V. \& Ringrose, J.~R. (1997).
\newblock {\em Fundamentals of the theory of operator algebras. {V}ol.\ {II}},
  volume~16 of {\em Graduate Studies in Mathematics}.
\newblock American Mathematical Society, Providence, RI.
\newblock Advanced theory, Corrected reprint of the 1986 original.

\bibitem[\protect\citeauthoryear{Kriegl \& Michor}{Kriegl \&
  Michor}{1997}]{KriMicCSGA}
Kriegl, A. \& Michor, P.~W. (1997).
\newblock {\em The convenient setting of global analysis}, volume~53 of {\em
  Mathematical Surveys and Monographs}.
\newblock American Mathematical Society, Providence, RI.

\bibitem[\protect\citeauthoryear{Lang}{Lang}{2002}]{LanA3rd}
Lang, S. (2002).
\newblock {\em Algebra\/} (3rd. ed.)., volume 211 of {\em Graduate Texts in
  Mathematics}.
\newblock Springer-Verlag, New York.

\bibitem[\protect\citeauthoryear{Lomont \& Mendelson}{Lomont \&
  Mendelson}{1963}]{LomMenWUAT}
Lomont, J.~S. \& Mendelson, P. (1963).
\newblock The {W}igner unitarity-antiunitarity theorem.
\newblock {\em Ann. of Math. (2)}, {\em 78}, 548--559.

\bibitem[\protect\citeauthoryear{Montgomery \& Zippin}{Montgomery \&
  Zippin}{1955}]{MonZipTTG}
Montgomery, D. \& Zippin, L. (1955).
\newblock {\em Topological transformation groups}.
\newblock Interscience Publishers, New York-London.

\bibitem[\protect\citeauthoryear{Nakagami}{Nakagami}{1970a}]{NakITPvNAI}
Nakagami, Y. (1970a).
\newblock Infinite tensor products of von {N}eumann algebras. {I}.
\newblock {\em K{\={o}}dai Math. Sem. Rep.}, {\em 22}, 341--354.

\bibitem[\protect\citeauthoryear{Nakagami}{Nakagami}{1970b}]{NakITPvNAII}
Nakagami, Y. (1970b).
\newblock Infinite tensor products of von {N}eumann algebras. {II}.
\newblock {\em Publ. Res. Inst. Math. Sci.}, {\em 6}, 257--292.

\bibitem[\protect\citeauthoryear{Neeb}{Neeb}{1997}]{NeeTSB}
Neeb, K.-H. (1997).
\newblock On a theorem of {S}. {B}anach.
\newblock {\em J. Lie Theory}, {\em 7\/}(2), 293--300.

\bibitem[\protect\citeauthoryear{Ng}{Ng}{2013}]{NgGIATP}
Ng, C.-K. (2013).
\newblock On genuine infinite algebraic tensor products.
\newblock {\em Rev. Mat. Iberoam.}, {\em 29\/}(1), 329--356.

\bibitem[\protect\citeauthoryear{Ostrowski}{Ostrowski}{1916}]{OstLF}
Ostrowski, A. (1916).
\newblock {\"U}ber einige {L}{\"o}sungen der {F}unktionalgleichung
  {$\psi(x)\cdot\psi(x)=\psi(xy)$}.
\newblock {\em Acta Math.}, {\em 41\/}(1), 271--284.

\bibitem[\protect\citeauthoryear{Pietsch}{Pietsch}{1972}]{PieNLCS}
Pietsch, A. (1972).
\newblock {\em Nuclear locally convex spaces}.
\newblock Springer-Verlag, New York-Heidelberg.
\newblock Translated from the second German edition by William H. Ruckle,
  Ergebnisse der Mathematik und ihrer Grenzgebiete, Band 66.

\bibitem[\protect\citeauthoryear{Schottenloher}{Schottenloher}{1995}]{SchotGSP}
Schottenloher, M. (1995).
\newblock {\em Geometrie und Symmetrie in der Physik}.
\newblock Vieweg Verlagsgesellschaft.

\bibitem[\protect\citeauthoryear{Schottenloher}{Schottenloher}{2008}]{SchotMICFT}
Schottenloher, M. (2008).
\newblock {\em A mathematical introduction to conformal field theory\/} (Second
  ed.)., volume 759 of {\em Lecture Notes in Physics}.
\newblock Springer-Verlag, Berlin.

\bibitem[\protect\citeauthoryear{Simms}{Simms}{1968}]{SimLGQM}
Simms, D.~J. (1968).
\newblock {\em Lie groups and quantum mechanics}, volume~59 of {\em Lecture
  Notes in Mathematics}.
\newblock Springer-Verlag, Berlin-New York.

\bibitem[\protect\citeauthoryear{Simms}{Simms}{1971}]{SimSPBCLPRLG}
Simms, D.~J. (1971).
\newblock A short proof of {B}argmann's criterion for the lifting of projective
  representations of {L}ie groups.
\newblock {\em Rep. Mathematical Phys.}, {\em 2\/}(4), 283--287.

\bibitem[\protect\citeauthoryear{St{\o}rmer}{St{\o}rmer}{1971}]{StoITPvNA}
St{\o}rmer, E. (1971).
\newblock On infinite tensor products of von {N}eumann algebras.
\newblock {\em Amer. J. Math.}, {\em 93}, 810--818.

\bibitem[\protect\citeauthoryear{Streater \& Wightman}{Streater \&
  Wightman}{2000}]{StrWigPCTSS}
Streater, R.~F. \& Wightman, A.~S. (2000).
\newblock {\em P{CT}, spin and statistics, and all that}.
\newblock Princeton Landmarks in Physics. Princeton University Press,
  Princeton, NJ.
\newblock Corrected third printing of the 1978 edition.

\bibitem[\protect\citeauthoryear{Uhlhorn}{Uhlhorn}{1962}]{UhlRSTQM}
Uhlhorn, U. (1962).
\newblock Representation of symmetry transformations in quantum mechanics.
\newblock {\em Ark. Fys.}, {\em 23\/}(30), 307--340.

\bibitem[\protect\citeauthoryear{von Neumann}{von Neumann}{1939}]{vNeuIDP}
von Neumann, J. (1939).
\newblock On infinite direct products.
\newblock {\em Compositio Math.}, {\em 6}, 1--77.

\bibitem[\protect\citeauthoryear{von Neumann \& Wigner}{von Neumann \&
  Wigner}{1929}]{vNeuWigVEAP}
von Neumann, J. \& Wigner, E. (1929).
\newblock {{\"U}ber das Verhalten von Eigenwerten bei adiabatischen Prozessen}.
  ({German}) [{On} the behavior of the eigenvalues of adiabatic processes].
\newblock {\em Physikalische Zeitschrift}, {\em 30\/}(15), 467--470.

\bibitem[\protect\citeauthoryear{Wightman \& G{\aa}rding}{Wightman \&
  G{\aa}rding}{1964}]{WigGarFOVDRQT}
Wightman, A.~S. \& G{\aa}rding, L. (1964).
\newblock Fields as operator-valued distributions in relativistic quantum
  theory.
\newblock {\em Arkiv f. Fysik, Kungl. Svenska Vetenskapsak}, {\em 28},
  129--189.

\bibitem[\protect\citeauthoryear{Wigner}{Wigner}{1944}]{WigGAQA}
Wigner, E. (1944).
\newblock {\em Gruppentheorie und ihre {A}nwendung auf die {Q}uantenmechanik
  der {A}tomspektren}.
\newblock J. W. Edwards, Ann Arbor, Michigan.

\end{thebibliography}
